// [v0.1.024-20240906]

const xObj = require('#lib/xObj-lib.js');

const t_Mod = require('#lib/xObj-lib.js');

const { runTestFn } = require('#test-dir/test-hfunc.js');

const t_Dat = require('./data/index.2.data.js');

describe('1. special r/w ops applied for attr/param', () => {
  describe.each([
    {
      msg: '1.1 - function',
      method: 'writeXObjParamRaw',
    }, {
      msg: '1.2 - function',
      method: 'writeXObjAttrRaw',
    },
  ])('$msg: xObj.$method()', ({ method }) => {
    const testData = t_Dat[method];
    describe.each(testData.tests)('+ $msg $rem', ({ param }) => {
      describe.each(param)('+ $msg', ({ param }) => {
        describe.each(param)('+ $msg $rem', ({ values, status }) => {
          const { before: stat_b, after: stat_a } = status;
          let obj = undefined;
          beforeAll(() => {
            if (stat_b || stat_a) { ({ obj } = values); };
          });
          if (stat_b) {
            it('check object status (before)', () => {
              expect(obj).toStrictEqual(stat_b.obj);
            });
          };
          it('perform ops', () => {
            const { ops, assertQty } = status;
            let result = undefined; let isERR = false;
            try {
              result = runTestFn({ testInst: t_Mod, method }, values);
              //console.log('CHECK: > NO_ERR');
            } catch (err) {
              //console.log('CHECK: > '+err);
              isERR = true;
              result = err;
            } finally {
              expect.assertions(assertQty);
              expect(isERR).toBe(ops.isERR);
              if (isERR) {
                const { errType, errCode } = ops;
                expect((result instanceof errType)).toBe(true);
                expect(result.code).toStrictEqual(errCode);
              } else {
                const { className, value } = ops;
                switch (typeof className) {
                  case 'string' : {
                    expect(result instanceof globalThis[className]).toBe(true);
                    if (className !== 'HTMLElement') break;
                  }
                  default : {
                    expect(result).toStrictEqual(value);
                  }
                };
              };
            };
          });
          if (stat_a) {
            it('check object status (after)', () => {
              expect(obj).toStrictEqual(stat_a.obj);
            });
          };
        });
      });
    });
  });

});

describe('2. base r/w ops applied for attr/param', () => {
  // *** function: writeXObjParam()
  describe('2.1 - function: xObj.writeXObjParam', () => {
    const method = 'writeXObjParam';
    const testData = t_Dat[method];
    describe.each(testData.descr)('$msg', ({ param }) => {
      describe.each(param)('$msg', ({ param }) => {
        const testFn = (...args) => { return xObj[method](...args); };
        it.each(param)('$msg $rem', ({ values, status }) => {
          const { ops, assertQty } = status;
          const { isErr, errType, errCode } = ops;
          let { obj, value, key } = values;
          let result = undefined;
          try {
            result = testFn(obj, value, key);
            expect(isErr).toBe(false);
            //console.log('CHECK: > NO_ERR');
            expect(result).toStrictEqual(ops.value);
          } catch (err) {
            expect(isErr).toBe(true);
            //console.log('CHECK: > '+err);
            expect((err instanceof errType)).toBe(true);
            expect(err.code).toStrictEqual(errCode);
          } finally {
            expect(obj).toStrictEqual(status.obj);
            expect.assertions(assertQty);
          };
        });
      });
    });
  });

  // *** function: writeXObjAttr()
  describe('2.2 - function: xObj.writeXObjAttr', () => {
    const method = 'writeXObjAttr';
    const testData = t_Dat[method];
    describe.each(testData.descr)('$msg', ({ param }) => {
      describe.each(param)('$msg', ({ param }) => {
        const testFn = (...args) => { return xObj[method](...args); };
        it.each(param)('$msg $rem', ({ values, status }) => {
          const { ops, assertQty } = status;
          const { isErr, errType, errCode } = ops;
          let { obj, attr, value, key } = values;
          let result = undefined;
          try {
            result = testFn(obj, attr, value, key);
            expect(isErr).toBe(false);
            //console.log('CHECK: > NO_ERR');
            expect(result).toStrictEqual(ops.value);
          } catch (err) {
            expect(isErr).toBe(true);
            //console.log('CHECK: > '+err);
            expect((err instanceof errType)).toBe(true);
            expect(err.code).toStrictEqual(errCode);
          } finally {
            expect(obj).toStrictEqual(status.obj);
            expect.assertions(assertQty);
          };
        });
      });
    });
  });
});

describe('3. enhanced r/w ops applied for attr/param', () => {
  describe.each([
    {
      msg: '3.1 - function',
      method: 'writeXObjParamAsBool',
    }, {
      msg: '3.2 - function',
      method: 'writeXObjParamAsNum',
    }, {
      msg: '3.3 - function',
      method: 'writeXObjParamAsIndex',
    }, {
      msg: '3.5 - function',
      method: 'writeXObjAttrAsBool',
    }, {
      msg: '3.6 - function',
      method: 'writeXObjAttrAsNum',
    }, {
      msg: '3.7 - function',
      method: 'writeXObjAttrAsIndex',
    },
  ])('$msg: xObj.$method()', ({ method }) => {
    const testData = t_Dat[method];
    describe.each(testData.tests)('+ $msg $rem', ({ param }) => {
      describe.each(param)('+ $msg', ({ param }) => {
        describe.each(param)('+ $msg $rem', ({ values, status }) => {
          const { before: stat_b, after: stat_a } = status;
          let obj = undefined;
          beforeAll(() => {
            if (stat_b || stat_a) { ({ obj } = values); };
          });
          if (stat_b) {
            it('check object status (before)', () => {
              expect(obj).toStrictEqual(stat_b.obj);
            });
          };
          it('perform ops', () => {
            const { ops, assertQty } = status;
            let result = undefined; let isERR = false;
            try {
              result = runTestFn({ testInst: t_Mod, method }, values);
              //console.log('CHECK: > NO_ERR');
            } catch (err) {
              //console.log('CHECK: > '+err);
              isERR = true;
              result = err;
            } finally {
              expect.assertions(assertQty);
              expect(isERR).toBe(ops.isERR);
              if (isERR) {
                const { errType, errCode } = ops;
                expect((result instanceof errType)).toBe(true);
                expect(result.code).toStrictEqual(errCode);
              } else {
                const { className, value } = ops;
                switch (typeof className) {
                  case 'string' : {
                    expect(result instanceof globalThis[className]).toBe(true);
                    if (className !== 'HTMLElement') break;
                  }
                  default : {
                    expect(result).toStrictEqual(value);
                  }
                };
              };
            };
          });
          if (stat_a) {
            it('check object status (after)', () => {
              expect(obj).toStrictEqual(stat_a.obj);
            });
          };
        });
      });
    });
  });

  // *** function: writeXObjParamEx()
  describe('3.4 - function: xObj.writeXObjParamEx', () => {
    const method = 'writeXObjParamEx';
    const testData = t_Dat[method];
    describe.each(testData.descr)('$msg', ({ param }) => {
      describe.each(param)('$msg', ({ param }) => {
        const testFn = (...args) => { return xObj[method](...args); };
        it.each(param)('$msg $rem', ({ values, status }) => {
          const { ops, assertQty } = status;
          const { isErr, errType, errCode } = ops;
          let { obj, value, defValue, key } = values;
          let result = undefined;
          try {
            result = testFn(obj, value, defValue, key);
            expect(isErr).toBe(false);
            //console.log('CHECK: > NO_ERR');
            expect(result).toStrictEqual(ops.value);
          } catch (err) {
            expect(isErr).toBe(true);
            //console.log('CHECK: > '+err);
            expect((err instanceof errType)).toBe(true);
            expect(err.code).toStrictEqual(errCode);
          } finally {
            expect(obj).toStrictEqual(status.obj);
            expect.assertions(assertQty);
          };
        });
      });
    });
  });

  // *** function: writeXObjAttrEx()
  describe('3.8 - function: xObj.writeXObjAttrEx', () => {
    const method = 'writeXObjAttrEx';
    const testData = t_Dat[method];
    describe.each(testData.descr)('$msg', ({ param }) => {
      describe.each(param)('$msg', ({ param }) => {
        const testFn = (...args) => { return xObj[method](...args); };
        it.each(param)('$msg $rem', ({ values, status }) => {
          const { ops, assertQty } = status;
          const { isErr, errType, errCode } = ops;
          let { obj, attr, value, defValue, key } = values;
          let result = undefined;
          try {
            result = testFn(obj, attr, value, defValue, key);
            expect(isErr).toBe(false);
            //console.log('CHECK: > NO_ERR');
            expect(result).toStrictEqual(ops.value);
          } catch (err) {
            expect(isErr).toBe(true);
            //console.log('CHECK: > '+err);
            expect((err instanceof errType)).toBe(true);
            expect(err.code).toStrictEqual(errCode);
          } finally {
            expect(obj).toStrictEqual(status.obj);
            expect.assertions(assertQty);
          };
        });
      });
    });
  });
});
