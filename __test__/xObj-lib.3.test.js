// [v0.1.036-20240910]

const xObj = require('#lib/xObj-lib.js');

const t_Mod = require('#lib/xObj-lib.js');

const { runTestFn } = require('#test-dir/test-hfunc.js');

const t_Dat = require('./data/index.3.data.js');

describe('1. base ops applied to elements', () => {
  describe.each([
    {
      msg: '1.1 - function',
      method: 'getXObjElement',
    }, {
      msg: '1.2 - function',
      method: 'getXObjAttributes',
    }, {
      msg: '1.3 - function',
      method: 'checkXObjAttribute',
    },
  ])('$msg: xObj.$method()', ({ method }) => {
    const testData = t_Dat[method];
    describe.each(testData.tests)('+ $msg $rem', ({ param }) => {
      describe.each(param)('+ $msg', ({ param }) => {
        it.each(param)('+ $msg $rem', ({ values, status }) => {
          const { ops, assertQty } = status;
          let result = undefined; let isERR = false;
          try {
            result = runTestFn({ testInst: t_Mod, method }, values);
            //console.log('CHECK: > NO_ERR');
          } catch (err) {
            //console.log('CHECK: > '+err);
            isERR = true;
            result = err;
          } finally {
            expect.assertions(assertQty);
            expect(isERR).toBe(ops.isERR);
            if (isERR) {
              const { errType, errCode } = ops;
              expect((result instanceof errType)).toBe(true);
              expect(result.code).toStrictEqual(errCode);
            } else {
              const { className, value } = ops;
              switch (typeof className) {
                case 'string' : {
                  expect(result instanceof globalThis[className]).toBe(true);
                  if (className !== 'HTMLElement') break;
                }
                default : {
                  expect(result).toStrictEqual(value);
                }
              };
            };
          };
        });
      });
    });
  });
  //===
});

describe('2. base write ops applied to elements', () => {
  describe.each([
    {
      msg: '2.1 - function',
      method: 'addXObjElement',
    }, {
      msg: '2.2 - function',
      method: 'insertXObjElement',
    }, {
      msg: '2.3 - function',
      method: 'insertXObjElementEx',
    }, {
      msg: '2.4 - function',
      method: 'deleteXObjElement',
    }, {
      msg: '2.5 - function',
      method: 'deleteXObjElementEx',
    },
  ])('$msg: xObj.$method()', ({ method }) => {
    const testData = t_Dat[method];
    describe.each(testData.tests)('+ $msg $rem', ({ param }) => {
      describe.each(param)('+ $msg', ({ param }) => {
        describe.each(param)('+ $msg $rem', ({ values, status }) => {
          const { before: stat_b, after: stat_a } = status;
          let obj = undefined;
          beforeAll(() => {
            if (stat_b || stat_a) { ({ obj } = values); };
          });
          if (stat_b) {
            it('check object status (before)', () => {
              expect(obj).toStrictEqual(stat_b.obj);
            });
          };
          it('perform ops', () => {
            const { ops, assertQty } = status;
            let result = undefined; let isERR = false;
            try {
              result = runTestFn({ testInst: t_Mod, method }, values);
              //console.log('CHECK: > NO_ERR');
            } catch (err) {
              //console.log('CHECK: > '+err);
              isERR = true;
              result = err;
            } finally {
              expect.assertions(assertQty);
              expect(isERR).toBe(ops.isERR);
              if (isERR) {
                const { errType, errCode } = ops;
                expect((result instanceof errType)).toBe(true);
                expect(result.code).toStrictEqual(errCode);
              } else {
                const { className, value } = ops;
                switch (typeof className) {
                  case 'string' : {
                    expect(result instanceof globalThis[className]).toBe(true);
                    if (className !== 'HTMLElement') break;
                  }
                  default : {
                    expect(result).toStrictEqual(value);
                  }
                };
              };
            };
          });
          if (stat_a) {
            it('check object status (after)', () => {
              expect(obj).toStrictEqual(stat_a.obj);
            });
          };
        });
      });
    });
  });
  //===
});

describe('3. special ops applied to elements', () => {
  describe.each([
    {
      msg: '3.1 - function',
      method: 'insertXObjEList',
    }, {
      msg: '3.2 - function',
      method: 'insertXObjEListEx',
    }, {
      msg: '3.3 - function',
      method: 'deleteXObjAttribute',
    }, {
      msg: '3.4 - function',
      method: 'renameXObjElement',
    }, {
      msg: '3.5 - function',
      method: 'renameXObjAttribute',
    },
  ])('$msg: xObj.$method()', ({ method }) => {
    const testData = t_Dat[method];
    describe.each(testData.tests)('+ $msg $rem', ({ param }) => {
      describe.each(param)('+ $msg', ({ param }) => {
        describe.each(param)('+ $msg $rem', ({ values, status }) => {
          const { before: stat_b, after: stat_a } = status;
          let obj = undefined;
          beforeAll(() => {
            if (stat_b || stat_a) { ({ obj } = values); };
          });
          if (stat_b) {
            it('check object status (before)', () => {
              expect(obj).toStrictEqual(stat_b.obj);
            });
          };
          it('perform ops', () => {
            const { ops, assertQty } = status;
            let result = undefined; let isERR = false;
            try {
              result = runTestFn({ testInst: t_Mod, method }, values);
              //console.log('CHECK: > NO_ERR');
            } catch (err) {
              //console.log('CHECK: > '+err);
              isERR = true;
              result = err;
            } finally {
              expect.assertions(assertQty);
              expect(isERR).toBe(ops.isERR);
              if (isERR) {
                const { errType, errCode } = ops;
                expect((result instanceof errType)).toBe(true);
                expect(result.code).toStrictEqual(errCode);
              } else {
                const { className, value } = ops;
                switch (typeof className) {
                  case 'string' : {
                    expect(result instanceof globalThis[className]).toBe(true);
                    if (className !== 'HTMLElement') break;
                  }
                  default : {
                    expect(result).toStrictEqual(value);
                  }
                };
              };
            };
          });
          if (stat_a) {
            it('check object status (after)', () => {
              expect(obj).toStrictEqual(stat_a.obj);
            });
          };
        });
      });
    });
  });
  //===
});
