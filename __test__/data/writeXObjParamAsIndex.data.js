// [v0.1.003-20240906]

/* module:   `xObj-lib`
 * function: writeXObjParamAsIndex()
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const objTestData = {
  msg: 'run tests against "obj" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: undefined,
            value: 127,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: undefined,
            value: 127,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is an object',
          rem: '',
          values: {
            obj: {},
            value: 127,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __text: '127' },
        },
      },
    }),
  ],
};

const valueTestData = {
  msg: 'run tests against "value" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(failed: "false" returned)',
          values: {
            obj: {},
            value: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {},
        },
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '("-1" will be stored as a value)',
          values: {
            obj: {},
            value: null,
            key: undefined,
          },
        }, {
          msg: 'value is a "NaN"',
          rem: '("-1" will be stored as a value)',
          values: {
            obj: {},
            value: NaN,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __text: '-1' },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is a number',
          rem: '',
          values: {
            obj: {},
            value: 127,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __text: '127' },
        },
      },
    }),
  ],
};

const keyTestData = {
  msg: 'run tests against "key" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(defaults must be used)',
          values: {
            obj: {},
            value: 127,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __text: '127' },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(failed: "false" returned)',
          values: {
            obj: {},
            value: 127,
            key: null,
          },
        }, {
          msg: 'value is an empty string',
          rem: '(failed: "false" returned)',
          values: {
            obj: {},
            value: 127,
            key: '',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {},
        },
      },
    }),
    ...genTestCase({
      inGroup: 'valid args given',
      cases: [
        {
          msg: 'value is a non-empty string',
          rem: '',
          values: {
            obj: {},
            value: 265,
            key: 'some_key',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { some_key: '265' },
        },
      },
    }),
  ],
};

const resultTestData = {
  msg: 'run tests against result value',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'cases when a "value" param is a string',
      cases: [
        {
          msg: 'value convertable to a number',
          rem: '(value accepted)',
          values: {
            obj: {},
            value: '1278',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __text: '1278' },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'cases when a "value" param is a string',
      cases: [
        {
          msg: 'value not convertable to a number',
          rem: '("-1" will be stored as a value)',
          values: {
            obj: {},
            value: 'some not convertable value',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __text: '-1' },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'cases when a "value" param is a number',
      cases: [
        {
          msg: 'value is a negative number',
          rem: '("-1" will be stored as a value)',
          values: {
            obj: {},
            value: -125,
            key: undefined,
          },
        }, {
          msg: 'value not integer',
          rem: '("-1" will be stored as a value)',
          values: {
            obj: {},
            value: 12.5,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __text: '-1' },
        },
      },
    }),
  ],
};

module.exports = {
  //descr: [],
  tests: [
    objTestData,
    valueTestData,
    keyTestData,
    resultTestData,
  ],
};
