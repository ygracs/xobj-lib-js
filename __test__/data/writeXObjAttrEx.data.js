// [v0.1.002-20230224]

/* module:   `xObj-lib`
 * function: writeXObjAttrEx()
 */

const objTestData = {
  msg: 'run tests against "obj" param',
  param: [
    {
      msg: 'no args given',
      param: [
        {
          msg: 'value is undefined',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: undefined,
            attr: 'attr_1',
            value: 256,
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: {
              isErr: true,
              errType: TypeError,
              errCode: 'ERR_XOBJ_NPOBJ',
              value: undefined,
            },
            obj: undefined,
            assertQty: 4,
          },
        },
      ],
    }, {
      msg: 'non valid args given',
      param: [
        {
          msg: 'value is "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: null,
            attr: 'attr_1',
            value: 256,
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: {
              isErr: true,
              errType: TypeError,
              errCode: 'ERR_XOBJ_NPOBJ',
              value: undefined,
            },
            obj: null,
            assertQty: 4,
          },
        },
      ],
    }, {
      msg: 'a valid args given',
      param: [
        {
          msg: 'value is empty object',
          rem: '',
          values: {
            obj: {},
            attr: 'attr_1',
            value: 256,
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: {
              __attr: { attr_1: '256' },
            },
            assertQty: 3,
          },
        },
      ],
    },
  ],
};

const attrTestData = {
  msg: 'run tests against "attr" param',
  param: [
    {
      msg: 'no args given',
      param: [
        {
          msg: 'value is undefined',
          rem: '(function failed: "false" is returned)',
          values: {
            obj: {},
            attr: undefined,
            value: 256,
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: false,
            },
            obj: {},
            assertQty: 3,
          },
        },
      ],
    }, {
      msg: 'non valid args given',
      param: [
        {
          msg: 'value is "null"',
          rem: '(function failed: "false" is returned)',
          values: {
            obj: {},
            attr: null,
            value: 256,
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: false,
            },
            obj: {},
            assertQty: 3,
          },
        }, {
          msg: 'value is an empty string',
          rem: '(function failed: "false" is returned)',
          values: {
            obj: {},
            attr: '',
            value: 256,
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: false,
            },
            obj: {},
            assertQty: 3,
          },
        },
      ],
    }, {
      msg: 'a valid args given',
      param: [
        {
          msg: 'value is a non-empty string',
          rem: '',
          values: {
            obj: {},
            attr: 'attr_2',
            value: 256,
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: {
              __attr: { attr_2: '256' },
            },
            assertQty: 3,
          },
        },
      ],
    },
  ],
};

const valueTestData = {
  msg: 'run tests against "value" param',
  param: [
    {
      msg: 'no args given',
      param: [
        {
          msg: 'value is undefined',
          rem: '(function failed: "false" is returned)',
          values: {
            obj: { __text: 'some value' },
            attr: 'attr_2',
            value: undefined,
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: false,
            },
            obj: { __text: 'some value' },
            assertQty: 3,
          },
        },
      ],
    }, {
      msg: 'invalid args given',
      param: [
        {
          msg: 'value is "null"',
          rem: '(empty string is used as a value)',
          values: {
            obj: {
              __attr: { attr_2: 'some value' },
            },
            attr: 'attr_2',
            value: null,
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: {
              __attr: { attr_2: '' },
            },
            assertQty: 3,
          },
        },
      ],
    }, {
      msg: 'valid args given',
      param: [
        {
          msg: 'value is a boolean',
          rem: '(value converted to a string)',
          values: {
            obj: {
              __attr: { attr_2: 'some value' },
            },
            attr: 'attr_2',
            value: true,
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: {
              __attr: { attr_2: 'true' },
            },
            assertQty: 3,
          },
        }, {
          msg: 'value is a number',
          rem: '(value converted to a string)',
          values: {
            obj: {
              __attr: { attr_2: 'some value' },
            },
            attr: 'attr_2',
            value: 137,
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: {
              __attr: { attr_2: '137' },
            },
            assertQty: 3,
          },
        }, {
          msg: 'value is a string',
          rem: '',
          values: {
            obj: {
              __attr: { attr_2: 'some value' },
            },
            attr: 'attr_2',
            value: 'sone new value',
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: {
              __attr: { attr_2: 'sone new value' },
            },
            assertQty: 3,
          },
        },
      ],
    },
  ],
};

const keyTestData = {
  msg: 'run tests against "key" param',
  param: [
    {
      msg: 'no args given',
      param: [
        {
          msg: 'value is undefined',
          rem: '(a default "XOBJ_DEF_ATTR_TNAME" is used)',
          values: {
            obj: {},
            attr: 'attr_1',
            value: 256,
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: {
              __attr: { attr_1: '256' },
            },
            assertQty: 3,
          },
        },
      ],
    }, {
      msg: 'non valid args given',
      param: [
        {
          msg: 'value is "null"',
          rem: '(function failed: "false" is returned)',
          values: {
            obj: {},
            attr: 'attr_1',
            value: 256,
            defValue: undefined,
            key: null,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: false,
            },
            obj: {},
            assertQty: 3,
          },
        }, {
          msg: 'value is an empty string',
          rem: '(function failed: "false" is returned)',
          values: {
            obj: {},
            attr: 'attr_1',
            value: 256,
            defValue: undefined,
            key: '',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: false,
            },
            obj: {},
            assertQty: 3,
          },
        },
      ],
    }, {
      msg: 'a valid args given',
      param: [
        {
          msg: 'value is a non-empty string',
          rem: '',
          values: {
            obj: {},
            attr: 'attr_2',
            value: 256,
            defValue: undefined,
            key: 'key1',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: {
              key1: { attr_2: '256' },
            },
            assertQty: 3,
          },
        },
      ],
    },
  ],
};

const defsTestData = {
  msg: 'run tests against "defValue" param',
  param: [
    {
      msg: 'no args given',
      param: [
        {
          msg: 'value is undefined',
          rem: '(empty string is used as a value)',
          values: {
            obj: { __text: 'some value' },
            attr: 'attr_2',
            value: null,
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: {
              __attr: { attr_2: '' },
              __text: 'some value',
            },
            assertQty: 3,
          },
        },
      ],
    }, {
      msg: 'invalid args given',
      param: [
        {
          msg: 'value is "null"',
          rem: '(empty string is used as a value)',
          values: {
            obj: {
              __attr: { attr_2: 'some value' },
            },
            attr: 'attr_2',
            value: null,
            defValue: null,
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: {
              __attr: { attr_2: '' },
            },
            assertQty: 3,
          },
        },
      ],
    }, {
      msg: 'valid args given',
      param: [
        {
          msg: 'value is a number',
          rem: '',
          values: {
            obj: {
              __attr: { attr_2: 'some value' },
            },
            attr: 'attr_2',
            value: null,
            defValue: 256,
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: {
              __attr: { attr_2: '256' },
            },
            assertQty: 3,
          },
        }, {
          msg: 'value is a string',
          rem: '',
          values: {
            obj: {
              __attr: { attr_2: 'some value' },
            },
            attr: 'attr_2',
            value: null,
            defValue: 'default value',
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: {
              __attr: { attr_2: 'default value' },
            },
            assertQty: 3,
          },
        },
      ],
    },
  ],
};

const resultTestData = {
  msg: 'run tests against result values',
  param: [
    {
      msg: 'attribute exists',
      param: [
        {
          msg: 'result is "true"',
          rem: '(attribute value will be changed)',
          values: {
            obj: {
              __attr: {
                attr_1: 'value of "attr_1"',
                attr_2: 'value of "attr_2"',
              },
            },
            attr: 'attr_1',
            value: 256,
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: {
              __attr: {
                attr_1: '256',
                attr_2: 'value of "attr_2"',
              },
            },
            assertQty: 3,
          },
        },
      ],
    },
  ],
};

module.exports = {
  descr: [
    objTestData,
    attrTestData,
    valueTestData,
    keyTestData,
    defsTestData,
    resultTestData,
  ],
};
