// [v0.1.011-20240831]

/* module:   `xObj-lib`
 * function: addXObjElement()
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const objTestData = {
  msg: 'run tests against "obj" param',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: undefined,
            name: 'some_key',
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: null,
            name: 'some_key',
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is an empty object',
          rem: '',
          values: {
            obj: {},
            name: 'some_key',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: {
            isSucceed: true,
            item: {},
          },
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { some_key: {} },
        },
      },
    }),
  ],
};

const nameTestData = {
  msg: 'run tests against "name" param',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: {},
            name: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NSTR',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: {
          obj: {},
        },
        after: {
          obj: {},
        },
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: {},
            name: null,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NSTR',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: {
          obj: {},
        },
        after: {
          obj: {},
        },
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is an empty string',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: {},
            name: '',
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_INVARG_KEY',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: {
          obj: {},
        },
        after: {
          obj: {},
        },
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is a not empty string',
          rem: '',
          values: {
            obj: {},
            name: 'some_key',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: {
            isSucceed: true,
            item: {},
          },
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { some_key: {} },
        },
      },
    }),
  ],
};

const resultTestData = {
  msg: 'run tests against result values',
  param: [
    ...genTestCase({
      inGroup: 'cases when a target element exists',
      cases: [
        {
          msg: 'element is an object',
          rem: '(element will be transformed to an array)',
          values: {
            obj: { target_key: {} },
            name: 'target_key',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: {
            isSucceed: true,
            item: {},
          },
        },
        assertQty: 2,
        before: {
          obj: { target_key: {} },
        },
        after: {
          obj: { target_key: [ {}, {} ] },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'cases when a target element exists',
      cases: [
        {
          msg: 'element is an array',
          rem: '(element will be added into an array)',
          values: {
            obj: { target_key: [ {} ] },
            name: 'target_key',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: {
            isSucceed: true,
            item: {},
          },
        },
        assertQty: 2,
        before: {
          obj: { target_key: [ {} ] },
        },
        after: {
          obj: { target_key: [ {}, {} ] },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'cases when a target element exists',
      cases: [
        {
          msg: 'element is a "null"',
          rem: '(new element will be assinged to a target key)',
          values: {
            obj: { target_key: null },
            name: 'target_key',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: {
            isSucceed: true,
            item: {},
          },
        },
        assertQty: 2,
        before: {
          obj: { target_key: null },
        },
        after: {
          obj: { target_key: {} },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'cases when a target element exists',
      cases: [
        {
          msg: 'element is a string',
          rem: '(failed: no changes applied)',
          values: {
            obj: { target_key: 'some string' },
            name: 'target_key',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: {
            isSucceed: false,
            item: null,
          },
        },
        assertQty: 2,
        before: {
          obj: { target_key: 'some string' },
        },
        after: {
          obj: { target_key: 'some string' },
        },
      },
    }),
  ],
};

module.exports = {
  //descr: [],
  tests: [
    objTestData,
    nameTestData,
    resultTestData,
  ],
};
