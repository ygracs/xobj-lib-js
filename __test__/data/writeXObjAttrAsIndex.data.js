// [v0.1.003-20240906]

/* module:   `xObj-lib`
 * function: writeXObjAttrAsIndex()
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const objTestData = {
  msg: 'run tests against "obj" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: undefined,
            attr: 'some_attr',
            value: 127,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: null,
            attr: 'some_attr',
            value: 172,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is an object',
          rem: '',
          values: {
            obj: {},
            attr: 'some_attr',
            value: 256,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __attr: { some_attr: '256' } },
        },
      },
    }),
  ],
};

const attrTestData = {
  msg: 'run tests against "attr" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(failed: "false" returned)',
          values: {
            obj: {},
            attr: undefined,
            value: 172,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {},
        },
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(failed: "false" returned)',
          values: {
            obj: {},
            attr: null,
            value: 127,
            key: undefined,
          },
        }, {
          msg: 'value is an empty string',
          rem: '(failed: "false" returned)',
          values: {
            obj: {},
            attr: '',
            value: 256,
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {},
        },
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is a non-empty string',
          rem: '',
          values: {
            obj: {},
            attr: 'some_attr',
            value: 256,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __attr: { some_attr: '256' } },
        },
      },
    }),
  ],
};

const valueTestData = {
  msg: 'run tests against "value" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(failed: "false" returned)',
          values: {
            obj: {},
            attr: 'some_attr',
            value: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {},
        },
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '("-1" will be stored as a value)',
          values: {
            obj: {},
            attr: 'some_attr',
            value: null,
            key: undefined,
          },
        }, {
          msg: 'value is a "NaN"',
          rem: '("-1" will be stored as a value)',
          values: {
            obj: {},
            attr: 'some_attr',
            value: NaN,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __attr: { some_attr: '-1' } },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is a number',
          rem: '',
          values: {
            obj: {},
            attr: 'some_attr',
            value: 127,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __attr: { some_attr: '127' } },
        },
      },
    }),
  ],
};

const keyTestData = {
  msg: 'run tests against "key" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(defaults must be used)',
          values: {
            obj: {},
            attr: 'some_attr',
            value: 127,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __attr: { some_attr: '127' } },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(failed: "false" returned)',
          values: {
            obj: {},
            attr: 'some_attr',
            value: 256,
            key: null,
          },
        }, {
          msg: 'value is an empty string',
          rem: '(failed: "false" returned)',
          values: {
            obj: {},
            attr: 'some_attr',
            value: 256,
            key: '',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {},
        },
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is a non-empty string',
          rem: '',
          values: {
            obj: {},
            attr: 'some_attr',
            value: 256,
            key: 'target_key',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { target_key: { some_attr: '256' } },
        },
      },
    }),
  ],
};

const resultTestData = {
  msg: 'run tests against result values',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'cases when a "value" param is a string',
      cases: [
        {
          msg: 'value is a string convertable to a number',
          rem: '(value accepted)',
          values: {
            obj: {},
            attr: 'some_attr',
            value: '1248',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __attr: { some_attr: '1248' } },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'cases when a "value" param is a string',
      cases: [
        {
          msg: 'value is a string not convertable to a number',
          rem: '("-1" will be stored as a value)',
          values: {
            obj: {},
            attr: 'some_attr',
            value: 'not number',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __attr: { some_attr: '-1' } },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'cases when a "value" param is a number',
      cases: [
        {
          msg: 'value is a negative number',
          rem: '()"-1" will be stored as a value)',
          values: {
            obj: {},
            attr: 'some_attr',
            value: -125,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __attr: { some_attr: '-1' } },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'cases for attribute container type',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            obj: { __attr: null },
            attr: 'some_attr',
            value: 127,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: { __attr: null },
        },
        after: {
          obj: { __attr: { some_attr: '127' } },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'cases for attribute container type',
      cases: [
        {
          msg: 'value is a boolean',
          rem: '',
          values: {
            obj: { __attr: false },
            attr: 'some_attr',
            value: 127,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: { __attr: false },
        },
        after: {
          obj: { __attr: { some_attr: '127' } },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'cases for attribute container type',
      cases: [
        {
          msg: 'value is an object',
          rem: '',
          values: {
            obj: { __attr: {} },
            attr: 'some_attr',
            value: 127,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: { __attr: {} },
        },
        after: {
          obj: { __attr: { some_attr: '127' } },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'cases for attribute container type',
      cases: [
        {
          msg: 'value is an array',
          rem: '',
          values: {
            obj: { __attr: [] },
            attr: 'some_attr',
            value: 127,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: { __attr: [] },
        },
        after: {
          obj: { __attr: { some_attr: '127' } },
        },
      },
    }),
  ],
};

module.exports = {
  //descr: [],
  tests: [
    objTestData,
    attrTestData,
    valueTestData,
    keyTestData,
    resultTestData,
  ],
};
