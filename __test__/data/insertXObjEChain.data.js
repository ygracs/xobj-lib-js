// [v0.1.009-20240914]

/* module:   `xObj-lib`
 * function: insertXObjEChain()
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const objTestDescr = {
  msg: 'run tests against "obj" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(a "TypeError" is thrown)',
          values: [
            /* obj */
            undefined,
            /* keys */
            ...[ 'key1', 'key2' ],
            /* options */
            //undefined,
          ],
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(a "TypeError" is thrown)',
          values: [
            /* obj */
            null,
            /* keys */
            ...[ 'key1', 'key2' ],
            /* options */
            //undefined,
          ],
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is an empty object',
          rem: '',
          values: [
            /* obj */
            {},
            /* keys */
            ...[ 'key1', 'key2' ],
            /* options */
            //undefined,
          ],
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: {},
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { key1: { key2: {} } },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'special cases',
      cases: [
        {
          msg: 'value is an array',
          rem: '(a "TypeError" is thrown)',
          values: [
            /* obj */
            [],
            /* keys */
            ...[ 'key1', 'key2' ],
            /* options */
            //undefined,
          ],
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
  ],
};

const keyTestDescr = {
  msg: 'run tests against "key" list',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'perform ops',
          rem: '(function failed: "null" is returned)',
          values: [
            /* obj */
            {},
            /* keys */
            ...[],
            /* options */
            //undefined,
          ],
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {},
        },
      },
    }),
    ...genTestCase({
      inGroup: 'some args are given',
      cases: [
        {
          msg: 'some of the given keys has a non-valid value',
          rem: '(failed: "null" returned)',
          values: [
            /* obj */
            {},
            /* keys */
            ...[ 'key1', null, true, 10, 'key2' ],
            /* options */
            //undefined,
          ],
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { key1: {} },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'some args are given',
      cases: [
        {
          msg: 'all of the given keys has a valid value',
          rem: '(only one element in list)',
          values: [
            /* obj */
            {},
            /* keys */
            ...[ 'key1' ],
            /* options */
            //undefined,
          ],
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: {},
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { key1: {} },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'some args are given',
      cases: [
        {
          msg: 'all of the given keys has a valid value',
          rem: '(more than one element in list)',
          values: [
            /* obj */
            {},
            /* keys */
            ...[ 'key1', 'key2' ],
            /* options */
            //undefined,
          ],
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: {},
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { key1: { key2: {} } },
        },
      },
    }),
  ],
};

const resultTestDescr = {
  msg: 'run tests against result values',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'some of the keys are exists',
      cases: [
        {
          msg: 'element is a ref to object',
          rem: '',
          values: [
            /* obj */
            { prm2: { __text: 'prm2' } },
            /* keys */
            ...[ 'prm2', 'prm23' ],
            /* options */
            //undefined,
          ],
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: {},
        },
        assertQty: 2,
        before: {
          obj: { prm2: { __text: 'prm2' } },
        },
        after: {
          obj: {
            prm2: {
              __text: 'prm2',
              prm23: {},
            },
          },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'some of the keys are exists',
      cases: [
        {
          msg: 'element is a ref to non-object',
          rem: '(failed: "null" returned) [*]',
          values: [
            /* obj */
            { prm2: { prm23: 'prm23 value' } },
            /* keys */
            ...[ 'prm2', 'prm23', 'prm3' ],
            /* options */
            //undefined,
          ],
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
        },
        assertQty: 2,
        before: {
          obj: { prm2: { prm23: 'prm23 value' } },
        },
        after: {
          obj: {
            prm2: {
              prm23: 'prm23 value',
            },
          },
        },
      },
    }),
  ],
};

const optTestDescr = {
  msg: 'run tests against "options" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'options: force = "true"',
      cases: [
        {
          msg: 'element exists and is a ref to non-object',
          rem: '(passed: element replaced)',
          values: [
            /* obj */
            {
              prm1: 'key exists',
              prm3: {},
            },
            /* keys */
            ...[ 'prm1', 'prm2' ],
            /* options */
            { force: true },
          ],
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: {},
        },
        assertQty: 2,
        before: {
          obj: {
            prm1: 'key exists',
            prm3: {},
          },
        },
        after: {
          obj: {
            prm1: { prm2: {} },
            prm3: {},
          },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'options: force = "true"',
      cases: [
        {
          msg: 'element exists and is a ref to object',
          rem: '(passed: element returned)',
          values: [
            /* obj */
            {
              prm1: { prm2: { __text: 'key exists' } },
              prm3: {},
            },
            /* keys */
            ...[ 'prm1', 'prm2' ],
            /* options */
            { force: true },
          ],
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: { __text: 'key exists' },
        },
        assertQty: 2,
        before: {
          obj: {
            prm1: { prm2: { __text: 'key exists' } },
            prm3: {},
          },
        },
        after: {
          obj: {
            prm1: { prm2: { __text: 'key exists' } },
            prm3: {},
          },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'options: force = "true"',
      cases: [
        {
          msg: 'element exists and is a ref to array',
          rem: '(failed: "null" returned)',
          values: [
            /* obj */
            {
              prm1: [ 'key exists' ],
              prm3: {},
            },
            /* keys */
            ...[ 'prm1', 'prm2' ],
            /* options */
            { force: true },
          ],
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
        },
        assertQty: 2,
        before: {
          obj: {
            prm1: [ 'key exists' ],
            prm3: {},
          },
        },
        after: {
          obj: {
            prm1: [ 'key exists' ],
            prm3: {},
          },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'options: force = "true" & ripOldies = "true"',
      cases: [
        {
          msg: 'element exists and is a ref to object',
          rem: '(passed: element replaced)',
          values: [
            /* obj */
            {
              prm1: { prm2: { __text: 'key exists' } },
              prm3: {},
            },
            /* keys */
            ...[ 'prm1', 'prm2' ],
            /* options */
            { force: true, ripOldies: true },
          ],
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: {},
        },
        assertQty: 2,
        before: {
          obj: {
            prm1: { prm2: { __text: 'key exists' } },
            prm3: {},
          },
        },
        after: {
          obj: {
            prm1: { prm2: {} },
            prm3: {},
          },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'options: force = "true" & ripOldies = "true"',
      cases: [
        {
          msg: 'element exists and is a ref to array',
          rem: '(passed: element replaced)',
          values: [
            /* obj */
            {
              prm1: [ 'key exists' ],
              prm3: {},
            },
            /* keys */
            ...[ 'prm1', 'prm2' ],
            /* options */
            { force: true, ripOldies: true },
          ],
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: {},
        },
        assertQty: 2,
        before: {
          obj: {
            prm1: [ 'key exists' ],
            prm3: {},
          },
        },
        after: {
          obj: {
            prm1: { prm2: {} },
            prm3: {},
          },
        },
      },
    }),
  ],
};

module.exports = {
  //descr: [],
  tests: [
    objTestDescr,
    keyTestDescr,
    optTestDescr,
    resultTestDescr,
  ],
};
