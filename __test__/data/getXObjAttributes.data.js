// [v0.1.006-20240831]

/* module:   `xObj-lib`
 * function: getXObjAttributes()
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const objTestData = {
  msg: 'run tests against "obj" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: null,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is an object',
          rem: '',
          values: {
            obj: {
              __attr: { attr_1: 'value of attr' },
            },
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: { attr_1: 'value of attr' },
        },
        assertQty: 2,
      },
    }),
  ],
};

const keyTestData = {
  msg: 'run tests against "key" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(a default key value must be applied)',
          values: {
            obj: {
              __attr: { attr_1: 'value of attr_1' },
            },
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: { attr_1: 'value of attr_1' },
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(failed: "null" returned)',
          values: {
            obj: {
              __attr: { attr_1: 'value of attr_1' },
            },
            key: null,
          },
        }, {
          msg: 'value is an empty string',
          rem: '(failed: "null" returned)',
          values: {
            obj: {
              __attr: { attr_1: 'value of attr_1' },
            },
            key: null,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is a not empty string',
          rem: '',
          values: {
            obj: {
              __attr: { attr_1: 'value of attr_1' },
              target_key: { attr_2: 'value of attr_2' },
            },
            key: 'target_key',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: { attr_2: 'value of attr_2' },
        },
        assertQty: 2,
      },
    }),
  ],
};

const resultTestData = {
  msg: 'run tests against result values',
  param: [
    ...genTestCase({
      inGroup: 'test cases against existence of a target element',
      cases: [
        {
          msg: 'key is not exists',
          rem: '(result must be a "null")',
          values: {
            obj: {
              __attr: { attr_1: 'value of attr_1' },
              target_key: { attr_2: 'value of attr_2' },
            },
            key: 'absent_key',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'test cases against existence of a target element',
      cases: [
        {
          msg: 'key is exists',
          rem: '',
          values: {
            obj: {
              __attr: { attr_1: 'value of attr_1' },
              target_key: { attr_2: 'value of attr_2' },
            },
            key: 'target_key',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: { attr_2: 'value of attr_2' },
        },
        assertQty: 2,
      },
    }),
  ],
};

module.exports = {
  //descr: [],
  tests: [
    objTestData,
    keyTestData,
    resultTestData,
  ],
};
