// [v0.1.002-20230225]

/* module:   `xObj-lib`
 * function: writeXObjParamEx()
 */

const objTestData = {
  msg: 'run tests against "obj" param',
  param: [
    {
      msg: 'no args given',
      param: [
        {
          msg: 'value is undefined',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: undefined,
            value: 'some new value',
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: {
              isErr: true,
              errType: TypeError,
              errCode: 'ERR_XOBJ_NPOBJ',
              value: undefined,
            },
            obj: undefined,
            assertQty: 4,
          },
        },
      ],
    }, {
      msg: 'non valid args given',
      param: [
        {
          msg: 'value is "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: null,
            value: 'some new value',
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: {
              isErr: true,
              errType: TypeError,
              errCode: 'ERR_XOBJ_NPOBJ',
              value: undefined,
            },
            obj: null,
            assertQty: 4,
          },
        },
      ],
    }, {
      msg: 'a valid args given',
      param: [
        {
          msg: 'value is proper object',
          rem: '',
          values: {
            obj: { __text: 'some value' },
            value: 'some new value',
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: { __text: 'some new value' },
            assertQty: 3,
          },
        },
      ],
    },
  ],
};

const valueTestData = {
  msg: 'run tests against "value" param',
  param: [
    {
      msg: 'no args given',
      param: [
        {
          msg: 'value is undefined',
          rem: '(function failed: "false" is returned)',
          values: {
            obj: { __text: 'some value' },
            value: undefined,
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: false,
            },
            obj: { __text: 'some value' },
            assertQty: 3,
          },
        },
      ],
    }, {
      msg: 'non valid args given',
      param: [
        {
          msg: 'value is "null"',
          rem: '(empty string will be stored as a value)',
          values: {
            obj: { __text: 'some value' },
            value: null,
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: { __text: '' },
            assertQty: 3,
          },
        },
      ],
    }, {
      msg: 'valid args given',
      param: [
        {
          msg: 'value is a boolean',
          rem: '(value converted to a string)',
          values: {
            obj: { __text: 'some value' },
            value: true,
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: { __text: 'true' },
            assertQty: 3,
          },
        }, {
          msg: 'value is a number',
          rem: '(value converted to a string)',
          values: {
            obj: { __text: 'some value' },
            value: 256,
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: { __text: '256' },
            assertQty: 3,
          },
        }, {
          msg: 'value is a string',
          rem: '',
          values: {
            obj: { __text: 'some value' },
            value: 'some new value',
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: { __text: 'some new value' },
            assertQty: 3,
          },
        },
      ],
    },
  ],
};

const keyTestData = {
  msg: 'run tests against "key" param',
  param: [
    {
      msg: 'no args given',
      param: [
        {
          msg: 'value is undefined',
          rem: '(a default "XOBJ_DEF_PARAM_TNAME" is used)',
          values: {
            obj: { __text: 'some value' },
            value: 'some new value',
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: { __text: 'some new value' },
            assertQty: 3,
          },
        },
      ],
    }, {
      msg: 'non valid args given',
      param: [
        {
          msg: 'value is "null"',
          rem: '(function failed: "false" is returned)',
          values: {
            obj: { __text: 'some value' },
            value: 'some new value',
            defValue: undefined,
            key: null,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: false,
            },
            obj: { __text: 'some value' },
            assertQty: 3,
          },
        }, {
          msg: 'value is an empty string',
          rem: '(function failed: "false" is returned)',
          values: {
            obj: { __text: 'some value' },
            value: 'some new value',
            defValue: undefined,
            key: '',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: false,
            },
            obj: { __text: 'some value' },
            assertQty: 3,
          },
        },
      ],
    }, {
      msg: 'a valid args given',
      param: [
        {
          msg: 'value is a non-empty string',
          rem: '(key exists)',
          values: {
            obj: {
              __text: 'some value',
              key1: 'value of "key1"',
            },
            value: 'some new value',
            defValue: undefined,
            key: 'key1',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: {
              __text: 'some value',
              key1: 'some new value',
            },
            assertQty: 3,
          },
        },
      ],
    },
  ],
};

const defsTestData = {
  msg: 'run tests against "defValue" param',
  param: [
    {
      msg: 'no args given',
      param: [
        {
          msg: 'value is undefined',
          rem: '(empty string will be stored as a value)',
          values: {
            obj: { __text: 'some value' },
            value: null,
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: { __text: '' },
            assertQty: 3,
          },
        },
      ],
    }, {
      msg: 'non valid args given',
      param: [
        {
          msg: 'value is "null"',
          rem: '(empty string will be stored as a value)',
          values: {
            obj: { __text: 'some value' },
            value: null,
            defValue: null,
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: { __text: '' },
            assertQty: 3,
          },
        },
      ],
    }, {
      msg: 'a valid args given',
      param: [
        {
          msg: 'value is a boolean',
          rem: '(value converted to a string)',
          values: {
            obj: {
              __text: 'some value',
              key1: 'value of "key1"',
            },
            value: null,
            defValue: true,
            key: 'key1',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: {
              __text: 'some value',
              key1: 'true',
            },
            assertQty: 3,
          },
        }, {
          msg: 'value is a number',
          rem: '(value converted to a string)',
          values: {
            obj: {
              __text: 'some value',
              key1: 'value of "key1"',
            },
            value: null,
            defValue: 128,
            key: 'key1',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: {
              __text: 'some value',
              key1: '128',
            },
            assertQty: 3,
          },
        }, {
          msg: 'value is a string',
          rem: '',
          values: {
            obj: {
              __text: 'some value',
              key1: 'value of "key1"',
            },
            value: null,
            defValue: 'some default value',
            key: 'key1',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: {
              __text: 'some value',
              key1: 'some default value',
            },
            assertQty: 3,
          },
        },
      ],
    },
  ],
};

const resultTestData = {
  msg: 'run tests against result values',
  param: [
    {
      msg: 'key not exists',
      param: [
        {
          msg: 'result is "true"',
          rem: '(new target key will be inserted)',
          values: {
            obj: { __text: 'some value' },
            value: 'some new value',
            defValue: undefined,
            key: 'key1',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: {
              __text: 'some value',
              key1: 'some new value',
            },
            assertQty: 3,
          },
        },
      ],
    },
  ],
};

module.exports = {
  descr: [
    objTestData,
    valueTestData,
    keyTestData,
    defsTestData,
    resultTestData,
  ],
};
