// [v0.1.006-20240910]

/* module:   `xObj-lib`
 * function: renameXObjElement()
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const objTestData = {
  msg: 'run tests against "obj" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: undefined,
            name: 'some_key',
            newName: 'new_name',
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: null,
            name: 'some_key',
            newName: 'new_name',
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is a non-empty object',
          rem: '',
          values: {
            obj: { some_key: { __text: 'some text' } },
            name: 'some_key',
            newName: 'new_name',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: { some_key: { __text: 'some text' } },
        },
        after: {
          obj: { new_name: { __text: 'some text' } },
        },
      },
    }),
  ],
};

const keyTestData1 = {
  msg: 'run tests against "name" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(failed: "false" returned)',
          values: {
            obj: { some_key: { __text: 'some text' } },
            name: undefined,
            newName: 'new_name',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: { some_key: { __text: 'some text' } },
        },
        after: {
          obj: { some_key: { __text: 'some text' } },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: { some_key: { __text: 'some text' } },
            name: null,
            newName: 'new_name',
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NSTR',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is an empty string',
          rem: '(failed: "false" returned)',
          values: {
            obj: { some_key: { __text: 'some text' } },
            name: '',
            newName: 'new_name',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: { some_key: { __text: 'some text' } },
        },
        after: {
          obj: { some_key: { __text: 'some text' } },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is a non-empty string',
          rem: '',
          values: {
            obj: { some_key: { __text: 'some text' } },
            name: 'some_key',
            newName: 'new_name',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: { some_key: { __text: 'some text' } },
        },
        after: {
          obj: { new_name: { __text: 'some text' } },
        },
      },
    }),
  ],
};

const keyTestData2 = {
  msg: 'run tests against "newName" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(failed: "false" returned)',
          values: {
            obj: { some_key: { __text: 'some text' } },
            name: 'some_key',
            newName: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: { some_key: { __text: 'some text' } },
        },
        after: {
          obj: { some_key: { __text: 'some text' } },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: { some_key: { __text: 'some text' } },
            name: 'some_key',
            newName: null,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NSTR',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is an empty string',
          rem: '(failed: "false" returned)',
          values: {
            obj: { some_key: { __text: 'some text' } },
            name: 'some_key',
            newName: '',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: { some_key: { __text: 'some text' } },
        },
        after: {
          obj: { some_key: { __text: 'some text' } },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is a non-empty string',
          rem: '',
          values: {
            obj: { some_key: { __text: 'some text' } },
            name: 'some_key',
            newName: 'new_name',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: { some_key: { __text: 'some text' } },
        },
        after: {
          obj: { new_name: { __text: 'some text' } },
        },
      },
    }),
  ],
};

const resultTestData = {
  msg: 'run tests against result values',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'case where old and new name are equal',
      cases: [
        {
          msg: 'perform test',
          rem: '(passed: "true" returned)',
          values: {
            obj: { some_key: { __text: 'some text' } },
            name: 'some_key',
            newName: 'some_key',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: { some_key: { __text: 'some text' } },
        },
        after: {
          obj: { some_key: { __text: 'some text' } },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'tests against element existance',
      cases: [
        {
          msg: 'target element is not exists',
          rem: '(failed: "false" returned)',
          values: {
            obj: { some_key: { __text: 'some text' } },
            name: 'target_key',
            newName: 'new_name',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: { some_key: { __text: 'some text' } },
        },
        after: {
          obj: { some_key: { __text: 'some text' } },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'tests against element existance',
      cases: [
        {
          msg: 'target element is exists',
          rem: '(failed: "false" returned)',
          values: {
            obj: {
              some_key: { __text: 'some text' },
              target_key: { __text: 'some text' },
            },
            name: 'target_key',
            newName: 'new_name',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {
            some_key: { __text: 'some text' },
            target_key: { __text: 'some text' },
          },
        },
        after: {
          obj: {
            some_key: { __text: 'some text' },
            new_name: { __text: 'some text' },
          },
        },
      },
    }),
  ],
};

module.exports = {
  //descr: [],
  tests: [
    objTestData,
    keyTestData1,
    keyTestData2,
    resultTestData,
  ],
};
