// [v0.1.001-20240910]

/* module:   `xObj-lib`
 * function: renameXObjAttribute()
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const objTestData = {
  msg: 'run tests against "obj" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: undefined,
            attr: 'some_attr',
            newName: 'new_name',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: null,
            attr: 'some_attr',
            newName: 'new_name',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is proper object',
          rem: '',
          values: {
            obj: {
              __attr: { some_attr: 'attribute value' },
            },
            attr: 'some_attr',
            newName: 'new_name',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {
            __attr: { some_attr: 'attribute value' },
          },
        },
        after: {
          obj: {
            __attr: { new_name: 'attribute value' },
          },
        },
      },
    }),
  ],
};

const attrTestData = {
  msg: 'run tests against "attr" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(failed: "false" returned)',
          values: {
            obj: {
              __attr: { some_attr: 'attribute value' },
            },
            attr: undefined,
            newName: 'new_name',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: {
            __attr: { some_attr: 'attribute value' },
          },
        },
        after: {
          obj: {
            __attr: { some_attr: 'attribute value' },
          },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: {
              __attr: { some_attr: 'attribute value' },
            },
            attr: null,
            newName: 'new_name',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NSTR',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is an empty string',
          rem: '(failed: "false" returned)',
          values: {
            obj: {
              __attr: { some_attr: 'attribute value' },
            },
            attr: '',
            newName: 'new_name',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: {
            __attr: { some_attr: 'attribute value' },
          },
        },
        after: {
          obj: {
            __attr: { some_attr: 'attribute value' },
          },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is a non-empty string',
          rem: '',
          values: {
            obj: {
              __attr: { some_attr: 'attribute value' },
            },
            attr: 'some_attr',
            newName: 'new_name',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {
            __attr: { some_attr: 'attribute value' },
          },
        },
        after: {
          obj: {
            __attr: { new_name: 'attribute value' },
          },
        },
      },
    }),
  ],
};

const nameTestData = {
  msg: 'run tests against "newName" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(failed: "false" returned)',
          values: {
            obj: {
              __attr: { some_attr: 'attribute value' },
            },
            attr: 'some_attr',
            newName: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: {
            __attr: { some_attr: 'attribute value' },
          },
        },
        after: {
          obj: {
            __attr: { some_attr: 'attribute value' },
          },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: {
              __attr: { some_attr: 'attribute value' },
            },
            attr: 'some_attr',
            newName: null,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NSTR',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is an empty string',
          rem: '(failed: "false" returned)',
          values: {
            obj: {
              __attr: { some_attr: 'attribute value' },
            },
            attr: 'some_attr',
            newName: '',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: {
            __attr: { some_attr: 'attribute value' },
          },
        },
        after: {
          obj: {
            __attr: { some_attr: 'attribute value' },
          },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is a non-empty string',
          rem: '',
          values: {
            obj: {
              __attr: { some_attr: 'attribute value' },
            },
            attr: 'some_attr',
            newName: 'new_name',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {
            __attr: { some_attr: 'attribute value' },
          },
        },
        after: {
          obj: {
            __attr: { new_name: 'attribute value' },
          },
        },
      },
    }),
  ],
};

const keyTestData = {
  msg: 'run tests against "key" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(defaults must be used)',
          values: {
            obj: {
              __attr: { some_attr: 'attribute value' },
            },
            attr: 'some_attr',
            newName: 'new_name',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {
            __attr: { some_attr: 'attribute value' },
          },
        },
        after: {
          obj: {
            __attr: { new_name: 'attribute value' },
          },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: {
              __attr: { some_attr: 'attribute value' },
            },
            attr: 'some_attr',
            newName: 'new_name',
            key: null,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NSTR',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is an empty string',
          rem: '(failed: "false" returned)',
          values: {
            obj: {
              __attr: { some_attr: 'attribute value' },
            },
            attr: 'some_attr',
            newName: 'new_name',
            key: '',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: {
            __attr: { some_attr: 'attribute value' },
          },
        },
        after: {
          obj: {
            __attr: { some_attr: 'attribute value' },
          },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is a non-empty string',
          rem: '',
          values: {
            obj: {
              __attr: { some_attr: 'attribute value' },
              target_obj: { target_attr: 'target value' },
            },
            attr: 'target_attr',
            newName: 'new_name',
            key: 'target_obj',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {
            __attr: { some_attr: 'attribute value' },
            target_obj: { target_attr: 'target value' },
          },
        },
        after: {
          obj: {
            __attr: { some_attr: 'attribute value' },
            target_obj: { new_name: 'target value' },
          },
        },
      },
    }),
  ],
};

const resultTestData = {
  msg: 'run tests against result values',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'case where "attr" and "newName" params are equal',
      cases: [
        {
          msg: 'perform test',
          rem: '(passed: "true" returned)',
          values: {
            obj: {
              __attr: { some_attr: 'attribute value' },
            },
            attr: 'some_attr',
            newName: 'some_attr',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {
            __attr: { some_attr: 'attribute value' },
          },
        },
        after: {
          obj: {
            __attr: { some_attr: 'attribute value' },
          },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'tests against attribute existance',
      cases: [
        {
          msg: 'target attribute is not exists',
          rem: '(failed: "false" returned)',
          values: {
            obj: {
              __attr: { some_attr: 'attribute value' },
            },
            attr: 'target_attr',
            newName: 'new_name',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: {
            __attr: { some_attr: 'attribute value' },
          },
        },
        after: {
          obj: {
            __attr: { some_attr: 'attribute value' },
          },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'tests against attribute existance',
      cases: [
        {
          msg: 'attributes container is not present',
          rem: '(failed: "false" returned)',
          values: {
            obj: {},
            attr: 'target_attr',
            newName: 'new_name',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {},
        },
      },
    }),
  ],
};

module.exports = {
  //descr: [],
  tests: [
    objTestData,
    attrTestData,
    nameTestData,
    keyTestData,
    resultTestData,
  ],
};
