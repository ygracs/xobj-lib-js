// [v0.1.011-20230205]

/* module:   `xObj-lib`
 * function: ---
 */

module.exports = {
  readXObjParamRaw: require('./readXObjParamRaw.data.js'),
  readXObjAttrRaw: require('./readXObjAttrRaw.data.js'),
  readXObjParam: require('./readXObjParam.data.js'),
  readXObjAttr: require('./readXObjAttr.data.js'),
  readXObjParamAsBool: require('./readXObjParamAsBool.data.js'),
  readXObjParamAsNum: require('./readXObjParamAsNum.data.js'),
  readXObjParamAsStr: require('./readXObjParamAsStr.data.js'),
  readXObjParamAsIndex: require('./readXObjParamAsIndex.data.js'),
  readXObjAttrAsBool: require('./readXObjAttrAsBool.data.js'),
  readXObjAttrAsNum: require('./readXObjAttrAsNum.data.js'),
  readXObjAttrAsStr: require('./readXObjAttrAsStr.data.js'),
  readXObjAttrAsIndex: require('./readXObjAttrAsIndex.data.js'),
};
