// [v0.1.010-20240904]

/* module:   `xObj-lib`
 * function: readXObjAttrRaw()
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const objTestData = {
  msg: 'run tests against "obj" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: undefined,
            attr: 'some_attr',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: null,
            attr: 'some_attr',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is proper object',
          rem: '',
          values: {
            obj: {
              __attr: { some_attr: 'attribute value' },
            },
            attr: 'some_attr',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 'attribute value',
        },
        assertQty: 2,
      },
    }),
  ],
};

const attrTestData = {
  msg: 'run tests against "attr" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(failed: "undefined" returned)',
          values: {
            obj: {
              __attr: { some_attr: 'attribute value' },
            },
            attr: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: undefined,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: {
              __attr: { some_attr: 'attribute value' },
            },
            attr: null,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NSTR',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is an empty string',
          rem: '(failed: "undefined" returned)',
          values: {
            obj: {
              __attr: { some_attr: 'attribute value' },
            },
            attr: '',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: undefined,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is a non-empty string',
          rem: '',
          values: {
            obj: {
              __attr: { some_attr: 'attribute value' },
            },
            attr: 'some_attr',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 'attribute value',
        },
        assertQty: 2,
      },
    }),
  ],
};

const keyTestData = {
  msg: 'run tests against "key" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(defaults must be used)',
          values: {
            obj: {
              __attr: { some_attr: 'attribute value' },
            },
            attr: 'some_attr',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 'attribute value',
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(failed: "undefined" returned)',
          values: {
            obj: {
              __attr: { some_attr: 'attribute value' },
            },
            attr: 'some_attr',
            key: null,
          },
        }, {
          msg: 'value is an empty string',
          rem: '(failed: "undefined" returned)',
          values: {
            obj: {
              __attr: { some_attr: 'attribute value' },
            },
            attr: 'some_attr',
            key: '',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: undefined,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is a non-empty string',
          rem: '',
          values: {
            obj: {
              __attr: { some_attr: 'attribute value' },
              target_obj: { target_attr: 'target value' },
            },
            attr: 'target_attr',
            key: 'target_obj',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 'target value',
        },
        assertQty: 2,
      },
    }),
  ],
};

const resultTestData = {
  msg: 'run tests against result values',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'tests against attribute existance',
      cases: [
        {
          msg: 'target attribute is not exists',
          rem: '(failed: "undefined" returned)',
          values: {
            obj: {
              __attr: { some_attr: 'attribute value' },
            },
            attr: 'target_attr',
            key: undefined,
          },
        }, {
          msg: 'attributes container is not present',
          rem: '(failed: "undefined" returned)',
          values: {
            obj: {},
            attr: 'target_attr',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: undefined,
        },
        assertQty: 2,
      },
    }),
  ],
};

module.exports = {
  //descr: [],
  tests: [
    objTestData,
    attrTestData,
    keyTestData,
    resultTestData,
  ],
};
