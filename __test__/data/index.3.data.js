// [v0.1.010-20240911]

/* module:   `xObj-lib`
 * function: ---
 */

module.exports = {
  getXObjAttributes: require('./getXObjAttributes.data.js'),
  checkXObjAttribute: require('./checkXObjAttribute.data.js'),
  deleteXObjAttribute: require('./deleteXObjAttribute.data.js'),
  renameXObjAttribute: require('./renameXObjAttribute.data.js'),
  getXObjElement: require('./getXObjElement.data.js'),
  addXObjElement: require('./addXObjElement.data.js'),
  insertXObjElement: require('./insertXObjElement.data.js'),
  insertXObjElementEx: require('./insertXObjElementEx.data.js'),
  deleteXObjElement: require('./deleteXObjElement.data.js'),
  deleteXObjElementEx: require('./deleteXObjElementEx.data'),
  renameXObjElement: require('./renameXObjElement.data.js'),
  insertXObjEList: require('./insertXObjEList.data.js'),
  insertXObjEListEx: require('./insertXObjEListEx.data.js'),
};
