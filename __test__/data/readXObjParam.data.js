// [v0.1.002-20230226]

/* module:   `xObj-lib`
 * function: readXObjParam()
 */

const objTestData = {
  msg: 'run tests against "obj" param',
  param: [
    {
      msg: 'no args given',
      param: [
        {
          msg: 'value is undefined',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: undefined,
            key: undefined,
          },
          status: {
            ops: {
              isErr: true,
              errType: TypeError,
              errCode: 'ERR_XOBJ_NPOBJ',
              value: undefined,
            },
            obj: undefined,
            assertQty: 3,
          },
        },
      ],
    }, {
      msg: 'non valid args given',
      param: [
        {
          msg: 'value is "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: null,
            key: undefined,
          },
          status: {
            ops: {
              isErr: true,
              errType: TypeError,
              errCode: 'ERR_XOBJ_NPOBJ',
              value: undefined,
            },
            obj: undefined,
            assertQty: 3,
          },
        },
      ],
    }, {
      msg: 'a valid args given',
      param: [
        {
          msg: 'value is proper object',
          rem: '',
          values: {
            obj: { __text: 'some value', },
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: 'some value',
            },
            obj: undefined,
            assertQty: 2,
          },
        },
      ],
    },
  ],
};

const keyTestData = {
  msg: 'run tests against "key" param',
  param: [
    {
      msg: 'no args given',
      param: [
        {
          msg: 'value is undefined',
          rem: '(a default "XOBJ_DEF_PARAM_TNAME" is used)',
          values: {
            obj: { __text: 'some value' },
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: 'some value',
            },
            obj: undefined,
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'non valid args given',
      param: [
        {
          msg: 'value is "null"',
          rem: '(function failed: empty string is returned)',
          values: {
            obj: { __text: 'some value' },
            key: null,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: '',
            },
            obj: undefined,
            assertQty: 2,
          },
        }, {
          msg: 'value is an empty string',
          rem: '(function failed: empty string is returned)',
          values: {
            obj: { __text: 'some value' },
            key: '',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: '',
            },
            obj: undefined,
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'a valid args given',
      param: [
        {
          msg: 'value is a non-empty string',
          rem: '(key exists)',
          values: {
            obj: {
              __text: 'some value',
              key1: 'value of "key1"',
            },
            key: 'key1',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: 'value of "key1"',
            },
            obj: undefined,
            assertQty: 2,
          },
        },
      ],
    },
  ],
};

const resultTestData = {
  msg: 'run tests against result values',
  param: [
    {
      msg: 'key not exists',
      param: [
        {
          msg: 'result is empty string',
          rem: '',
          values: {
            obj: { __text: 'some value' },
            key: 'key1',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: '',
            },
            obj: undefined,
            assertQty: 2,
          },
        },
      ],
    },
  ],
};

module.exports = {
  descr: [
    objTestData,
    keyTestData,
    resultTestData,
  ],
};
