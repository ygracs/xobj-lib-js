// [v0.1.008-20240831]

/* module:   `xObj-lib`
 * function: ---
 */

module.exports = {
  insertXObjElements: require('./insertXObjElements.data'),
  insertXObjEChain: require('./insertXObjEChain.data'),
  evalXObjEName: require('./evalXObjEName.data'),
  genXObjENameDescr: require('./genXObjENameDescr.data'),
};
