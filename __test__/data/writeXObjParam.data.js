// [v0.1.002-20230225]

/* module:   `xObj-lib`
 * function: writeXObjParam()
 */

const objTestData = {
  msg: 'run tests against "obj" param',
  param: [
    {
      msg: 'no args given',
      param: [
        {
          msg: 'value is undefined',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: undefined,
            value: 'some value',
            key: undefined,
          },
          status: {
            ops: {
              isErr: true,
              errType: TypeError,
              errCode: 'ERR_XOBJ_NPOBJ',
              value: undefined,
            },
            obj: undefined,
            assertQty: 4,
          },
        },
      ],
    }, {
      msg: 'non valid args given',
      param: [
        {
          msg: 'value is "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: null,
            value: 'some value',
            key: undefined,
          },
          status: {
            ops: {
              isErr: true,
              errType: TypeError,
              errCode: 'ERR_XOBJ_NPOBJ',
              value: undefined,
            },
            obj: null,
            assertQty: 4,
          },
        },
      ],
    }, {
      msg: 'a valid args given',
      param: [
        {
          msg: 'value is proper object',
          rem: '',
          values: {
            obj: { __text: 'some value' },
            value: 'some new value',
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: { __text: 'some new value' },
            assertQty: 3,
          },
        },
      ],
    },
  ],
};

const valueTestData = {
  msg: 'run tests against "value" param',
  param: [
    {
      msg: 'no args given',
      param: [
        {
          msg: 'value is undefined',
          rem: '(function failed: "false" is returned)',
          values: {
            obj: { __text: 'some value' },
            value: undefined,
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: false,
            },
            obj: { __text: 'some value' },
            assertQty: 3,
          },
        },
      ],
    }, {
      msg: 'some other args given',
      param: [
        {
          msg: 'value is "null"',
          rem: '(empty string will be stored as a value)',
          values: {
            obj: { __text: 'some value' },
            value: null,
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: { __text: '' },
            assertQty: 3,
          },
        }, {
          msg: 'value is a boolean',
          rem: '(value converted to a string)',
          values: {
            obj: { __text: 'some value' },
            value: false,
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: { __text: 'false' },
            assertQty: 3,
          },
        }, {
          msg: 'value is a number',
          rem: '(value converted to a string)',
          values: {
            obj: { __text: 'some value' },
            value: 137,
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: { __text: '137' },
            assertQty: 3,
          },
        }, {
          msg: 'value is a string',
          rem: '',
          values: {
            obj: { __text: 'some value' },
            value: 'some new value',
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: { __text: 'some new value' },
            assertQty: 3,
          },
        },
      ],
    },
  ],
};

const keyTestData = {
  msg: 'run tests against "key" param',
  param: [
    {
      msg: 'no args given',
      param: [
        {
          msg: 'value is undefined',
          rem: '(a default "XOBJ_DEF_PARAM_TNAME" is used)',
          values: {
            obj: { __text: 'some value' },
            value: 'some new value',
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: { __text: 'some new value' },
            assertQty: 3,
          },
        },
      ],
    }, {
      msg: 'non valid args given',
      param: [
        {
          msg: 'value is "null"',
          rem: '(function failed: "false" is returned)',
          values: {
            obj: { __text: 'some value' },
            value: 'some new value',
            key: null,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: false,
            },
            obj: { __text: 'some value' },
            assertQty: 3,
          },
        }, {
          msg: 'value is an empty string',
          rem: '(function failed: "false" is returned)',
          values: {
            obj: { __text: 'some value' },
            value: 'some new value',
            key: '',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: false,
            },
            obj: { __text: 'some value' },
            assertQty: 3,
          },
        },
      ],
    }, {
      msg: 'a valid args given',
      param: [
        {
          msg: 'value is a non-empty string',
          rem: '(key exists)',
          values: {
            obj: {
              __text: 'some value',
              key1: 'value of "key1"',
            },
            value: 'some new value',
            key: 'key1',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: {
              __text: 'some value',
              key1: 'some new value',
            },
            assertQty: 3,
          },
        },
      ],
    },
  ],
};

const resultTestData = {
  msg: 'run tests against result values',
  param: [
    {
      msg: 'key not exists',
      param: [
        {
          msg: 'result is "true"',
          rem: '(new target key will be inserted)',
          values: {
            obj: { __text: 'some value' },
            value: 'some new value',
            key: 'key1',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: true,
            },
            obj: {
              __text: 'some value',
              key1: 'some new value',
            },
            assertQty: 3,
          },
        },
      ],
    },
  ],
};

module.exports = {
  descr: [
    objTestData,
    valueTestData,
    keyTestData,
    resultTestData,
  ],
};
