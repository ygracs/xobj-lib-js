// [v0.1.003-20240624]

/* module:   `xObj-lib`
 * function: genXObjENameDescr()
 */

const valueTestData = {
  msg: 'run tests against "value" param',
  param: [
    {
      msg: 'no args given',
      param: [
        {
          msg: 'value is undefined',
          rem: '',
          values: {
            name: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: {
                isERR: true,
                name: null,
                conditions: null,
              },
            },
            obj: undefined,
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'non valid args given',
      param: [
        {
          msg: 'value is a "null"',
          rem: '(function failed)',
          values: {
            name: null,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: {
                isERR: true,
                name: null,
                conditions: null,
              },
            },
            obj: undefined,
            assertQty: 2,
          },
        }, {
          msg: 'value is a "boolean"',
          rem: '(function failed)',
          values: {
            name: true,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: {
                isERR: true,
                name: null,
                conditions: null,
              },
            },
            obj: undefined,
            assertQty: 2,
          },
        }, {
          msg: 'value is a negative number',
          rem: '(function failed)',
          values: {
            name: -123,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: {
                isERR: true,
                name: null,
                conditions: null,
              },
            },
            obj: undefined,
            assertQty: 2,
          },
        }, {
          msg: 'value is an empty string',
          rem: '(function failed)',
          values: {
            name: '',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: {
                isERR: true,
                name: null,
                conditions: null,
              },
            },
            obj: undefined,
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'a valid args given',
      param: [
        {
          msg: 'value is a positive number',
          rem: '',
          values: {
            name: 2,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: {
                isERR: false,
                name: 2,
                conditions: null,
              },
            },
            obj: undefined,
            assertQty: 2,
          },
        }, {
          msg: 'value is a non-empty string',
          rem: '',
          values: {
            name: 'key1',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: {
                isERR: false,
                name: 'key1',
                conditions: null,
              },
            },
            obj: undefined,
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'special cases',
      param: [
        {
          msg: 'value is a string filled with spaces',
          rem: '(function failed)',
          values: {
            name: '  \n   \t  ',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: {
                isERR: true,
                name: null,
                conditions: null,
              },
            },
            obj: undefined,
            assertQty: 2,
          },
        }, {
          msg: 'value is a string that represents a negative number',
          rem: '(function failed)',
          values: {
            name: '-27',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: {
                isERR: true,
                name: null,
                conditions: null,
              },
            },
            obj: undefined,
            assertQty: 2,
          },
        }, {
          msg: 'value is a string that represents a positive number',
          rem: '(function succeed: name converted to a number)',
          values: {
            name: '27',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: {
                isERR: false,
                name: 27,
                conditions: null,
              },
            },
            obj: undefined,
            assertQty: 2,
          },
        },
      ],
    },
  ],
};

const resultTestData = {
  msg: 'run tests against result values',
  param: [
    {
      msg: 'test case against well-formed strings',
      param: [
        {
          msg: 'case 1.',
          rem: 'template: <name>',
          values: {
            name: 'note     ',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: {
                isERR: false,
                name: 'note',
                conditions: null,
              },
            },
            obj: undefined,
            assertQty: 2,
          },
        }, {
          msg: 'case 2-a.',
          rem: 'template: <name>[<child>]',
          values: {
            name: '   note[id] ',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: {
                isERR: false,
                name: 'note',
                conditions: {
                  type: 'child',
                  name: 'id',
                  value: undefined,
                },
              },
            },
            obj: undefined,
            assertQty: 2,
          },
        }, {
          msg: 'case 2-b.',
          rem: 'template: <name>[<child>=<value>]',
          values: {
            name: '   note[id=jkl] ',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: {
                isERR: false,
                name: 'note',
                conditions: {
                  type: 'child',
                  name: 'id',
                  value: 'jkl',
                },
              },
            },
            obj: undefined,
            assertQty: 2,
          },
        }, {
          msg: 'case 2-c.',
          rem: 'template: <name>[<child>="<value>"]',
          values: {
            name: '   note[id="jkl"] ',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: {
                isERR: false,
                name: 'note',
                conditions: {
                  type: 'child',
                  name: 'id',
                  value: 'jkl',
                },
              },
            },
            obj: undefined,
            assertQty: 2,
          },
        }, {
          msg: 'case 3-a.',
          rem: 'template: <name>[@<attribute>]',
          values: {
            name: '   note[@id] ',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: {
                isERR: false,
                name: 'note',
                conditions: {
                  type: 'attribute',
                  name: 'id',
                  value: undefined,
                },
              },
            },
            obj: undefined,
            assertQty: 2,
          },
        }, {
          msg: 'case 3-b.',
          rem: 'template: <name>[@<attribute>=<value>]',
          values: {
            name: '   note[@id=jkl] ',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: {
                isERR: false,
                name: 'note',
                conditions: {
                  type: 'attribute',
                  name: 'id',
                  value: 'jkl',
                },
              },
            },
            obj: undefined,
            assertQty: 2,
          },
        }, {
          msg: 'case 3-c.',
          rem: 'template: <name>[@<attribute>="<value>"]',
          values: {
            name: '   note[@id="jkl"] ',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: {
                isERR: false,
                name: 'note',
                conditions: {
                  type: 'attribute',
                  name: 'id',
                  value: 'jkl',
                },
              },
            },
            obj: undefined,
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'test case against ill-formed strings',
      param: [
        {
          msg: 'case 1.',
          rem: 'template: <name>   [<child>]',
          values: {
            name: '   note   [id] ',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: {
                isERR: true,
                name: null,
                conditions: null,
              },
            },
            obj: undefined,
            assertQty: 2,
          },
        }, {
          msg: 'case 2.',
          rem: 'template: [<name>][<child>]',
          values: {
            name: '   [note][id] ',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: {
                isERR: true,
                name: null,
                conditions: null,
              },
            },
            obj: undefined,
            assertQty: 2,
          },
        }, {
          msg: 'case 3.',
          rem: 'template: <name>   <child>',
          values: {
            name: '   note   id ',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: {
                isERR: true,
                name: null,
                conditions: null,
              },
            },
            obj: undefined,
            assertQty: 2,
          },
        }, {
          msg: 'case 4.',
          rem: 'template: <name>[<child>]<some_tail>',
          values: {
            name: '   note[id]    kksll;k ',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: {
                isERR: true,
                name: null,
                conditions: null,
              },
            },
            obj: undefined,
            assertQty: 2,
          },
        },
      ],
    },
  ],
};

module.exports = {
  descr: [
    valueTestData,
    resultTestData,
  ],
};
