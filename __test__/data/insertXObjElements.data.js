// [v0.1.008-20240915]

/* module:   `xObj-lib`
 * function: insertXObjElements()
 */

const objTestDescr = {
  msg: 'run tests against "obj" param',
  rem: '(defaults used)',
  param: [
    {
      msg: 'no args given',
      param: [
        {
          msg: 'value is undefined',
          rem: '(a "TypeError" is thrown)',
          values: [
            /* obj */
            undefined,
            /* keys */
            ...[ 'key1' ],
            /* options */
            //undefined,
          ],
          status: {
            ops: {
              isErr: true,
              errType: TypeError,
              errCode: 'ERR_XOBJ_NPOBJ',
              value: undefined,
            },
            obj: undefined,
            assertQty: 4,
          },
        },
      ],
    }, {
      msg: 'invalid value is given',
      param: [
        {
          msg: 'value is a "null"',
          rem: '(a "TypeError" is thrown)',
          values: [
            /* obj */
            null,
            /* keys */
            ...[ 'key1' ],
            /* options */
            //undefined,
          ],
          status: {
            ops: {
              isErr: true,
              errType: TypeError,
              errCode: 'ERR_XOBJ_NPOBJ',
              value: undefined,
            },
            obj: null,
            assertQty: 4,
          },
        }, {
          msg: 'value is a "boolean"',
          rem: '(a "TypeError" is thrown)',
          values: [
            /* obj */
            true,
            /* keys */
            ...[ 'key1' ],
            /* options */
            //undefined,
          ],
          status: {
            ops: {
              isErr: true,
              errType: TypeError,
              errCode: 'ERR_XOBJ_NPOBJ',
              value: undefined,
            },
            obj: true,
            assertQty: 4,
          },
        },
      ],
    }, {
      msg: 'a valid args given',
      param: [
        {
          msg: 'value is an empty object',
          rem: '',
          values: [
            /* obj */
            {},
            /* keys */
            ...[ 'key1', 'key2' ],
            /* options */
            //undefined,
          ],
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: 2,
            },
            obj: {
              key1: {},
              key2: {},
            },
            assertQty: 3,
          },
        },
      ],
    }, {
      msg: 'special cases',
      param: [
        {
          msg: 'value is an array',
          rem: '(a "TypeError" is thrown)',
          values: [
            /* obj */
            [],
            /* keys */
            ...[ 'key1', 'key2' ],
            /* options */
            //undefined,
          ],
          status: {
            ops: {
              isErr: true,
              errType: TypeError,
              errCode: 'ERR_XOBJ_NPOBJ',
              value: undefined,
            },
            obj: [],
            assertQty: 4,
          },
        },
      ],
    },
  ],
};

const keyTestDescr = {
  msg: 'run tests against "key" list',
  rem: '',
  param: [
    {
      msg: 'none key is given',
      param: [
        {
          msg: 'perform ops',
          rem: '',
          values: [
            /* obj */
            {},
            /* keys */
            ...[],
            /* options */
            //undefined,
          ],
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: 0,
            },
            obj: {},
            assertQty: 3,
          },
        },
      ],
    }, {
      msg: 'some of the given keys has a non-valid value',
      param: [
        {
          msg: 'perform ops',
          rem: '(non-valid value will be ignored)',
          values: [
            /* obj */
            {},
            /* keys */
            ...[ 'prm2', null, true, 10, 'prm23' ],
            ...[ [ 1, 2 ], { name: 'some name' } ],
            /* options */
            //undefined,
          ],
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: 2,
            },
            obj: {
              prm2: {},
              prm23: {},
            },
            assertQty: 3,
          },
        },
      ],
    }, {
      msg: 'given keys are a valid values',
      param: [
        {
          msg: 'some of the keys are exists',
          rem: '(ref to object)',
          values: [
            /* obj */
            { prm2: { __text: 'prm2' } },
            /* keys */
            ...[ 'prm2', 'prm23' ],
            /* options */
            //undefined,
          ],
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: 2,
            },
            obj: {
              prm2: { __text: 'prm2' },
              prm23: {},
            },
            assertQty: 3,
          },
        }, {
          msg: 'some of the keys are exists',
          rem: '(ref to non-object)',
          values: [
            /* obj */
            { prm2: 'prm2 value' },
            /* keys */
            ...[ 'prm2', 'prm23' ],
            /* options */
            //undefined,
          ],
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: 1,
            },
            obj: {
              prm2: 'prm2 value',
              prm23: {},
            },
            assertQty: 3,
          },
        },
      ],
    },
  ],
};

const optTestDescr = {
  msg: 'run tests against "options" parameter',
  rem: '',
  param: [
    {
      msg: 'with a custom options (object + force action)',
      param: [
        {
          msg: 'provide a valid key (some of the keys exists)',
          rem: '(case 1)',
          values: [
            /* obj */
            { prm1: 'key exists', prm3: {} },
            /* keys */
            ...[ 'prm1', 'prm2' ],
            /* options */
            { force: true },
          ],
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: 2,
            },
            obj: {
              prm1: {},
              prm2: {},
              prm3: {},
            },
            assertQty: 3,
          },
        }, {
          msg: 'provide a valid key (some of the keys exists)',
          rem: '(case 2)',
          values: [
            /* obj */
            { prm1: { text: 'key exists' }, prm3: {} },
            /* keys */
            ...[ 'prm1', 'prm2' ],
            /* options */
            { force: true },
          ],
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: 2,
            },
            obj: {
              prm1: { text: 'key exists' },
              prm2: {},
              prm3: {},
            },
            assertQty: 3,
          },
        }, {
          msg: 'provide a valid key (some of the keys exists)',
          rem: '(case 3)',
          values: [
            /* obj */
            { prm1: { text: 'key exists' }, prm3: {} },
            /* keys */
            ...[ 'prm1', 'prm2' ],
            /* options */
            { force: true, ripOldies: true },
          ],
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: 2,
            },
            obj: {
              prm1: {},
              prm2: {},
              prm3: {},
            },
            assertQty: 3,
          },
        }, {
          msg: 'provide a valid key (some of the keys exists)',
          rem: '(case 4)',
          values: [
            /* obj */
            { prm1: {}, prm3: {} },
            /* keys */
            ...[ 'prm1', 'prm2', 'prm123' ],
            /* options */
            { force: true },
          ],
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: 3,
            },
            obj: {
              prm1: {},
              prm2: {},
              prm3: {},
              prm123: {},
            },
            assertQty: 3,
          },
        },
      ],
    },
  ],
};

module.exports = {
  descr: [
    objTestDescr,
    keyTestDescr,
    optTestDescr,
  ],
};
