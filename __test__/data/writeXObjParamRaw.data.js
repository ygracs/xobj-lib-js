// [v0.1.010-20240902]

/* module:   `xObj-lib`
 * function: writeXObjParamRaw()
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const objTestData = {
  msg: 'run tests against "obj" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: undefined,
            value: 'some value',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: undefined,
            value: 'some value',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is an object',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: {},
            value: 'some value',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __text: 'some value' },
        },
      },
    }),
  ],
};

const valueTestData = {
  msg: 'run tests against "value" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(failed: "false" returned)',
          values: {
            obj: {},
            value: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {},
        },
      },
    }),
    ...genTestCase({
      inGroup: 'some args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            obj: {},
            value: null,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __text: null },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'some args are given',
      cases: [
        {
          msg: 'value is a boolean',
          rem: '',
          values: {
            obj: {},
            value: true,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __text: true },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'some args are given',
      cases: [
        {
          msg: 'value is a number',
          rem: '',
          values: {
            obj: {},
            value: 127,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __text: 127 },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'some args are given',
      cases: [
        {
          msg: 'value is a string',
          rem: '',
          values: {
            obj: {},
            value: 'some value',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __text: 'some value' },
        },
      },
    }),
  ],
};

const keyTestData = {
  msg: 'run tests against "key" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(defaults must be used)',
          values: {
            obj: {},
            value: 'some value',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __text: 'some value' },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: {},
            value: 'some value',
            key: null,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NSTR',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: {
          obj: {},
        },
        after: {
          obj: {},
        },
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args given',
      cases: [
        {
          msg: 'value is an empty string',
          rem: '(failed: "false" returned)',
          values: {
            obj: {},
            value: 'some value',
            key: '',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {},
        },
      },
    }),
    ...genTestCase({
      inGroup: 'valid args given',
      cases: [
        {
          msg: 'value is a non-empty string',
          rem: '',
          values: {
            obj: {},
            value: 'some value',
            key: 'some_key',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { some_key: 'some value' },
        },
      },
    }),
  ],
};

module.exports = {
  //descr: [],
  tests: [
    objTestData,
    valueTestData,
    keyTestData,
  ],
};
