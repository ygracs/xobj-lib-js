// [v0.1.002-20240624]

/* module:   `xObj-lib`
 * function: evalXObjEName()
 */

const valueTestData = {
  msg: 'run tests against "value" param',
  param: [
    {
      msg: 'no args given',
      param: [
        {
          msg: 'value is undefined',
          rem: '(function failed: "undefined" is returned)',
          values: {
            name: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: null,
            },
            obj: undefined,
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'non valid args given',
      param: [
        {
          msg: 'value is a "null"',
          rem: '(function failed: "null" is returned)',
          values: {
            name: null,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: null,
            },
            obj: undefined,
            assertQty: 2,
          },
        }, {
          msg: 'value is a "boolean"',
          rem: '(function failed: "null" is returned)',
          values: {
            name: true,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: null,
            },
            obj: undefined,
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'a valid args given',
      param: [
        {
          msg: 'value is a non-empty string',
          rem: '',
          values: {
            name: 'key1',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: 'key1',
            },
            obj: undefined,
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'special cases',
      param: [
        {
          msg: 'value is a positive number',
          rem: '(function succeed: result is a "number")',
          values: {
            name: 27,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: 27,
            },
            obj: undefined,
            assertQty: 2,
          },
        }, {
          msg: 'value is a negative number',
          rem: '(function failed: "null" is returned)',
          values: {
            name: -27,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: null,
            },
            obj: undefined,
            assertQty: 2,
          },
        }, {
          msg: 'value is an empty string',
          rem: '(function failed: "null" is returned)',
          values: {
            name: '',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: null,
            },
            obj: undefined,
            assertQty: 2,
          },
        }, {
          msg: 'value is a string that represents a negative number',
          rem: '(function failed: "null" is returned)',
          values: {
            name: '-27',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: null,
            },
            obj: undefined,
            assertQty: 2,
          },
        }, {
          msg: 'value is a string that represents a positive number',
          rem: '(function succeed: result is a "number")',
          values: {
            name: '27',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: 27,
            },
            obj: undefined,
            assertQty: 2,
          },
        },
      ],
    },
  ],
};

module.exports = {
  descr: [
    valueTestData,
  ],
};
