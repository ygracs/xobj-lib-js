// [v0.1.007-20240911]

/* module:   `xObj-lib`
 * function: insertXObjEList()
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const objTestData = {
  msg: 'run tests against "obj" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: undefined,
            name: 'some_key',
            opt: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: null,
            name: 'some_key',
            opt: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is an empty object',
          rem: '',
          values: {
            obj: {},
            name: 'some_key',
            opt: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: [],
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { some_key: [] },
        },
      },
    }),
  ],
};

const nameTestData = {
  msg: 'run tests against "name" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(failed: "null" returned)',
          values: {
            obj: {},
            name: undefined,
            opt: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {},
        },
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(failed: "null" returned)',
          values: {
            obj: {},
            name: null,
            opt: undefined,
          },
        }, {
          msg: 'value is an empty string',
          rem: '(failed: "null" returned)',
          values: {
            obj: {},
            name: '',
            opt: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {},
        },
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is a not empty string',
          rem: '',
          values: {
            obj: {},
            name: 'some_key',
            opt: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: [],
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { some_key: [] },
        },
      },
    }),
  ],
};

const optTestData = {
  msg: 'run tests against "options" param',
  rem: '(target element exists)',
  param: [
    ...genTestCase({
      inGroup: 'options: force = "true"',
      cases: [
        {
          msg: 'element is a string',
          rem: '(new element will be assinged to a target key)',
          values: {
            obj: { target_key: 'some text' },
            name: 'target_key',
            opt: { force: true },
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: [],
        },
        assertQty: 2,
        before: {
          obj: { target_key: 'some text' },
        },
        after: {
          obj: { target_key: [] },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'options: force = "true"',
      cases: [
        {
          msg: 'element is an array',
          rem: '(an element will be returned, no changes applied)',
          values: {
            obj: { target_key: [ { __text: 'some text' } ] },
            name: 'target_key',
            opt: { force: true },
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: [ { __text: 'some text' } ],
        },
        assertQty: 2,
        before: {
          obj: { target_key: [ { __text: 'some text' } ] },
        },
        after: {
          obj: { target_key: [ { __text: 'some text' } ] },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'options: force = "true"',
      cases: [
        {
          msg: 'element is an object',
          rem: '(an element will be wrapped into an array)',
          values: {
            obj: {
              target_key: { __text: 'some text' },
            },
            name: 'target_key',
            opt: { force: true },
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: [ { __text: 'some text' } ],
        },
        assertQty: 2,
        before: {
          obj: {
            target_key: { __text: 'some text' },
          },
        },
        after: {
          obj: {
            target_key: [ { __text: 'some text' } ],
          },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'options: force = "true" & ripOldies = "true"',
      cases: [
        {
          msg: 'element is an object',
          rem: '(new element will be assinged to a target key)',
          values: {
            obj: {
              target_key: { __text: 'some text' },
            },
            name: 'target_key',
            opt: { force: true, ripOldies: true },
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: [],
        },
        assertQty: 2,
        before: {
          obj: {
            target_key: { __text: 'some text' },
          },
        },
        after: {
          obj: { target_key: [] },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'options: force = "true" & ripOldies = "true"',
      cases: [
        {
          msg: 'element is an array',
          rem: '(new element will be assinged to a target key)',
          values: {
            obj: {
              target_key: [ { __text: 'some text' } ],
            },
            name: 'target_key',
            opt: { force: true, ripOldies: true },
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: [],
        },
        assertQty: 2,
        before: {
          obj: {
            target_key: [ { __text: 'some text' } ],
          },
        },
        after: {
          obj: { target_key: [] },
        },
      },
    }),
  ],
};

const resultTestData = {
  msg: 'run tests against result values',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'target element exists',
      cases: [
        {
          msg: 'element is an object',
          rem: '(target element will be wrapped into an array)',
          values: {
            obj: { target_key: { __text: 'target' } },
            name: 'target_key',
            opt: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: [ { __text: 'target' } ],
        },
        assertQty: 2,
        before: {
          obj: { target_key: { __text: 'target' } },
        },
        after: {
          obj: { target_key: [ { __text: 'target' } ] },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'target element exists',
      cases: [
        {
          msg: 'element is an array',
          rem: '',
          values: {
            obj: {
              target_key: [ { __text: 'target' } ],
            },
            name: 'target_key',
            opt: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: [ { __text: 'target' } ],
        },
        assertQty: 2,
        before: {
          obj: {
            target_key: [ { __text: 'target' } ],
          },
        },
        after: {
          obj: {
            target_key: [ { __text: 'target' } ],
          },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'target element exists',
      cases: [
        {
          msg: 'element is a "null"',
          rem: '(new element will be assinged to a target key)',
          values: {
            obj: { target_key: null },
            name: 'target_key',
            opt: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: [],
        },
        assertQty: 2,
        before: {
          obj: { target_key: null },
        },
        after: {
          obj: { target_key: [] },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'target element exists',
      cases: [
        {
          msg: 'element is a string',
          rem: '(failed: "null" returned)',
          values: {
            obj: { target_key: 'some text' },
            name: 'target_key',
            opt: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: null,
        },
        assertQty: 2,
        before: {
          obj: { target_key: 'some text' },
        },
        after: {
          obj: { target_key: 'some text' },
        },
      },
    }),
  ],
};

module.exports = {
  //descr: [],
  tests: [
    objTestData,
    nameTestData,
    optTestData,
    resultTestData,
  ],
};
