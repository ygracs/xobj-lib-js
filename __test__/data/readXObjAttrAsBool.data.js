// [v0.1.002-20240906]

/* module:   `xObj-lib`
 * function: readXObjAttrAsBool()
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const objTestData = {
  msg: 'run tests against "obj" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: undefined,
            attr: 'some_attr',
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: null,
            attr: 'some_attr',
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is an object',
          rem: '',
          values: {
            obj: { __attr: { some_attr: 'true' } },
            attr: 'some_attr',
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
      },
    }),
  ],
};

const attrTestData = {
  msg: 'run tests against "attr" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(failed: "false" returned)',
          values: {
            obj: { __attr: { some_attr: 'true' } },
            attr: undefined,
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(failed: "false" returned)',
          values: {
            obj: { __attr: { some_attr: 'true' } },
            attr: null,
            defValue: undefined,
            key: undefined,
          },
        }, {
          msg: 'value is an empty string',
          rem: '(failed: "false" returned)',
          values: {
            obj: { __attr: { some_attr: 'true' } },
            attr: '',
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is a non-empty string',
          rem: '',
          values: {
            obj: { __attr: { target_attr: 'true' } },
            attr: 'target_attr',
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
      },
    }),
  ],
};

const keyTestData = {
  msg: 'run tests against "key" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no valid args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(defaults must be used)',
          values: {
            obj: { __attr: { some_attr: 'true' } },
            attr: 'some_attr',
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(failed: "false" returned)',
          values: {
            obj: { __attr: { some_attr: 'true' } },
            attr: 'some_attr',
            defValue: undefined,
            key: null,
          },
        }, {
          msg: 'value is an empty string',
          rem: '(failed: "false" returned)',
          values: {
            obj: { __attr: { some_attr: 'true' } },
            attr: 'some_attr',
            defValue: undefined,
            key: '',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is a non-empty string',
          rem: '',
          values: {
            obj: {
              __attr: { some_attr: 'true' },
              target_key: { target_attr: 'true' },
            },
            attr: 'target_attr',
            defValue: undefined,
            key: 'target_key',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
      },
    }),
  ],
};

const dvalTestData = {
  msg: 'run tests against "defValue" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '("false" is used by default)',
          values: {
            obj: {
              __attr: { some_attr: 'not boolean value'},
            },
            attr: 'some_attr',
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '("false" is used by default)',
          values: {
            obj: {
              __attr: { some_attr: 'not boolean value'},
            },
            attr: 'some_attr',
            defValue: null,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is a boolean',
          rem: '',
          values: {
            obj: {
              __attr: { some_attr: 'not boolean value'},
            },
            attr: 'some_attr',
            defValue: true,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'special cases',
      cases: [
        {
          msg: 'ensure a value from a target attribute returned',
          rem: '(not a default value)',
          values: {
            obj: {
              __attr: { some_attr: 'not boolean value'},
              target_key: { target_attr: 'true'},
            },
            attr: 'target_attr',
            defValue: false,
            key: 'target_key',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
      },
    }),
  ],
};

const resultTestData = {
  msg: 'run tests against result values',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'target attribute is not exists',
      cases: [
        {
          msg: 'perform ops',
          rem: '(failed: default value returned)',
          values: {
            obj: {
              __attr: { some_attr: 'true' },
            },
            attr: 'some_attr',
            defValue: undefined,
            key: 'target_attr',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'cases for value type of target attribute',
      cases: [
        {
          msg: 'value is a boolean',
          rem: '(value must be accepted)',
          values: {
            obj: {
              __attr: { target_attr: true },
            },
            attr: 'target_attr',
            defValue: undefined,
            key: undefined,
          },
        }, {
          msg: 'value is a number less than "0"',
          rem: '(value must be accepted)',
          values: {
            obj: {
              __attr: { target_attr: -127 },
            },
            attr: 'target_attr',
            defValue: undefined,
            key: undefined,
          },
        }, {
          msg: 'value is a number greater than "0"',
          rem: '(value must be accepted)',
          values: {
            obj: {
              __attr: { target_attr: 127 },
            },
            attr: 'target_attr',
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'cases for value type of target attribute',
      cases: [
        {
          msg: 'value is a number equal to "0"',
          rem: '(value must be accepted)',
          values: {
            obj: {
              __attr: { target_attr: 0 },
            },
            attr: 'target_attr',
            defValue: true,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
      },
    }),
  ],
};

module.exports = {
  //descr: [],
  tests: [
    objTestData,
    attrTestData,
    keyTestData,
    dvalTestData,
    resultTestData,
  ],
};
