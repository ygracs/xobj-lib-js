// [v0.1.003-20240906]

/* module:   `xObj-lib`
 * function: writeXObjAttrAsBool()
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const objTestData = {
  msg: 'run tests against "obj" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: undefined,
            attr: 'some_attr',
            value: true,
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: null,
            attr: 'some_attr',
            value: true,
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is an object',
          rem: '',
          values: {
            obj: {},
            attr: 'some_attr',
            value: true,
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __attr: { some_attr: 'true' } },
        },
      },
    }),
  ],
};

const attrTestData = {
  msg: 'run tests against "attr" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(failed: "false" returned)',
          values: {
            obj: {},
            attr: undefined,
            value: true,
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {},
        },
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(failed: "false" returned)',
          values: {
            obj: {},
            attr: null,
            value: true,
            defValue: undefined,
            key: undefined,
          },
        }, {
          msg: 'value is an empty string',
          rem: '(failed: "false" returned)',
          values: {
            obj: {},
            attr: '',
            value: true,
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {},
        },
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is a non-empty string',
          rem: '',
          values: {
            obj: {},
            attr: 'some_attr',
            value: true,
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __attr: { some_attr: 'true' } },
        },
      },
    }),
  ],
};

const valueTestData = {
  msg: 'run tests against "value" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(failed: "false" returned)',
          values: {
            obj: {},
            attr: 'some_attr',
            value: undefined,
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {},
        },
      },
    }),
    ...genTestCase({
      inGroup: 'some args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '("false" will be stored as a value)',
          values: {
            obj: {},
            attr: 'some_attr',
            value: null,
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __attr: { some_attr: 'false' } },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'some args are given',
      cases: [
        {
          msg: 'value is a boolean',
          rem: '',
          values: {
            obj: {},
            attr: 'some_attr',
            value: true,
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __attr: { some_attr: 'true' } },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'some args are given',
      cases: [
        {
          msg: 'value is a negative number',
          rem: '("true" will be stored as a value)',
          values: {
            obj: {},
            attr: 'some_attr',
            value: -127,
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __attr: { some_attr: 'true' } },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'some args are given',
      cases: [
        {
          msg: 'value is a number equal to "0"',
          rem: '("false" will be stored as a value)',
          values: {
            obj: {},
            attr: 'some_attr',
            value: 0,
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __attr: { some_attr: 'false' } },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'some args are given',
      cases: [
        {
          msg: 'value is a positive number',
          rem: '("true" will be stored as a value)',
          values: {
            obj: {},
            attr: 'some_attr',
            value: 127,
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __attr: { some_attr: 'true' } },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'some args are given',
      cases: [
        {
          msg: 'value is a string not convertable to a boolean',
          rem: '("false" will be stored as a value)',
          values: {
            obj: {},
            attr: 'some_attr',
            value: 'not a boolean',
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __attr: { some_attr: 'false' } },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'some args are given',
      cases: [
        {
          msg: 'value is a string convertable to a boolean',
          rem: '(value accepted)',
          values: {
            obj: {},
            attr: 'some_attr',
            value: 'true',
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __attr: { some_attr: 'true' } },
        },
      },
    }),
  ],
};

const keyTestData = {
  msg: 'run tests against "key" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(defaults must be used)',
          values: {
            obj: {},
            attr: 'some_attr',
            value: true,
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __attr: { some_attr: 'true' } },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(failed: "false" returned)',
          values: {
            obj: {},
            attr: 'some_attr',
            value: true,
            defValue: undefined,
            key: null,
          },
        }, {
          msg: 'value is an empty string',
          rem: '(failed: "false" returned)',
          values: {
            obj: {},
            attr: 'some_attr',
            value: true,
            defValue: undefined,
            key: '',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {},
        },
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is a non-empty string',
          rem: '',
          values: {
            obj: {},
            attr: 'some_attr',
            value: true,
            defValue: undefined,
            key: 'target_key',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { target_key: { some_attr: 'true' } },
        },
      },
    }),
  ],
};

const dvalTestData = {
  msg: 'run tests against "defValue" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(defaults used)',
          values: {
            obj: {},
            attr: 'some_attr',
            value: 'not boolean',
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __attr: { some_attr: 'false' } },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(defaults used)',
          values: {
            obj: {},
            attr: 'some_attr',
            value: 'not boolean',
            defValue: null,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __attr: { some_attr: 'false' } },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is a boolean',
          rem: '',
          values: {
            obj: {},
            attr: 'some_attr',
            value: 'not boolean',
            defValue: true,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __attr: { some_attr: 'true' } },
        },
      },
    }),
  ],
};

const resultTestData = {
  msg: 'run tests against result values',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'special cases',
      cases: [
        {
          msg: '"value" param is not given but "defValue" is',
          rem: '(a "defValue" must be applied if boolean)',
          values: {
            obj: {},
            attr: 'some_attr',
            value: undefined,
            defValue: true,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __attr: { some_attr: 'true' } },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'special cases',
      cases: [
        {
          msg: '"value" param is not given but "defValue" is',
          rem: '(a "defValue" must be ignored if not boolean)',
          values: {
            obj: {},
            attr: 'some_attr',
            value: undefined,
            defValue: 'not boolean',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {},
        },
      },
    }),
    ...genTestCase({
      inGroup: 'cases for attribute container type',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            obj: { __attr: null },
            attr: 'some_attr',
            value: true,
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: { __attr: null },
        },
        after: {
          obj: { __attr: { some_attr: 'true' } },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'cases for attribute container type',
      cases: [
        {
          msg: 'value is a boolean',
          rem: '',
          values: {
            obj: { __attr: false },
            attr: 'some_attr',
            value: true,
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: { __attr: false },
        },
        after: {
          obj: { __attr: { some_attr: 'true' } },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'cases for attribute container type',
      cases: [
        {
          msg: 'value is an object',
          rem: '',
          values: {
            obj: { __attr: {} },
            attr: 'some_attr',
            value: true,
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: { __attr: {} },
        },
        after: {
          obj: { __attr: { some_attr: 'true' } },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'cases for attribute container type',
      cases: [
        {
          msg: 'value is an array',
          rem: '',
          values: {
            obj: { __attr: [] },
            attr: 'some_attr',
            value: true,
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: { __attr: [] },
        },
        after: {
          obj: { __attr: { some_attr: 'true' } },
        },
      },
    }),
  ],
};

module.exports = {
  //descr: [],
  tests: [
    objTestData,
    attrTestData,
    valueTestData,
    keyTestData,
    dvalTestData,
    resultTestData,
  ],
};
