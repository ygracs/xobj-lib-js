// [v0.1.012-20240905]

/* module:   `xObj-lib`
 * function: readXObjParamAsIndex()
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const objTestData = {
  msg: 'run tests against "obj" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: null,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is an object',
          rem: '',
          values: {
            obj: { __text: '127' },
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 127,
        },
        assertQty: 2,
      },
    }),
  ],
};

const keyTestData = {
  msg: 'run tests against "key" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(defaults must be used)',
          values: {
            obj: { __text: '127' },
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 127,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(failed: default value returned)',
          values: {
            obj: { __text: '172' },
            key: null,
          },
        }, {
          msg: 'value is an empty string',
          rem: '(failed: default value returned)',
          values: {
            obj: { __text: '135' },
            key: '',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: -1,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is a non-empty string',
          rem: '',
          values: {
            obj: {
              __text: '137',
              some_key: '173',
            },
            key: 'some_key',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 173,
        },
        assertQty: 2,
      },
    }),
  ],
};

const resultTestData = {
  msg: 'run tests against result values',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'target key is not exists',
      cases: [
        {
          msg: 'perform ops',
          rem: '(failed: default value returned)',
          values: {
            obj: {
              __text: '127',
            },
            key: 'some_key',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: -1,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'cases for value type of target key',
      cases: [
        {
          msg: 'value is a positive number',
          rem: '(value accepted)',
          values: {
            obj: {
              __text: 127,
            },
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 127,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'cases for value type of target key',
      cases: [
        {
          msg: 'value is a negative number',
          rem: '(failed: default value returned)',
          values: {
            obj: {
              __text: -127,
            },
            key: undefined,
          },
        }, {
          msg: 'value is a string not convertable to a number',
          rem: '(failed: default value returned)',
          values: {
            obj: {
              __text: 'not number',
            },
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: -1,
        },
        assertQty: 2,
      },
    }),
  ],
};

module.exports = {
  //descr: [],
  tests: [
    objTestData,
    keyTestData,
    resultTestData,
  ],
};
