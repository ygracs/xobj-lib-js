// [v0.1.001-20230205]

/* module:   `xObj-lib`
 * function: readXObjAttrAsStr()
 */

const objTestData = {
  msg: 'run tests aganst "obj" param',
  param: [
    {
      msg: 'no args given',
      param: [
        {
          msg: 'value is undefined',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: undefined,
            attr: 'attr_1',
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: undefined,
            isErr: true,
            err_t: TypeError,
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'non valid args given',
      param: [
        {
          msg: 'value is "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: null,
            attr: 'attr_1',
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: undefined,
            isErr: true,
            err_t: TypeError,
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'a valid args given',
      param: [
        {
          msg: 'value is proper object',
          rem: '',
          values: {
            obj: {
              __attr: { attr_1: 'some value' },
            },
            attr: 'attr_1',
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: 'some value',
            isErr: false,
            err_t: undefined,
            assertQty: 2,
          },
        },
      ],
    },
  ],
};

const attrTestData = {
  msg: 'run tests aganst "attr" param',
  param: [
    {
      msg: 'no args given',
      param: [
        {
          msg: 'value is undefined',
          rem: '(function failed: empty string is returned)',
          values: {
            obj: {
              __attr: { attr_1: 'some value' },
            },
            attr: undefined,
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: '',
            isErr: false,
            err_t: undefined,
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'non valid args given',
      param: [
        {
          msg: 'value is "null"',
          rem: '(function failed: empty string is returned)',
          values: {
            obj: {
              __attr: { attr_1: 'some value' },
            },
            attr: null,
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: '',
            isErr: false,
            err_t: undefined,
            assertQty: 2,
          },
        }, {
          msg: 'value is an empty string',
          rem: '(function failed: empty string is returned)',
          values: {
            obj: {
              __attr: { attr_1: 'some value' },
            },
            attr: '',
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: '',
            isErr: false,
            err_t: undefined,
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'a valid args given',
      param: [
        {
          msg: 'value is a non-empty string',
          rem: '(key exists)',
          values: {
            obj: {
              __attr: { attr_1: 'value of "attr_1"' },
              key1: { attr_2: 'value of "attr_2"' },
            },
            attr: 'attr_2',
            defValue: undefined,
            key: 'key1',
          },
          status: {
            ops: 'value of "attr_2"',
            isErr: false,
            err_t: undefined,
            assertQty: 2,
          },
        },
      ],
    },
  ],
};

const keyTestData = {
  msg: 'run tests aganst "key" param',
  param: [
    {
      msg: 'no args given',
      param: [
        {
          msg: 'value is undefined',
          rem: '(a default "XOBJ_DEF_ATTR_TNAME" is used)',
          values: {
            obj: {
              __attr: { attr_1: 'some value' },
            },
            attr: 'attr_1',
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: 'some value',
            isErr: false,
            err_t: undefined,
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'non valid args given',
      param: [
        {
          msg: 'value is "null"',
          rem: '(function failed: empty string is returned)',
          values: {
            obj: {
              __attr: { attr_1: 'some value' },
            },
            attr: 'attr_1',
            defValue: undefined,
            key: null,
          },
          status: {
            ops: '',
            isErr: false,
            err_t: undefined,
            assertQty: 2,
          },
        }, {
          msg: 'value is an empty string',
          rem: '(function failed: empty string is returned)',
          values: {
            obj: {
              __attr: { attr_1: 'some value' },
            },
            attr: 'attr_1',
            defValue: undefined,
            key: '',
          },
          status: {
            ops: '',
            isErr: false,
            err_t: undefined,
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'a valid args given',
      param: [
        {
          msg: 'value is a non-empty string',
          rem: '(key exists)',
          values: {
            obj: {
              __attr: { attr_1: 'value of "attr_1"' },
              key1: { attr_2: 'value of "attr_2"' },
            },
            attr: 'attr_2',
            defValue: undefined,
            key: 'key1',
          },
          status: {
            ops: 'value of "attr_2"',
            isErr: false,
            err_t: undefined,
            assertQty: 2,
          },
        },
      ],
    },
  ],
};

const defsTestData = {
  msg: 'run tests aganst "defValue" param',
  param: [
    {
      msg: 'no args given',
      param: [
        {
          msg: 'value is undefined',
          rem: '(empty string is used by default)',
          values: {
            obj: {
              __attr: { attr_1: 'some value' },
            },
            attr: 'attr_2',
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: '',
            isErr: false,
            err_t: undefined,
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'non valid args given',
      param: [
        {
          msg: 'value is "null"',
          rem: '(empty string is used by default)',
          values: {
            obj: {
              __attr: { attr_1: 'some value' },
            },
            attr: 'attr_2',
            defValue: null,
            key: undefined,
          },
          status: {
            ops: '',
            isErr: false,
            err_t: undefined,
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'a valid args given',
      param: [
        {
          msg: 'value is string',
          rem: '',
          values: {
            obj: {
              __attr: { attr_1: 'some value' },
            },
            attr: 'attr_2',
            defValue: 'default value',
            key: undefined,
          },
          status: {
            ops: 'default value',
            isErr: false,
            err_t: undefined,
            assertQty: 2,
          },
        },
      ],
    },
  ],
};

const resultTestData = {
  msg: 'run tests aganst result values',
  param: [
    {
      msg: 'attribute not exists',
      param: [
        {
          msg: 'function failed: empty string is returned',
          rem: '',
          values: {
            obj: {
              __attr: { attr_1: 'value of "attr_1"' },
              key1: { attr_2: 'value of "attr_2"' },
            },
            attr: 'attr_1',
            defValue: undefined,
            key: 'key1',
          },
          status: {
            ops: '',
            isErr: false,
            err_t: undefined,
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'attribute exists',
      param: [
        {
          msg: 'value of target attribute is not a string',
          rem: '(function failed: empty string is returned)',
          values: {
            obj: {
              __attr: { attr_1: 'value of "attr_1"' },
              key1: { attr_2: { value: 'not a string' } },
            },
            attr: 'attr_2',
            defValue: undefined,
            key: 'key1',
          },
          status: {
            ops: '',
            isErr: false,
            err_t: undefined,
            assertQty: 2,
          },
        },
      ],
    },
  ],
};

module.exports = {
  descr: [
    objTestData,
    attrTestData,
    keyTestData,
    defsTestData,
    resultTestData,
  ],
};
