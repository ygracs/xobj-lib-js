// [v0.1.002-20240906]

/* module:   `xObj-lib`
 * function: readXObjAttrAsIndex()
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const objTestData = {
  msg: 'run tests against "obj" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: undefined,
            attr: 'some_attr',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: null,
            attr: 'some_attr',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is an object',
          rem: '',
          values: {
            obj: { __attr: { some_attr: '172' } },
            attr: 'some_attr',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 172,
        },
        assertQty: 2,
      },
    }),
  ],
};

const attrTestData = {
  msg: 'run tests against "attr" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(failed: "-1" returned)',
          values: {
            obj: { __attr: { some_attr: '127' } },
            attr: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: -1,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(failed: "-1" returned)',
          values: {
            obj: { __attr: { some_attr: '127' } },
            attr: null,
            key: undefined,
          },
        }, {
          msg: 'value is an empty string',
          rem: '(failed: "-1" returned)',
          values: {
            obj: { __attr: { some_attr: '256' } },
            attr: '',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: -1,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is a non-empty string',
          rem: '',
          values: {
            obj: { __attr: { target_attr: '256' } },
            attr: 'target_attr',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 256,
        },
        assertQty: 2,
      },
    }),
  ],
};

const keyTestData = {
  msg: 'run tests against "key" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no valid args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(defaults must be used)',
          values: {
            obj: { __attr: { some_attr: '256' } },
            attr: 'some_attr',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 256,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(failed: "-1" returned)',
          values: {
            obj: { __attr: { some_attr: '127' } },
            attr: 'some_attr',
            key: null,
          },
        }, {
          msg: 'value is an empty string',
          rem: '(failed: "-1" returned)',
          values: {
            obj: { __attr: { some_attr: '127' } },
            attr: 'some_attr',
            key: '',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: -1,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is a non-empty string',
          rem: '',
          values: {
            obj: {
              __attr: { some_attr: '127' },
              target_key: { target_attr: '256' },
            },
            attr: 'target_attr',
            key: 'target_key',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 256,
        },
        assertQty: 2,
      },
    }),
  ],
};

const resultTestData = {
  msg: 'run tests against result values',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'target attribute is not exists',
      cases: [
        {
          msg: 'perform ops',
          rem: '(failed: default value returned)',
          values: {
            obj: {
              __attr: { some_attr: '256' },
            },
            attr: 'some_attr',
            key: 'target_attr',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: -1,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'cases for value type of target attribute',
      cases: [
        {
          msg: 'value is a positive number',
          rem: '(value be accepted)',
          values: {
            obj: {
              __attr: { target_attr: 127 },
            },
            attr: 'target_attr',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: 127,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'cases for value type of target attribute',
      cases: [
        {
          msg: 'value is a negative number',
          rem: '(failed: default value returned)',
          values: {
            obj: {
              __attr: { target_attr: -256 },
            },
            attr: 'target_attr',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: -1,
        },
        assertQty: 2,
      },
    }),
    ...genTestCase({
      inGroup: 'cases for value type of target attribute',
      cases: [
        {
          msg: 'value is a string not convertable to a number',
          rem: '(failed: default value returned)',
          values: {
            obj: {
              __attr: { target_attr: 'not number' },
            },
            attr: 'target_attr',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: -1,
        },
        assertQty: 2,
      },
    }),
  ],
};

module.exports = {
  //descr: [],
  tests: [
    objTestData,
    attrTestData,
    keyTestData,
    resultTestData,
  ],
};
