// [v0.1.001-20230203]

/* module:   `xObj-lib`
 * function: readXObjAttr()
 */

const objTestData = {
  msg: 'run tests against "obj" param',
  param: [
    {
      msg: 'no args given',
      param: [
        {
          msg: 'value is undefined',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: undefined,
            attr: 'attr_1',
            key: undefined,
          },
          status: {
            ops: {
              isErr: true,
              errType: TypeError,
              errCode: 'ERR_XOBJ_NPOBJ',
              value: undefined,
            },
            obj: undefined,
            assertQty: 3,
          },
        },
      ],
    }, {
      msg: 'non valid args given',
      param: [
        {
          msg: 'value is "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: null,
            attr: 'attr_1',
            key: undefined,
          },
          status: {
            ops: {
              isErr: true,
              errType: TypeError,
              errCode: 'ERR_XOBJ_NPOBJ',
              value: undefined,
            },
            obj: undefined,
            assertQty: 3,
          },
        },
      ],
    }, {
      msg: 'a valid args given',
      param: [
        {
          msg: 'value is proper object',
          rem: '',
          values: {
            obj: {
              __attr: { attr_1: 'value of "attr_1"' },
            },
            attr: 'attr_1',
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: 'value of "attr_1"',
            },
            obj: undefined,
            assertQty: 2,
          },
        },
      ],
    },
  ],
};

const attrTestData = {
  msg: 'run tests against "attr" param',
  param: [
    {
      msg: 'no args given',
      param: [
        {
          msg: 'value is undefined',
          rem: '(function failed: empty string is returned)',
          values: {
            obj: {
              __attr: { attr_1: 'value of "attr_1"' },
            },
            attr: undefined,
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: '',
            },
            obj: undefined,
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'non valid args given',
      param: [
        {
          msg: 'value is "null"',
          rem: '(function failed: empty string is returned)',
          values: {
            obj: {
              __attr: { attr_1: 'value of "attr_1"' },
            },
            attr: null,
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: '',
            },
            obj: undefined,
            assertQty: 2,
          },
        }, {
          msg: 'value is an empty string',
          rem: '(function failed: empty string is returned)',
          values: {
            obj: {
              __attr: { attr_1: 'value of "attr_1"' },
            },
            attr: '',
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: '',
            },
            obj: undefined,
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'a valid args given',
      param: [
        {
          msg: 'value is a non-empty string',
          rem: '(key exists)',
          values: {
            obj: {
              __attr: { attr_1: 'value of "attr_1"' },
              key1: { attr_2: 'value of "attr_2"' },
            },
            attr: 'attr_2',
            key: 'key1',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: 'value of "attr_2"',
            },
            obj: undefined,
            assertQty: 2,
          },
        },
      ],
    },
  ],
};

const keyTestData = {
  msg: 'run tests against "key" param',
  param: [
    {
      msg: 'no args given',
      param: [
        {
          msg: 'value is undefined',
          rem: '(a default "XOBJ_DEF_ATTR_TNAME" is used)',
          values: {
            obj: {
              __attr: { attr_1: 'value of "attr_1"' },
            },
            attr: 'attr_1',
            key: undefined,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: 'value of "attr_1"',
            },
            obj: undefined,
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'non valid args given',
      param: [
        {
          msg: 'value is "null"',
          rem: '(function failed: empty string is returned)',
          values: {
            obj: {
              __attr: { attr_1: 'value of "attr_1"' },
            },
            attr: 'attr_1',
            key: null,
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: '',
            },
            obj: undefined,
            assertQty: 2,
          },
        }, {
          msg: 'value is an empty string',
          rem: '(function failed: empty string is returned)',
          values: {
            obj: {
              __attr: { attr_1: 'value of "attr_1"' },
            },
            attr: 'attr_1',
            key: '',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: '',
            },
            obj: undefined,
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'a valid args given',
      param: [
        {
          msg: 'value is a non-empty string',
          rem: '(key exists)',
          values: {
            obj: {
              __attr: { attr_1: 'value of "attr_1"' },
              key1: { attr_2: 'value of "attr_2"' },
            },
            attr: 'attr_2',
            key: 'key1',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: 'value of "attr_2"',
            },
            obj: undefined,
            assertQty: 2,
          },
        },
      ],
    },
  ],
};

const resultTestData = {
  msg: 'run tests against result values',
  param: [
    {
      msg: 'attribute not exists',
      param: [
        {
          msg: 'perform ops',
          rem: '(function failed: empty string is returned)',
          values: {
            obj: {
              __attr: { attr_1: 'value of "attr_1"' },
              key1: { attr_2: 'value of "attr_2"' },
            },
            attr: 'attr_1',
            key: 'key1',
          },
          status: {
            ops: {
              isErr: false,
              errType: undefined,
              errCode: undefined,
              value: '',
            },
            obj: undefined,
          },
        },
      ],
    },
  ],
};

module.exports = {
  descr: [
    objTestData,
    attrTestData,
    keyTestData,
    resultTestData,
  ],
};
