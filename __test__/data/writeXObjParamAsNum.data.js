// [v0.1.003-20240906]

/* module:   `xObj-lib`
 * function: writeXObjParamAsNum()
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const objTestData = {
  msg: 'run tests against "obj" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: undefined,
            value: 127,
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: undefined,
            value: 127,
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is an object',
          rem: '',
          values: {
            obj: {},
            value: 127,
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __text: '127' },
        },
      },
    }),
  ],
};

const valueTestData = {
  msg: 'run tests against "value" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(failed: "false" returned)',
          values: {
            obj: {},
            value: undefined,
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {},
        },
      },
    }),
    ...genTestCase({
      inGroup: 'some args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(value will be converted to a number)',
          values: {
            obj: {},
            value: null,
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __text: '0' },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'some args are given',
      cases: [
        {
          msg: 'value is a boolean',
          rem: '(value will be converted to a number)',
          values: {
            obj: {},
            value: true,
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __text: '1' },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'some args are given',
      cases: [
        {
          msg: 'value is a "NaN"',
          rem: '("0" will be stored as a value)',
          values: {
            obj: {},
            value: NaN,
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __text: '0' },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'some args are given',
      cases: [
        {
          msg: 'value is a number',
          rem: '',
          values: {
            obj: {},
            value: 127,
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __text: '127' },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'some args are given',
      cases: [
        {
          msg: 'value is a string convertable to a number',
          rem: '(value accepted)',
          values: {
            obj: {},
            value: '1278',
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __text: '1278' },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'some args are given',
      cases: [
        {
          msg: 'value is a string not convertable to a number',
          rem: '("0" will be stored as a value)',
          values: {
            obj: {},
            value: 'some not convertable value',
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __text: '0' },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'some args are given',
      cases: [
        {
          msg: 'value is a string convertable to a boolean',
          rem: '(value will be converted to a number)',
          values: {
            obj: {},
            value: 'true',
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __text: '1' },
        },
      },
    }),
  ],
};

const keyTestData = {
  msg: 'run tests against "key" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(defaults must be used)',
          values: {
            obj: {},
            value: 127,
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __text: '127' },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(failed: "false" returned)',
          values: {
            obj: {},
            value: 127,
            defValue: undefined,
            key: null,
          },
        }, {
          msg: 'value is an empty string',
          rem: '(failed: "false" returned)',
          values: {
            obj: {},
            value: 127,
            defValue: undefined,
            key: '',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {},
        },
      },
    }),
    ...genTestCase({
      inGroup: 'valid args given',
      cases: [
        {
          msg: 'value is a non-empty string',
          rem: '',
          values: {
            obj: {},
            value: 265,
            defValue: undefined,
            key: 'some_key',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { some_key: '265' },
        },
      },
    }),
  ],
};

const dvalTestData = {
  msg: 'run tests against "defValue" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(defaults must be used)',
          values: {
            obj: {},
            value: 'not a number',
            defValue: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __text: '0' },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(defaults must be used)',
          values: {
            obj: {},
            value: 'not a number',
            defValue: null,
            key: undefined,
          },
        }, {
          msg: 'value is a "NaN"',
          rem: '(defaults must be used)',
          values: {
            obj: {},
            value: 'not a number',
            defValue: NaN,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __text: '0' },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'valid args given',
      cases: [
        {
          msg: 'value is a number',
          rem: '',
          values: {
            obj: {},
            value: 'not a number',
            defValue: 127,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __text: '127' },
        },
      },
    }),
  ],
};

const resultTestData = {
  msg: 'run tests against result value',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'special cases',
      cases: [
        {
          msg: '"value" param is not given but "defValue" is',
          rem: '(a "defValue" must be applied if number)',
          values: {
            obj: {},
            value: undefined,
            defValue: 127,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: { __text: '127' },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'special cases',
      cases: [
        {
          msg: '"value" param is not given but "defValue" is',
          rem: '(a "defValue" must be ignored if not number)',
          values: {
            obj: {},
            value: undefined,
            defValue: 'not number',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {},
        },
      },
    }),
  ],
};

module.exports = {
  //descr: [],
  tests: [
    objTestData,
    valueTestData,
    keyTestData,
    dvalTestData,
    resultTestData,
  ],
};
