// [v0.1.006-20240910]

/* module:   `xObj-lib`
 * function: deleteXObjElementEx()
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const objTestData = {
  msg: 'run tests against "obj" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: undefined,
            name: 'some_key',
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: null,
            name: 'some_key',
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is a non-empty object',
          rem: '',
          values: {
            obj: { some_key: { __text: 'some text' } },
            name: 'some_key',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: {
            isSucceed: true,
            item: { __text: 'some text' },
          },
        },
        assertQty: 2,
        before: {
          obj: { some_key: { __text: 'some text' } },
        },
        after: {
          obj: {},
        },
      },
    }),
  ],
};

const nameTestData = {
  msg: 'run tests against "name" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: { some_key: 'some text' },
            name: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NSTR',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: {
          obj: { some_key: 'some text' },
        },
        after: {
          obj: { some_key: 'some text' },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: { some_key: 'some text' },
            name: null,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NSTR',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: {
          obj: { some_key: 'some text' },
        },
        after: {
          obj: { some_key: 'some text' },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is an empty string',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: { some_key: 'some text' },
            name: '',
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_INVARG_KEY',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: {
          obj: { some_key: 'some text' },
        },
        after: {
          obj: { some_key: 'some text' },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is a non-empty string',
          rem: '',
          values: {
            obj: { some_key: 'some text' },
            name: 'some_key',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: {
            isSucceed: true,
            item: null,
          },
        },
        assertQty: 2,
        before: {
          obj: { some_key: 'some text' },
        },
        after: {
          obj: {},
        },
      },
    }),
  ],
};

const resultTestData = {
  msg: 'run tests against result values',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'special cases against "obj" param',
      cases: [
        {
          msg: 'value is an empty object',
          rem: '',
          values: {
            obj: {},
            name: 'some_key',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: {
            isSucceed: true,
            item: null,
          },
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {},
        },
      },
    }),
    ...genTestCase({
      inGroup: 'tests against target element',
      cases: [
        {
          msg: 'target element is a string',
          rem: '',
          values: {
            obj: { some_key: 'some text' },
            name: 'some_key',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: {
            isSucceed: true,
            item: null,
          },
        },
        assertQty: 2,
        before: {
          obj: { some_key: 'some text' },
        },
        after: {
          obj: {},
        },
      },
    }),
    ...genTestCase({
      inGroup: 'tests against target element',
      cases: [
        {
          msg: 'target element is an array',
          rem: '',
          values: {
            obj: { some_key: [ 'some text' ] },
            name: 'some_key',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: {
            isSucceed: true,
            item: [ 'some text' ],
          },
        },
        assertQty: 2,
        before: {
          obj: { some_key: [ 'some text' ] },
        },
        after: {
          obj: {},
        },
      },
    }),
  ],
};

module.exports = {
  //descr: [],
  tests: [
    objTestData,
    nameTestData,
    resultTestData,
  ],
};
