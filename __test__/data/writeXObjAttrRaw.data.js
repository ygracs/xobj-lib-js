// [v0.1.010-20240904]

/* module:   `xObj-lib`
 * function: writeXObjAttrRaw()
 */

const { genTestCase } = require('#test-dir/test-hfunc.js');

const TEST_FUNC = (value) => { return value; };

const objTestData = {
  msg: 'run tests against "obj" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: undefined,
            attr: 'some_attr',
            value: 'attribute value',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: null,
            attr: 'some_attr',
            value: 'attribute value',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NPOBJ',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is an object',
          rem: '',
          values: {
            obj: {},
            attr: 'some_attr',
            value: 'attribute value',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {
            __attr: { some_attr: 'attribute value' },
          },
        },
      },
    }),
  ],
};

const attrTestData = {
  msg: 'run tests against "attr" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(failed: "false" returned)',
          values: {
            obj: {},
            attr: undefined,
            value: 'attribute value',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {},
        },
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: {},
            attr: null,
            value: 'attribute value',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: true,
          errType: TypeError,
          errCode: 'ERR_XOBJ_NSTR',
          className: undefined,
          value: undefined,
        },
        assertQty: 3,
        before: undefined,
        after: undefined,
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is an empty string',
          rem: '(failed: "false" returned)',
          values: {
            obj: {},
            attr: '',
            value: 'attribute value',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {},
        },
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is a non-empty string',
          rem: '',
          values: {
            obj: {},
            attr: 'some_attr',
            value: 'attribute value',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {
            __attr: { some_attr: 'attribute value' },
          },
        },
      },
    }),
  ],
};

const valueTestData = {
  msg: 'run tests against "value" param',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(failed: "false" returned)',
          values: {
            obj: {},
            attr: 'some_attr',
            value: undefined,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {},
        },
      },
    }),
    ...genTestCase({
      inGroup: 'some args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '',
          values: {
            obj: {},
            attr: 'some_attr',
            value: null,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {
            __attr: { some_attr: null },
          },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'some args are given',
      cases: [
        {
          msg: 'value is a boolean',
          rem: '',
          values: {
            obj: {},
            attr: 'some_attr',
            value: true,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {
            __attr: { some_attr: true },
          },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'some args are given',
      cases: [
        {
          msg: 'value is a number',
          rem: '',
          values: {
            obj: {},
            attr: 'some_attr',
            value: 127,
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {
            __attr: { some_attr: 127 },
          },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is a string',
          rem: '',
          values: {
            obj: {},
            attr: 'some_attr',
            value: 'attribute value',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {
            __attr: { some_attr: 'attribute value' },
          },
        },
      },
    }),
  ],
};

const keyTestData = {
  msg: 'run tests against "key" param',
  rem: '',
  param: [
    ...genTestCase({
      inGroup: 'no args are given',
      cases: [
        {
          msg: 'undefined value is passed',
          rem: '(defaults must be used)',
          values: {
            obj: {},
            attr: 'some_attr',
            value: 'attribute value',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {
            __attr: { some_attr: 'attribute value' },
          },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'non-valid args are given',
      cases: [
        {
          msg: 'value is a "null"',
          rem: '(failed: "false" returned)',
          values: {
            obj: {},
            attr: 'some_attr',
            value: 'attribute value',
            key: null,
          },
        }, {
          msg: 'value is an empty string',
          rem: '(failed: "false" returned)',
          values: {
            obj: {},
            attr: 'some_attr',
            value: 'attribute value',
            key: '',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: false,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {},
        },
      },
    }),
    ...genTestCase({
      inGroup: 'valid args are given',
      cases: [
        {
          msg: 'value is a non-empty string',
          rem: '',
          values: {
            obj: {},
            attr: 'some_attr',
            value: 'attribute value',
            key: 'target_key',
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {},
        },
        after: {
          obj: {
            target_key: { some_attr: 'attribute value' },
          },
        },
      },
    }),
  ],
};

const resultTestData = {
  msg: 'run tests against result values',
  rem: '(defaults used)',
  param: [
    ...genTestCase({
      inGroup: 'tests against attributes container type',
      cases: [
        {
          msg: 'container value set to a "null"',
          rem: '',
          values: {
            obj: {
              __attr: null,
            },
            attr: 'some_attr',
            value: 'attribute value',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {
            __attr: null,
          },
        },
        after: {
          obj: {
            __attr: { some_attr: 'attribute value' },
          },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'tests against attributes container type',
      cases: [
        {
          msg: 'container value set to a boolean',
          rem: '',
          values: {
            obj: {
              __attr: true,
            },
            attr: 'some_attr',
            value: 'attribute value',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {
            __attr: true,
          },
        },
        after: {
          obj: {
            __attr: { some_attr: 'attribute value' },
          },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'tests against attributes container type',
      cases: [
        {
          msg: 'container value set to a number',
          rem: '',
          values: {
            obj: {
              __attr: 127,
            },
            attr: 'some_attr',
            value: 'attribute value',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {
            __attr: 127,
          },
        },
        after: {
          obj: {
            __attr: { some_attr: 'attribute value' },
          },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'tests against attributes container type',
      cases: [
        {
          msg: 'container value set to a string',
          rem: '',
          values: {
            obj: {
              __attr: 'some value',
            },
            attr: 'some_attr',
            value: 'attribute value',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {
            __attr: 'some value',
          },
        },
        after: {
          obj: {
            __attr: { some_attr: 'attribute value' },
          },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'tests against attributes container type',
      cases: [
        {
          msg: 'container value set to an object',
          rem: '',
          values: {
            obj: {
              __attr: {},
            },
            attr: 'some_attr',
            value: 'attribute value',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {
            __attr: {},
          },
        },
        after: {
          obj: {
            __attr: { some_attr: 'attribute value' },
          },
        },
      },
    }),
    ...genTestCase({
      inGroup: 'tests against attributes container type',
      cases: [
        {
          msg: 'container value set to an array',
          rem: '',
          values: {
            obj: {
              __attr: [],
            },
            attr: 'some_attr',
            value: 'attribute value',
            key: undefined,
          },
        },
      ],
      status: {
        ops: {
          isERR: false,
          errType: undefined,
          errCode: undefined,
          className: undefined,
          value: true,
        },
        assertQty: 2,
        before: {
          obj: {
            __attr: [],
          },
        },
        after: {
          obj: {
            __attr: { some_attr: 'attribute value' },
          },
        },
      },
    }),
  ],
};

module.exports = {
  //descr: [],
  tests: [
    objTestData,
    attrTestData,
    valueTestData,
    keyTestData,
    resultTestData,
  ],
};
