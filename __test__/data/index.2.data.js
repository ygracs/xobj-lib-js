// [v0.1.009-20230206]

/* module:   `xObj-lib`
 * function: ---
 */

module.exports = {
  writeXObjParamRaw: require('./writeXObjParamRaw.data.js'),
  writeXObjAttrRaw: require('./writeXObjAttrRaw.data.js'),
  writeXObjParam: require('./writeXObjParam.data.js'),
  writeXObjAttr: require('./writeXObjAttr.data.js'),
  writeXObjParamAsBool: require('./writeXObjParamAsBool.data.js'),
  writeXObjParamAsNum: require('./writeXObjParamAsNum.data.js'),
  writeXObjParamAsIndex: require('./writeXObjParamAsIndex.data.js'),
  writeXObjParamEx: require('./writeXObjParamEx.data.js'),
  writeXObjAttrAsBool: require('./writeXObjAttrAsBool.data.js'),
  writeXObjAttrAsNum: require('./writeXObjAttrAsNum.data.js'),
  writeXObjAttrAsIndex: require('./writeXObjAttrAsIndex.data.js'),
  writeXObjAttrEx: require('./writeXObjAttrEx.data.js'),
};
