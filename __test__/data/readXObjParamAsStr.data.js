// [v0.1.001-20230202]

/* module:   `xObj-lib`
 * function: readXObjParamAsStr()
 */

const objTestData = {
  msg: 'run tests aganst "obj" param',
  param: [
    {
      msg: 'no args given',
      param: [
        {
          msg: 'value is undefined',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: undefined,
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: undefined,
            isErr: true,
            err_t: TypeError,
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'non valid args given',
      param: [
        {
          msg: 'value is "null"',
          rem: '(a "TypeError" is thrown)',
          values: {
            obj: null,
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: undefined,
            isErr: true,
            err_t: TypeError,
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'a valid args given',
      param: [
        {
          msg: 'value is proper object',
          rem: '',
          values: {
            obj: { __text: 'some value', },
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: 'some value',
            isErr: false,
            err_t: undefined,
            assertQty: 2,
          },
        },
      ],
    },
  ],
};

const keyTestData = {
  msg: 'run tests aganst "key" param',
  param: [
    {
      msg: 'no args given',
      param: [
        {
          msg: 'value is undefined',
          rem: '(a default "XOBJ_DEF_PARAM_TNAME" is used)',
          values: {
            obj: { __text: 'some value' },
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: 'some value',
            isErr: false,
            err_t: undefined,
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'non valid args given',
      param: [
        {
          msg: 'value is "null"',
          rem: '(function failed: empty string is returned)',
          values: {
            obj: { __text: 'some value' },
            defValue: undefined,
            key: null,
          },
          status: {
            ops: '',
            isErr: false,
            err_t: undefined,
            assertQty: 2,
          },
        }, {
          msg: 'value is an empty string',
          rem: '(function failed: empty string is returned)',
          values: {
            obj: { __text: 'some value' },
            defValue: undefined,
            key: '',
          },
          status: {
            ops: '',
            isErr: false,
            err_t: undefined,
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'a valid args given',
      param: [
        {
          msg: 'value is a non-empty string',
          rem: '(key exists)',
          values: {
            obj: {
              __text: 'some value',
              key1: 'value of "key1"',
            },
            defValue: undefined,
            key: 'key1',
          },
          status: {
            ops: 'value of "key1"',
            isErr: false,
            err_t: undefined,
            assertQty: 2,
          },
        },
      ],
    },
  ],
};

const defsTestData = {
  msg: 'run tests aganst "defValue" param',
  param: [
    {
      msg: 'no args given',
      param: [
        {
          msg: 'value is undefined',
          rem: '(empty string is used by default)',
          values: {
            obj: { __text: null, },
            defValue: undefined,
            key: undefined,
          },
          status: {
            ops: '',
            isErr: false,
            err_t: undefined,
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'non valid args given',
      param: [
        {
          msg: 'value is "null"',
          rem: '(empty string is used by default)',
          values: {
            obj: { __text: null, },
            defValue: null,
            key: undefined,
          },
          status: {
            ops: '',
            isErr: false,
            err_t: undefined,
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'a valid args given',
      param: [
        {
          msg: 'value is a string',
          rem: '',
          values: {
            obj: { __text: null, },
            defValue: 'default value',
            key: undefined,
          },
          status: {
            ops: 'default value',
            isErr: false,
            err_t: undefined,
            assertQty: 2,
          },
        },
      ],
    },
  ],
};

const resultTestData = {
  msg: 'run tests aganst result values',
  param: [
    {
      msg: 'key not exists',
      param: [
        {
          msg: 'result is empty string',
          rem: '',
          values: {
            obj: { __text: 'true' },
            defValue: undefined,
            key: 'key1',
          },
          status: {
            ops: '',
            isErr: false,
            err_t: undefined,
            assertQty: 2,
          },
        },
      ],
    }, {
      msg: 'key exists',
      param: [
        {
          msg: 'value of target key is "null"',
          rem: '(function failed: empty string is returned)',
          values: {
            obj: {
              __text: 'true',
              key1: null,
            },
            defValue: undefined,
            key: 'key1',
          },
          status: {
            ops: '',
            isErr: false,
            err_t: undefined,
            assertQty: 2,
          },
        }, {
          msg: 'value of target key is boolean',
          rem: '(value will be converted to a string)',
          values: {
            obj: {
              __text: 'true',
              key1: false,
            },
            defValue: undefined,
            key: 'key1',
          },
          status: {
            ops: 'false',
            isErr: false,
            err_t: undefined,
            assertQty: 2,
          },
        }, {
          msg: 'value of target key is number',
          rem: '(value will be converted to a string)',
          values: {
            obj: {
              __text: 'true',
              key1: -124,
            },
            defValue: undefined,
            key: 'key1',
          },
          status: {
            ops: '-124',
            isErr: false,
            err_t: undefined,
            assertQty: 2,
          },
        },
      ],
    },
  ],
};

module.exports = {
  descr: [
    objTestData,
    keyTestData,
    defsTestData,
    resultTestData,
  ],
};
