>|***rev.*:**|0.0.2|
>|:---|---:|
>|date:|2023-01-17|

## Introduction

This paper describes a tests shipped within the package.

## Provided tests

- `xObj-lib` module:
  - `c1` (read ops param/attr);
  - `c2` (write ops param/attr);
  - `c3` and `c4` (ops with elements).

## Use cases

To run all tests available use the next command:

`npm test` or `npm run test` or `npm run test-xobj`

To run a separate tests use the next command:

`npm run test-xobj:<t_grp>`

where `<t_grp>` is a targeted group of the tests (i.e. `c1`, `c2`, etc).
