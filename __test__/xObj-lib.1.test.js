// [v0.1.024-20240906]

const xObj = require('#lib/xObj-lib.js');

const t_Mod = require('#lib/xObj-lib.js');

const { runTestFn } = require('#test-dir/test-hfunc.js');

const t_Dat = require('./data/index.1.data.js');

describe('1. special r/w ops applied for attr/param', () => {
  describe.each([
    {
      msg: '1.1 - function',
      method: 'readXObjParamRaw',
    }, {
      msg: '1.2 - function',
      method: 'readXObjAttrRaw',
    },
  ])('$msg: xObj.$method()', ({ method }) => {
    const testData = t_Dat[method];
    describe.each(testData.tests)('+ $msg $rem', ({ param }) => {
      describe.each(param)('+ $msg', ({ param }) => {
        it.each(param)('+ $msg $rem', ({ values, status }) => {
          const { ops, assertQty } = status;
          let result = undefined; let isERR = false;
          try {
            result = runTestFn({ testInst: t_Mod, method }, values);
            //console.log('CHECK: > NO_ERR');
          } catch (err) {
            //console.log('CHECK: > '+err);
            isERR = true;
            result = err;
          } finally {
            expect.assertions(assertQty);
            expect(isERR).toBe(ops.isERR);
            if (isERR) {
              const { errType, errCode } = ops;
              expect((result instanceof errType)).toBe(true);
              expect(result.code).toStrictEqual(errCode);
            } else {
              const { className, value } = ops;
              switch (typeof className) {
                case 'string' : {
                  expect(result instanceof globalThis[className]).toBe(true);
                  if (className !== 'HTMLElement') break;
                }
                default : {
                  expect(result).toStrictEqual(value);
                }
              };
            };
          };
        });
      });
    });
  });

});

describe('2. base r/w ops applied for attr/param', () => {
  // *** function: readXObjParam()
  describe('2.1 - function: xObj.readXObjParam', () => {
    const method = 'readXObjParam';
    const testData = t_Dat[method];
    describe.each(testData.descr)('$msg', ({ param }) => {
      describe.each(param)('$msg', ({ param }) => {
        const testFn = (...args) => { return xObj[method](...args); };
        it.each(param)('$msg $rem', ({ values, status }) => {
          const { ops, assertQty } = status;
          const { isErr, errType, errCode } = ops;
          let { obj, key } = values;
          let result = undefined;
          try {
            result = testFn(obj, key);
            expect(isErr).toBe(false);
            //console.log('CHECK: > NO_ERR');
            expect(result).toStrictEqual(ops.value);
          } catch (err) {
            expect(isErr).toBe(true);
            //console.log('CHECK: > '+err);
            expect((err instanceof errType)).toBe(true);
            expect(err.code).toStrictEqual(errCode);
          } finally {
            expect.assertions(assertQty);
          };
        });
      });
    });
  });

  // *** function: readXObjAttr()
  describe('2.2 - function: xObj.readXObjAttr', () => {
    const method = 'readXObjAttr';
    const testData = t_Dat[method];
    describe.each(testData.descr)('$msg', ({ param }) => {
      describe.each(param)('$msg', ({ param }) => {
        const testFn = (...args) => { return xObj[method](...args); };
        it.each(param)('$msg $rem', ({ values, status }) => {
          const { ops, assertQty } = status;
          const { isErr, errType, errCode } = ops;
          let { obj, attr, key } = values;
          let result = undefined;
          try {
            result = testFn(obj, attr, key);
            expect(isErr).toBe(false);
            //console.log('CHECK: > NO_ERR');
            expect(result).toStrictEqual(ops.value);
          } catch (err) {
            expect(isErr).toBe(true);
            //console.log('CHECK: > '+err);
            expect((err instanceof errType)).toBe(true);
            expect(err.code).toStrictEqual(errCode);
          } finally {
            expect.assertions(assertQty);
          };
        });
      });
    });
  });
});

describe('3. enhanced r/w ops applied for attr/param', () => {
  describe.each([
    {
      msg: '3.1 - function',
      method: 'readXObjParamAsBool',
    }, {
      msg: '3.2 - function',
      method: 'readXObjParamAsNum',
    }, {
      msg: '3.4 - function',
      method: 'readXObjParamAsIndex',
    }, {
      msg: '3.5 - function',
      method: 'readXObjAttrAsBool',
    }, {
      msg: '3.6 - function',
      method: 'readXObjAttrAsNum',
    }, {
      msg: '3.8 - function',
      method: 'readXObjAttrAsIndex',
    },
  ])('$msg: xObj.$method()', ({ method }) => {
    const testData = t_Dat[method];
    describe.each(testData.tests)('+ $msg $rem', ({ param }) => {
      describe.each(param)('+ $msg', ({ param }) => {
        it.each(param)('+ $msg $rem', ({ values, status }) => {
          const { ops, assertQty } = status;
          let result = undefined; let isERR = false;
          try {
            result = runTestFn({ testInst: t_Mod, method }, values);
            //console.log('CHECK: > NO_ERR');
          } catch (err) {
            //console.log('CHECK: > '+err);
            isERR = true;
            result = err;
          } finally {
            expect.assertions(assertQty);
            expect(isERR).toBe(ops.isERR);
            if (isERR) {
              const { errType, errCode } = ops;
              expect((result instanceof errType)).toBe(true);
              expect(result.code).toStrictEqual(errCode);
            } else {
              const { className, value } = ops;
              switch (typeof className) {
                case 'string' : {
                  expect(result instanceof globalThis[className]).toBe(true);
                  if (className !== 'HTMLElement') break;
                }
                default : {
                  expect(result).toStrictEqual(value);
                }
              };
            };
          };
        });
      });
    });
  });

  // *** function: readXObjParamAsStr()
  describe('3.3 - function: xObj.readXObjParamAsStr', () => {
    const method = 'readXObjParamAsStr';
    const testData = t_Dat[method];
    describe.each(testData.descr)('$msg', ({ param }) => {
      describe.each(param)('$msg', ({ param }) => {
        const testFn = (...args) => { return xObj[method](...args); };
        it.each(param)('$msg $rem', ({ values, status }) => {
          const { ops, isErr, err_t, assertQty } = status;
          let { obj, defValue, key } = values;
          let result = undefined;
          try {
            result = testFn(obj, defValue, key);
            expect(isErr).toBe(false);
            //console.log('CHECK: > NO_ERR');
            expect(result).toStrictEqual(ops);
          } catch (err) {
            expect(isErr).toBe(true);
            //console.log('CHECK: > '+err);
            expect((err instanceof err_t)).toBe(true);
          } finally {
            expect.assertions(assertQty);
          };
        });
      });
    });
  });

  // *** function: readXObjAttrAsStr()
  describe('3.7 - function: xObj.readXObjAttrAsStr', () => {
    const method = 'readXObjAttrAsStr';
    const testData = t_Dat[method];
    describe.each(testData.descr)('$msg', ({ param }) => {
      describe.each(param)('$msg', ({ param }) => {
        const testFn = (...args) => { return xObj[method](...args); };
        it.each(param)('$msg $rem', ({ values, status }) => {
          const { ops, isErr, err_t, assertQty } = status;
          let { obj, attr, defValue, key } = values;
          let result = undefined;
          try {
            result = testFn(obj, attr, defValue, key);
            expect(isErr).toBe(false);
            //console.log('CHECK: > NO_ERR');
            expect(result).toStrictEqual(ops);
          } catch (err) {
            expect(isErr).toBe(true);
            //console.log('CHECK: > '+err);
            expect((err instanceof err_t)).toBe(true);
          } finally {
            expect.assertions(assertQty);
          };
        });
      });
    });
  });

});
