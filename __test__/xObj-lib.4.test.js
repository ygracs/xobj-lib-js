// [v0.1.023-20240915]

const xObj = require('#lib/xObj-lib.js');

const t_Mod = require('#lib/xObj-lib.js');

const { runTestFn } = require('#test-dir/test-hfunc.js');

const t_Dat = require('./data/index.4.data.js');

describe('2. experimental ops applied for elements', () => {
  // *** function: insertXObjElements()
  describe('2.1 - function: xObj.insertXObjElements', () => {
    const method = 'insertXObjElements';
    const testData = t_Dat[method];
    describe.each(testData.descr)('$msg', ({ param }) => {
      describe.each(param)('$msg', ({ param }) => {
        const testFn = (...args) => { return xObj[method](...args); };
        it.each(param)('$msg $rem', ({ values, status }) => {
          const { ops, assertQty } = status;
          let [ obj ] = values;
          let result = undefined; let isERR = false;
          try {
            result = testFn(...values);
            //console.log('CHECK: > NO_ERR');
          } catch (err) {
            //console.log('CHECK: > '+err);
            isERR = true;
            result = err;
          } finally {
            expect.assertions(assertQty);
            expect(isERR).toBe(ops.isErr);
            if (isERR) {
              const { errType, errCode } = ops;
              expect((result instanceof errType)).toBe(true);
              expect(result.code).toStrictEqual(errCode);
            } else {
              expect(result).toStrictEqual(ops.value);
            };
            expect(obj).toStrictEqual(status.obj);
          };
        });
      });
    });
  });

  describe.each([
    {
      msg: '2.2 - function',
      method: 'insertXObjEChain',
    },
  ])('$msg: xObj.$method()', ({ method }) => {
    const testData = t_Dat[method];
    describe.each(testData.tests)('+ $msg $rem', ({ param }) => {
      describe.each(param)('+ $msg', ({ param }) => {
        describe.each(param)('+ $msg $rem', ({ values, status }) => {
          const { before: stat_b, after: stat_a } = status;
          let obj = undefined;
          beforeAll(() => {
            if (stat_b || stat_a) { ([ obj ] = values); };
          });
          if (stat_b) {
            it('check object status (before)', () => {
              expect(obj).toStrictEqual(stat_b.obj);
            });
          };
          it('perform ops', () => {
            const { ops, assertQty } = status;
            let result = undefined; let isERR = false;
            try {
              result = runTestFn({ testInst: t_Mod, method }, values);
              //console.log('CHECK: > NO_ERR');
            } catch (err) {
              //console.log('CHECK: > '+err);
              isERR = true;
              result = err;
            } finally {
              expect.assertions(assertQty);
              expect(isERR).toBe(ops.isERR);
              if (isERR) {
                const { errType, errCode } = ops;
                expect((result instanceof errType)).toBe(true);
                expect(result.code).toStrictEqual(errCode);
              } else {
                const { className, value } = ops;
                switch (typeof className) {
                  case 'string' : {
                    expect(result instanceof globalThis[className]).toBe(true);
                    if (className !== 'HTMLElement') break;
                  }
                  default : {
                    expect(result).toStrictEqual(value);
                  }
                };
              };
            };
          });
          if (stat_a) {
            it('check object status (after)', () => {
              expect(obj).toStrictEqual(stat_a.obj);
            });
          };
        });
      });
    });
  });
  //===
});

describe('3. other module functions', () => {
  // *** function: evalXObjEName()
  describe('3.1 - function: xObj.evalXObjEName', () => {
    const method = 'evalXObjEName';
    const testData = t_Dat[method];
    describe.each(testData.descr)('$msg', ({ param }) => {
      describe.each(param)('$msg', ({ param }) => {
        const testFn = (...args) => { return xObj[method](...args); };
        it.each(param)('$msg $rem', ({ values, status }) => {
          const { ops, assertQty } = status;
          let { name } = values;
          let result = undefined; let isERR = false;
          try {
            result = testFn(name);
            //console.log('CHECK: > NO_ERR');
          } catch (err) {
            //console.log('CHECK: > '+err);
            isERR = true;
            result = err;
          } finally {
            expect.assertions(assertQty);
            expect(isERR).toBe(ops.isErr);
            if (isERR) {
              const { errType, errCode } = ops;
              expect((result instanceof errType)).toBe(true);
              expect(result.code).toStrictEqual(errCode);
            } else {
              expect(result).toStrictEqual(ops.value);
            };
          };
        });
      });
    });
  });

  // *** function: genXObjENameDescr()
  describe('3.2 - function: xObj.genXObjENameDescr', () => {
    const method = 'genXObjENameDescr';
    const testData = t_Dat[method];
    describe.each(testData.descr)('$msg', ({ param }) => {
      describe.each(param)('$msg', ({ param }) => {
        const testFn = (...args) => { return xObj[method](...args); };
        it.each(param)('$msg $rem', ({ values, status }) => {
          const { ops, assertQty } = status;
          let { name } = values;
          let result = undefined; let isERR = false;
          try {
            result = testFn(name);
            //console.log('CHECK: > NO_ERR');
          } catch (err) {
            //console.log('CHECK: > '+err);
            isERR = true;
            result = err;
          } finally {
            expect.assertions(assertQty);
            expect(isERR).toBe(ops.isErr);
            if (isERR) {
              const { errType, errCode } = ops;
              expect((result instanceof errType)).toBe(true);
              expect(result.code).toStrictEqual(errCode);
            } else {
              expect(result).toStrictEqual(ops.value);
            };
          };
        });
      });
    });
  });
});
