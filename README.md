|***rev.*:**|0.1.2|
|:---|---:|
|***date***:|2024-09-15|

## Introduction

This module provide a helper functions for object manipulation. It was primarily written to work with a XML-parse provided by a [`xml-js`](https://www.npmjs.com/package/xml-js) module for 'Node.js' running in "compact" mode.

> Note: for more see `xObj.md` in the project `doc` directory.

> Notes on upgrade to `v0.2.0`
> > The `v0.2.0` is not fully compatible with a previous one due to some breaking changes. For more details read the docs for a functions used in your project.

## Use cases

### Installation

`npm install @ygracs/xobj-lib-js`
