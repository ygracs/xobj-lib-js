// [v0.1.060-20240623]

// === module init block ===

const {
  //valueToIndex,
  //readAsString, readAsBoolEx, readAsNumberEx,
  //isNullOrUndef,
  //isArray, isObject,
  isPlainObject, //readAsListS,
} = require('@ygracs/bsfoc-lib-js');

// === module extra block (helper functions) ===

// === module main block ===

/***
 * (* constant definitions *)
 */

const XOBJ_DEF_PARAM_TNAME = '__text';
const XOBJ_DEF_ATTR_TNAME = '__attr';

const XOBJ_TE_INVARG_EMSG = 'invalid argument';
const XOBJ_TE_NOBJ_EMSG = `${XOBJ_TE_INVARG_EMSG} (an object expected)`;
const XOBJ_TE_NOBJ_ECODE = 'ERR_XOBJ_NOBJ';
const XOBJ_TE_NARR_EMSG = `${XOBJ_TE_INVARG_EMSG} (an array expected)`;
const XOBJ_TE_NARR_ECODE = 'ERR_XOBJ_NARR';
const XOBJ_TE_NSTR_EMSG = `${XOBJ_TE_INVARG_EMSG} (a string expected)`;
const XOBJ_TE_NSTR_ECODE = 'ERR_XOBJ_NSTR';
const XOBJ_TE_NPOBJ_EMSG = `${XOBJ_TE_INVARG_EMSG} (a plain object expected)`;
const XOBJ_TE_NPOBJ_ECODE = 'ERR_XOBJ_NPOBJ';
const XOBJ_TE_ANES_EMSG = '<attr_name> must be a non-empty string';
const XOBJ_TE_ANES_ECODE = 'ERR_XOBJ_INVARG_ATTR';
const XOBJ_TE_KNES_EMSG = '<key_name> must be a non-empty string';
const XOBJ_TE_KNES_ECODE = 'ERR_XOBJ_INVARG_KEY';

const DEF_XML_PARSE_OPTIONS = {
  compact: true,
  declarationKey: '__decl',
  attributesKey: XOBJ_DEF_ATTR_TNAME,
  textKey: XOBJ_DEF_PARAM_TNAME,
  commentKey: '__note',
  cdataKey: '__cdata',
  nameKey: '__name',
  typeKey: '__type',
  parentKey: 'parent',
  elementsKey: 'items',
  ignoreDeclaration: false,
  ignoreDocType: false,
  ignoreInstractions: false,
  ignoreText: false,
  ignoreComments: false,
  ignoreCData: false,
  fullTagEmptyElement: true,
  addParent: false,
  trim: true,
  spaces: 2,
};

/***
 * (* function definitions *)
 */

/***
 * (* class definitions *)
 */

class TXmlContentParseOptions {
  #_options = null;

  constructor(param){
    this.#_options = TXmlContentParseOptions.createNewOptionsSet(param);
  }

  get settings(){ return this.#_options }

  get xml2js(){
    let _settings = this.#_options;
    return {
      compact: _settings.compact,
      declarationKey: _settings.declarationKey,
      attributesKey: _settings.attributesKey,
      textKey: _settings.textKey,
      commentKey: _settings.commentKey,
      cdataKey: _settings.cdataKey,
      nameKey: _settings.nameKey,
      typeKey: _settings.typeKey,
      parentKey: _settings.parentKey,
      elementsKey: _settings.elementsKey,
      ignoreDeclaration: _settings.ignoreDeclaration,
      ignoreDocType: _settings.ignoreDocType,
      ignoreInstraction: _settings.ignoreInstractions,
      ignoreText: _settings.ignoreText,
      ignoreComment: _settings.ignoreComments,
      ignoreCData: _settings.ignoreCData,
      addParent: _settings.addParent,
      trim: _settings.trim,
    };
  }

  get js2xml(){
    let _settings = this.#_options;
    return {
      compact: _settings.compact,
      declarationKey: _settings.declarationKey,
      attributesKey: _settings.attributesKey,
      textKey: _settings.textKey,
      commentKey: _settings.commentKey,
      cdataKey: _settings.cdataKey,
      nameKey: _settings.nameKey,
      typeKey: _settings.typeKey,
      parentKey: _settings.parentKey,
      elementsKey: _settings.elementsKey,
      ignoreDeclaration: _settings.ignoreDeclaration,
      ignoreDocType: _settings.ignoreDocType,
      ignoreInstraction: _settings.ignoreInstractions,
      ignoreText: _settings.ignoreText,
      ignoreComment: _settings.ignoreComments,
      ignoreCData: _settings.ignoreCData,
      fullTagEmptyElement: _settings.fullTagEmptyElement,
      spaces: _settings.spaces,
    };
  }

  static createNewOptionsSet(opt){
    if (opt instanceof TXmlContentParseOptions) {
      opt = opt.settings;
    } else if (isPlainObject(opt)) {
      opt = isPlainObject(opt.settings) ? opt.settings : opt;
    } else {
      opt = DEF_XML_PARSE_OPTIONS;
    };
    return {
      compact: opt.compact,
      declarationKey: opt.declarationKey,
      attributesKey: opt.attributesKey,
      textKey: opt.textKey,
      commentKey: opt.commentKey,
      cdataKey: opt.cdataKey,
      nameKey: opt.nameKey,
      typeKey: opt.typeKey,
      parentKey: opt.parentKey,
      elementsKey: opt.elementsKey,
      ignoreDeclaration: opt.ignoreDeclaration,
      ignoreDocType: opt.ignoreDocType,
      ignoreInstractions: opt.ignoreInstractions,
      ignoreText: opt.ignoreText,
      ignoreComments: opt.ignoreComments,
      ignoreCData: opt.ignoreCData,
      fullTagEmptyElement: opt.fullTagEmptyElement,
      addParent: opt.addParent,
      trim: opt.trim,
      spaces: opt.spaces,
    };
  }

}

// === module exports block ===

module.exports.XOBJ_DEF_PARAM_TNAME = XOBJ_DEF_PARAM_TNAME;
module.exports.XOBJ_DEF_ATTR_TNAME = XOBJ_DEF_ATTR_TNAME;
module.exports.DEF_XML_PARSE_OPTIONS = DEF_XML_PARSE_OPTIONS;

module.exports.TXmlContentParseOptions = TXmlContentParseOptions;
