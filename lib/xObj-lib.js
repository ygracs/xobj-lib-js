// [v0.2.091-20240915]

// === module init block ===

const {
  valueToIndex, valueToIDString,
  readAsString, readAsBoolEx, readAsNumberEx,
  isNullOrUndef,
  isInteger,
  isArray, isObject, isPlainObject,
  readAsListS,
} = require('@ygracs/bsfoc-lib-js');

// === module extra block (helper functions) ===

/**
 * @typedef RVAL_reason
 * @type {object}
 * @property {string} code - message ID
 * @property {string} msg - message text
 * @description A result of a value check ops.
 */

/**
 * @typedef VCOR_evalkname
 * @type {object}
 * @property {boolean} isSucceed - ops flag
 * @property {(string|RVAL_reason)} value - result value or reson if failed
 * @description A result of a value check ops.
 */

/**
 * @function evalKeyName
 * @param {any} value
 * @param {boolean} [opt=true]
 * @returns {VCOR_evalkname}
 * @inner
 * @description Tries to convert a value into an ID.
 */
function evalKeyName(value, opt = true) {
  if (typeof value !== 'string') {
    return {
      isSucceed: false,
      value: {
        code: XOBJ_TE_NSTR_ECODE,
        msg: XOBJ_TE_NSTR_EMSG,
      },
    };
  };
  const key = value.trim();
  if (opt && key === '') {
    return {
      isSucceed: false,
      value: {
        code: XOBJ_TE_KNES_ECODE,
        msg: XOBJ_TE_KNES_EMSG,
      },
    };
  };
  return {
    isSucceed: true,
    value: key,
  };
};

// === module main block ===

/***
 * (* constant definitions *)
 */

const XOBJ_DEF_PARAM_TNAME = '__text';
const XOBJ_DEF_ATTR_TNAME = '__attr';

const XOBJ_TE_INVARG_EMSG = 'invalid argument';
const XOBJ_TE_NOBJ_EMSG = `${XOBJ_TE_INVARG_EMSG} (an object expected)`;
const XOBJ_TE_NOBJ_ECODE = 'ERR_XOBJ_NOBJ';
const XOBJ_TE_NARR_EMSG = `${XOBJ_TE_INVARG_EMSG} (an array expected)`;
const XOBJ_TE_NARR_ECODE = 'ERR_XOBJ_NARR';
const XOBJ_TE_NSTR_EMSG = `${XOBJ_TE_INVARG_EMSG} (a string expected)`;
const XOBJ_TE_NSTR_ECODE = 'ERR_XOBJ_NSTR';
const XOBJ_TE_NPOBJ_EMSG = `${XOBJ_TE_INVARG_EMSG} (a plain object expected)`;
const XOBJ_TE_NPOBJ_ECODE = 'ERR_XOBJ_NPOBJ';
const XOBJ_TE_ANES_EMSG = '<attr_name> must be a non-empty string';
const XOBJ_TE_ANES_ECODE = 'ERR_XOBJ_INVARG_ATTR';
const XOBJ_TE_KNES_EMSG = '<key_name> must be a non-empty string';
const XOBJ_TE_KNES_ECODE = 'ERR_XOBJ_INVARG_KEY';

/***
 * (* function definitions *)
 */

/**
 * @function evalXObjEName
 * @param {any}
 * @returns {(null|number|string)}
 * @description Tries to convert a value into an ID.
 */
function evalXObjEName(value) {
  //return valueToIDString(value); // // TODO: [?]
  let name = value;
  switch (typeof name) {
    case 'number' : {
      if (valueToIndex(name) === -1) name = null;
      break;
    }
    case 'string' : {
      name = name.trim();
      if (name !== '') {
        let value = Number(name);
        if (!Number.isNaN(value)) {
          name = (
            value < 0 || !isInteger(value)
            ? null
            : value
          );
        };
      } else {
        name = null;
      };
      break;
    }
    default: {
      name = null;
    }
  };
  return name;
};

/**
 * @function genXObjENameDescr
 * @param {any}
 * @returns {object}
 * @description Tries to convert a value into an element name description.
 */
function genXObjENameDescr(value) {
  let result = null;
  let name = null;
  let conditions = null;
  let isERR = false;
  let tail = null;
  if (typeof value === 'string') {
    const re = /^\s*([^\[\]\s]+)?(\s*)(\[.*])?\s*$/;
    tail = value.match(re);
    //console.log('CHECK: '+JSON.stringify(tail, null, 2));
    if (tail) {
      if (tail[1]) {
        name = tail[1];
        if (name) {
          if (tail[2] === '') {
            if (tail[3]) {
              const re = /\[(@{0,1})(?:([^\[=]*?)(?:(?=[=])(=)((?![\"\'])[^\]]*|(?=([\"\']))\5(.*)\5))?)](\s*.*)/;
              tail = tail[3].match(re);
              //console.log('CHECK: '+JSON.stringify(tail, null, 2));
              if (tail) {
                let name = evalXObjEName(tail[2]);
                let type = tail[1] === '@' ? 'attribute' : 'child';
                let value = undefined;
                if (tail[3] === '=') value = tail[5] ? tail[6] : tail[4];
                if (tail[7]) isERR = true;
                conditions = {
                  name,
                  type,
                  value,
                };
              };
            };
          } else if (tail[3]) {
            name = null;
          };
        };
      };
    };
  } else {
    name = value;
  };
  name = evalXObjEName(name);
  if (name === null) isERR = true;
  result = {
    name,
    conditions,
    isERR,
  };
  return result;
};

/**
 * @function getXObjElement
 * @param {object} obj
 * @param {string} name
 * @returns {?any}
 * @throws {TypeError} if first param is not an object
 * @throws {TypeError} if second param is empty string or not a string at all
 * @description Extracts an element from a given object by its key.
 */
function getXObjElement(obj, name) {
  if (!isPlainObject(obj)) {
    const err = new TypeError(XOBJ_TE_NPOBJ_EMSG);
    err.code = XOBJ_TE_NPOBJ_ECODE;
    throw err;
  };
  let { isSucceed, value: key } = evalKeyName(name);
  if (!isSucceed) {
    const { code, msg } = key;
    const err = new TypeError(msg);
    err.code = code;
    throw err;
  };
  // TODO: [?] check type of obj[key_name]
  return obj[key] !== undefined ? obj[key] : null;
};

/**
 * @function getXObjAttributes
 * @param {object} obj
 * @param {string} [key=XOBJ_DEF_ATTR_TNAME]
 * @returns {?object}
 * @throws {TypeError} if first param is not an object
 * @description Extracts an attributes from a given object by its key.
 */
function getXObjAttributes(obj, key = XOBJ_DEF_ATTR_TNAME) {
  let result = null;
  try {
    result = getXObjElement(obj, key);
  } catch (err) {
    switch (err.code) {
      case XOBJ_TE_NSTR_ECODE :
      case XOBJ_TE_KNES_ECODE : {
        break;
      }
      default: {
        throw err;
      }
    };
  };
  return isPlainObject(result) ? result : null;
};

/**
 * @typedef RVAL_emodif
 * @type {object}
 * @property {boolean} isSucceed - ops flag
 * @property {?object} item
 * @description A result of an xObj modification ops
 */

/**
 * @function addXObjElement
 * @param {object} obj
 * @param {string} name
 * @returns {RVAL_emodif}
 * @throws {TypeError} if first param is not an object
 * @throws {TypeError} if second param is empty string or not a string at all
 * @description Adds an element addressed by its key to a given object.
 */
function addXObjElement(obj, name) {
  if (!isPlainObject(obj)) {
    const err = new TypeError(XOBJ_TE_NPOBJ_EMSG);
    err.code = XOBJ_TE_NPOBJ_ECODE;
    throw err;
  };
  let { isSucceed, value: key } = evalKeyName(name);
  if (!isSucceed) {
    const { code, msg } = key;
    const err = new TypeError(msg);
    err.code = code;
    throw err;
  };
  const item = {};
  let prop = obj[key];
  isSucceed = false;
  // // TODO: [?] consider wheter or not do ops if new_key exists
  if (isNullOrUndef(prop)) {
    obj[key] = prop = item;
    isSucceed = true;
  } else if (isObject(prop)) {
    if (isArray(prop)) {
      prop.push(item);
    } else {
      obj[key] = [ prop, item ];
    };
    prop = item;
    isSucceed = true;
  };
  return {
    isSucceed,
    item: isSucceed ? prop : null,
  };
};

/**
 * typedef OPT_inselops_L
 * type {object}
 * @property {boolean} [force=false]
 * @property {boolean} [ripOldies=false]
 * @property {boolean} [acceptIfList=false]
 */

/**
 * @function insertXObjElement
 * @param {object} obj
 * @param {string} name
 * @param {OPT_inselops_L} [opt]
 * @returns {?object}
 * @throws {TypeError} if first param is not an object
 * @description Inserts an element addressed by its key into a given object.
 */
function insertXObjElement(...args) {
  let item = null;
  try {
    ({ item } = insertXObjElementEx(...args));
  } catch (err) {
    switch (err.code) {
      case XOBJ_TE_NSTR_ECODE :
      case XOBJ_TE_KNES_ECODE : {
        break;
      }
      default: {
        throw err;
      }
    };
  };
  return item;
};

/**
 * @function insertXObjElementEx
 * @param {object} obj
 * @param {string} name
 * @param {OPT_inselops_L} [opt]
 * @returns {RVAL_emodif}
 * @throws {TypeError} if first param is not an object
 * @throws {TypeError} if second param is empty string or not a string at all
 * @since 0.2.0
 * @description Inserts an element addressed by its key into a given object.
 */
function insertXObjElementEx(obj, name, opt) {
  if (!isPlainObject(obj)) {
    const err = new TypeError(XOBJ_TE_NPOBJ_EMSG);
    err.code = XOBJ_TE_NPOBJ_ECODE;
    throw err;
  };
  let { isSucceed, value: key } = evalKeyName(name);
  if (!isSucceed) {
    const { code, msg } = key;
    const err = new TypeError(msg);
    err.code = code;
    throw err;
  };
  const item = {};
  let prop = obj[key];
  isSucceed = false;
  if (isNullOrUndef(prop)) {
    obj[key] = prop = item;
    isSucceed = true;
  } else {
    const _opt = isPlainObject(opt) ? opt : {};
    let { force, ripOldies, acceptIfList } = _opt;
    if (typeof force !== 'boolean') force = false;
    if (typeof ripOldies !== 'boolean') ripOldies = false;
    if (typeof acceptIfList !== 'boolean') acceptIfList = false;
    if (force && (ripOldies || !isObject(prop))) {
      obj[key] = prop = item;
      isSucceed = true;
    } else {
      isSucceed = isPlainObject(prop) || (isArray(prop) && acceptIfList);
    };
  };
  return {
    isSucceed,
    item: isSucceed ? prop : null,
  };
};

/**
 * @function deleteXObjElement
 * @param {object} obj
 * @param {string} name
 * @returns {boolean}
 * @throws {TypeError} if first param is not an object
 * @description Deletes an element addressed by its key from a given object.
 */
function deleteXObjElement(...args) {
  let isSucceed = false;
  try {
    ({ isSucceed } = deleteXObjElementEx(...args));
  } catch (err) {
    switch (err.code) {
      case XOBJ_TE_NSTR_ECODE :
      case XOBJ_TE_KNES_ECODE : {
        break;
      }
      default: {
        throw err;
      }
    };
  };
  return isSucceed;
};

/**
 * @function deleteXObjElementEx
 * @param {object} obj
 * @param {string} name
 * @returns {RVAL_emodif}
 * @throws {TypeError} if first param is not an object
 * @throws {TypeError} if second param is empty string or not a string at all
 * @description Deletes an element addressed by its key from a given object.
 */
function deleteXObjElementEx(obj, name) {
  if (!isPlainObject(obj)) {
    const err = new TypeError(XOBJ_TE_NPOBJ_EMSG);
    err.code = XOBJ_TE_NPOBJ_ECODE;
    throw err;
  };
  let { isSucceed, value: key } = evalKeyName(name);
  if (!isSucceed) {
    const { code, msg } = key;
    const err = new TypeError(msg);
    err.code = code;
    throw err;
  };
  let prop = obj[key];
  isSucceed = false;
  // // TODO: catch errors in strict mode
  isSucceed = delete obj[key];
  return {
    isSucceed,
    item: isSucceed && isObject(prop) ? prop : null,
  };
};

/**
 * @function renameXObjElement
 * @param {object} obj
 * @param {string} name - old key
 * @param {string} newName - new key
 * @returns {boolean}
 * @throws {TypeError} if first param is not an object
 * @throws {TypeError} if second param is not a string
 * @throws {TypeError} if third param is not a string
 * @description Renames an element addressed by its key.
 */
function renameXObjElement(obj, name = '', newName = '') {
  if (!isPlainObject(obj)) {
    const err = new TypeError(XOBJ_TE_NPOBJ_EMSG);
    err.code = XOBJ_TE_NPOBJ_ECODE;
    throw err;
  };
  const opt = false;
  let { isSucceed: isAcceptON, value: oname } = evalKeyName(name, opt);
  if (!isAcceptON) {
    const { code, msg } = oname;
    const err = new TypeError(msg);
    err.code = code;
    throw err;
  };
  let { isSucceed: isAcceptNN, value: nname } = evalKeyName(newName, opt);
  if (!isAcceptNN) {
    const { code, msg } = nname;
    const err = new TypeError(msg);
    err.code = code;
    throw err;
  };
  let isSucceed = false;
  if (oname !== '' && oname in obj && nname !== '') {
    isSucceed = true;
    if (oname !== nname) {
      const prop = obj[oname];
      obj[newName] = prop;
      isSucceed = delete obj[oname];
      if (!isSucceed) {
        // undo change if failed delete old element
        delete obj[nname];
      };
    };
  };
  return isSucceed;
};

/**
 * @function checkXObjAttribute
 * @param {object} obj
 * @param {string} attr
 * @param {string} [key]
 * @returns {boolean}
 * @throws {TypeError} if first param is not an object
 * @description Checks wheter an attribute is exists.
 */
function checkXObjAttribute(obj, attr = '', key) {
  let _obj = null;
  try {
    _obj = getXObjAttributes(obj, key);
  } catch (err) {
    throw err;
  };
  if (_obj === null) return false;
  let { isSucceed, value: name } = evalKeyName(attr, false);
  if (isSucceed) {
    if (name === '') return false;
  } else {
    const { code, msg } = name;
    const err = new TypeError(msg);
    err.code = code;
    throw err;
  };
  return _obj[name] !== undefined;
};

/**
 * @function deleteXObjAttribute
 * @param {object} obj
 * @param {string} attr
 * @param {string} [key]
 * @returns {boolean}
 * @throws {TypeError} if first param is not an object
 * @description Deletes an attribute addressed by a given name.
 */
function deleteXObjAttribute(obj, attr = '', key) {
  let _obj = null;
  try {
    _obj = getXObjAttributes(obj, key);
  } catch (err) {
    throw err;
  };
  if (_obj === null) return false;
  let { isSucceed, value: name } = evalKeyName(attr, false);
  if (!isSucceed) {
    const { code, msg } = name;
    const err = new TypeError(msg);
    err.code = code;
    throw err;
  };
  isSucceed = false;
  if (name !== '') {
    // // TODO: catch errors in strict mode
    isSucceed = delete _obj[name];
  };
  return isSucceed;
};

/**
 * @function renameXObjAttribute
 * @param {object} obj
 * @param {string} attr
 * @param {string} newName
 * @param {string} [key]
 * @returns {boolean}
 * @throws {TypeError} if first param is not an object
 * @throws {TypeError} if second param is not an object
 * @throws {TypeError} if third param is not an object
 * @since 0.2.0
 * @description Renames an attribute addressed by a given name.
 */
function renameXObjAttribute(obj, attr = '', newName = '', key = XOBJ_DEF_ATTR_TNAME) {
  if (!isPlainObject(obj)) {
    const err = new TypeError(XOBJ_TE_NPOBJ_EMSG);
    err.code = XOBJ_TE_NPOBJ_ECODE;
    throw err;
  };
  const opt = false;
  let { isSucceed, value: _key } = evalKeyName(key, opt);
  if (!isSucceed) {
    const { code, msg } = _key;
    const err = new TypeError(msg);
    err.code = code;
    throw err;
  };
  isSucceed = false;
  if (_key !== '') {
    let _obj = obj[_key];
    if (isPlainObject(_obj)) {
      let { isSucceed: isAcceptON, value: oname } = evalKeyName(attr, opt);
      if (!isAcceptON) {
        const { code, msg } = oname;
        const err = new TypeError(msg);
        err.code = code;
        throw err;
      };
      let { isSucceed: isAcceptNN, value: nname } = evalKeyName(newName, opt);
      if (!isAcceptNN) {
        const { code, msg } = nname;
        const err = new TypeError(msg);
        err.code = code;
        throw err;
      };
      if (oname !== '' && oname in _obj && nname !== '') {
        if (oname !== nname) {
          _obj = Object.entries(_obj);
          const index = _obj.findIndex((item) => item[0] === oname);
          _obj[index][0] = nname;
          obj[_key] = Object.fromEntries(_obj);
        };
        isSucceed = true;
      };
    };
  };
  return isSucceed;
};

/**
 * @function readXObjParamRaw
 * @param {object} obj
 * @param {string} [key=XOBJ_DEF_PARAM_TNAME]
 * @returns {any}
 * @throws {TypeError} if first param is not an object
 * @throws {TypeError} if third param is not a string
 * @description Extracts a parameter 'AS IS' from a given object.
 */
function readXObjParamRaw(obj, key = XOBJ_DEF_PARAM_TNAME) {
  if (!isPlainObject(obj)) {
    const err = new TypeError(XOBJ_TE_NPOBJ_EMSG);
    err.code = XOBJ_TE_NPOBJ_ECODE;
    throw err;
  };
  let { isSucceed, value: _key } = evalKeyName(key, false);
  if (!isSucceed) {
    const { code, msg } = _key;
    const err = new TypeError(msg);
    err.code = code;
    throw err;
  };
  return _key !== '' ? obj[_key] : undefined;
};

/**
 * @function writeXObjParamRaw
 * @param {object} obj
 * @param {any} value
 * @param {string} [key=XOBJ_DEF_PARAM_TNAME]
 * @returns {boolean}
 * @throws {TypeError} if first param is not an object
 * @throws {TypeError} if third param is not a string
 * @description Writes a parameter 'AS IS' into a given object.
 */
function writeXObjParamRaw(obj, value, key = XOBJ_DEF_PARAM_TNAME) {
  if (!isPlainObject(obj)) {
    const err = new TypeError(XOBJ_TE_NPOBJ_EMSG);
    err.code = XOBJ_TE_NPOBJ_ECODE;
    throw err;
  };
  let { isSucceed, value: _key } = evalKeyName(key, false);
  if (!isSucceed) {
    const { code, msg } = _key;
    const err = new TypeError(msg);
    err.code = code;
    throw err;
  };
  if (_key === '' || value === undefined) {
    isSucceed = false;
  } else {
    obj[_key] = value;
  };
  return isSucceed;
};

/**
 * @function readXObjAttrRaw
 * @param {object} obj
 * @param {string} attr
 * @param {string} [key]
 * @returns {any}
 * @throws {TypeError} if first param is not an object
 * @throws {TypeError} if third param is not a string
 * @description Extracts an attribute 'AS IS' from a given object.
 */
function readXObjAttrRaw(obj, attr = '', key) {
  let _obj = null;
  try {
    _obj = getXObjAttributes(obj, key);
  } catch (err) {
    throw err;
  };
  if (_obj !== null) {
    let { isSucceed, value: name } = evalKeyName(attr, false);
    if (!isSucceed) {
      const { code, msg } = name;
      const err = new TypeError(msg);
      err.code = code;
      throw err;
    };
    if (name !== '') return _obj[name];
  };
};

/**
 * @function writeXObjAttrRaw
 * @param {object} obj
 * @param {string} attr
 * @param {any} value
 * @param {string} [key]
 * @returns {boolean}
 * @throws {TypeError} if first param is not an object
 * @throws {TypeError} if third param is not a string
 * @description Writes a parameter into a given object.
 */
function writeXObjAttrRaw(obj, attr = '', value, key = XOBJ_DEF_ATTR_TNAME) {
  let { isSucceed, value: name } = evalKeyName(attr, false);
  if (!isSucceed) {
    const { code, msg } = name;
    const err = new TypeError(msg);
    err.code = code;
    throw err;
  };
  isSucceed = false;
  if (name !== '' && value !== undefined) {
    let _obj = null;
    try {
      const opt = { force: true, acceptIfList: true };
      _obj = insertXObjElement(obj, key, opt);
    } catch (err) {
      throw err;
    };
    if (isArray(_obj)) {
      // force a replacement of the old element if it's array
      try {
        const opt = { force: true, ripOldies: true };
        _obj = insertXObjElement(obj, key, opt);
      } catch (err) {
        throw err;
      };
    };
    if (_obj !== null) {
      _obj[name] = value;
      isSucceed = true;
    };
  };
  return isSucceed;
};

/**
 * @function readXObjParam
 * @param {object} obj
 * @param {string} [key]
 * @returns {string}
 * @throws {TypeError} if first param is not an object
 * @description Extracts a parameter from a given object
 *              and returns it as string.
 */
function readXObjParam(obj, key) {
  const opt = {
    useTrim: false,
    numberToString: true,
    boolToString: true,
    defValue: '',
  };
  let result = undefined;
  try {
    result = readXObjParamEx(obj, opt, key);
  } catch (err) {
    throw err;
  };
  return result;
};

/**
 * @function readXObjParamAsBool
 * @param {object} obj
 * @param {boolean} [defValue]
 * @param {string} [key]
 * @returns {boolean}
 * @throws {TypeError} if first param is not an object
 * @description Extracts a parameter from a given object
 *              and returns it as boolean.
 */
function readXObjParamAsBool(obj, defValue, key) {
  let result = undefined;
  try {
    result = readXObjParamRaw(obj, key);
  } catch (err) {
    switch (err.code) {
      case XOBJ_TE_NSTR_ECODE : {
        break;
      }
      default: {
        throw err;
      }
    };
  };
  return readAsBoolEx(result, defValue);
};

/**
 * @function readXObjParamAsNum
 * @param {object} obj
 * @param {number} [defValue]
 * @param {string} [key]
 * @returns {number}
 * @throws {TypeError} if first param is not an object
 * @description Extracts a parameter from a given object
 *              and returns it as number.
 */
function readXObjParamAsNum(obj, defValue, key) {
  let result = undefined;
  try {
    result = readXObjParamRaw(obj, key);
  } catch (err) {
    switch (err.code) {
      case XOBJ_TE_NSTR_ECODE : {
        break;
      }
      default: {
        throw err;
      }
    };
  };
  return readAsNumberEx(result, defValue);
};

/**
 * @function readXObjParamEx
 * @param {object} obj
 * @param {any} [opt]
 * @param {string} [key]
 * @returns {string}
 * @throws {TypeError} if first param is not an object
 * @description Extracts a parameter from a given object
 *              and returns it as string.
 */
function readXObjParamEx(obj, opt, key) {
  let result = undefined;
  try {
    result = readXObjParamRaw(obj, key);
  } catch (err) {
    switch (err.code) {
      case XOBJ_TE_NSTR_ECODE : {
        break;
      }
      default: {
        throw err;
      }
    };
  };
  let _opt = opt;
  if (!isPlainObject(_opt)) {
    _opt = {
      useTrim: false,
      numberToString: true,
      boolToString: true,
      defValue: _opt,
    };
  };
  return readAsString(result, _opt);
};

/**
 * @function readXObjParamAsIndex
 * @param {object} obj
 * @param {string} [key]
 * @returns {number}
 * @throws {TypeError} if first param is not an object
 * @description Extracts a parameter from a given object
 *              and returns it as 'index' value.
 */
function readXObjParamAsIndex(obj, key) {
  let result = undefined;
  try {
    result = readXObjParamRaw(obj, key);
  } catch (err) {
    switch (err.code) {
      case XOBJ_TE_NSTR_ECODE : {
        break;
      }
      default: {
        throw err;
      }
    };
  };
  return valueToIndex(result);
};

/**
 * @function writeXObjParam
 * @param {object} obj
 * @param {any} value
 * @param {string} [key]
 * @returns {boolean}
 * @throws {TypeError} if first param is not an object
 * @description Tries to convert a given value to a string
 *              and writes it as a parameter into a given object.
 */
function writeXObjParam(obj, value, key) {
  let isSucceed = false;
  try {
    const opt = {
      useTrim: false,
      numberToString: true,
      boolToString: true,
      defValue: '',
    };
    isSucceed = writeXObjParamEx(obj, value, opt, key);
  } catch (err) {
    throw err;
  };
  return isSucceed;
};

/**
 * @function writeXObjParamAsBool
 * @param {object} obj
 * @param {any} value
 * @param {boolean} [defValue]
 * @param {string} [key]
 * @returns {boolean}
 * @throws {TypeError} if first param is not an object
 * @description Tries to convert a given value to a boolean
 *              and writes it as a parameter into a given object.
 */
function writeXObjParamAsBool(obj, value, defValue, key) {
  let isSucceed = false;
  if (value !== undefined || typeof defValue === 'boolean') {
    const _value = readAsBoolEx(value, defValue).toString();
    try {
      isSucceed = writeXObjParamRaw(obj, _value, key);
    } catch (err) {
      switch (err.code) {
        case XOBJ_TE_NSTR_ECODE : {
          break;
        }
        default: {
          throw err;
        }
      };
    };
  };
  return isSucceed;
};

/**
 * @function writeXObjParamAsNum
 * @param {object} obj
 * @param {any} value
 * @param {number} [defValue]
 * @param {string} [key]
 * @returns {boolean}
 * @throws {TypeError} if first param is not an object
 * @description Tries to convert a given value to a number
 *              and writes it as a parameter into a given object.
 */
function writeXObjParamAsNum(obj, value, defValue, key) {
  let isSucceed = false;
  if (value !== undefined || typeof defValue === 'number') {
    const _value = readAsNumberEx(value, defValue).toString();
    try {
      isSucceed = writeXObjParamRaw(obj, _value, key);
    } catch (err) {
      switch (err.code) {
        case XOBJ_TE_NSTR_ECODE : {
          break;
        }
        default: {
          throw err;
        }
      };
    };
  };
  return isSucceed;
};

/**
 * @function writeXObjParamAsIndex
 * @param {object} obj
 * @param {any} value
 * @param {string} [key]
 * @returns {boolean}
 * @throws {TypeError} if first param is not an object
 * @description Tries to convert a given value into an 'index' value
 *              and writes it as a parameter into a given object.
 */
function writeXObjParamAsIndex(obj, value, key) {
  let isSucceed = false;
  if (value !== undefined) {
    const _value = valueToIndex(value).toString();
    try {
      isSucceed = writeXObjParamRaw(obj, _value, key);
    } catch (err) {
      switch (err.code) {
        case XOBJ_TE_NSTR_ECODE : {
          break;
        }
        default: {
          throw err;
        }
      };
    };
  };
  return isSucceed;
};

/**
 * @function writeXObjParamEx
 * @param {object} obj
 * @param {any} value
 * @param {any} [opt]
 * @param {string} [key]
 * @returns {boolean}
 * @throws {TypeError} if first param is not an object
 * @description Tries to convert a given value to a string
 *              and writes it as a parameter into a given object.
 */
function writeXObjParamEx(obj, value, opt, key) {
  let isSucceed = false;
  if (value !== undefined) {
    let _opt = opt;
    if (!isPlainObject(_opt)) {
      const defValue = readAsString(_opt, {
        useTrim: false,
        numberToString: true,
        boolToString: true,
      });
      _opt = {
        useTrim: false,
        numberToString: true,
        boolToString: true,
        defValue,
      };
    };
    const _value = readAsString(value, _opt);
    try {
      isSucceed = writeXObjParamRaw(obj, _value, key);
    } catch (err) {
      switch (err.code) {
        case XOBJ_TE_NSTR_ECODE : {
          break;
        }
        default: {
          throw err;
        }
      };
    };
  };
  return isSucceed;
};

/**
 * @function readXObjAttr
 * @param {object} obj
 * @param {string} attr
 * @param {string} [key]
 * @returns {string}
 * @throws {TypeError} if first param is not an object
 * @description Extracts an attribute from a given object
 *              and returns it as string.
 */
function readXObjAttr(obj, attr, key) {
  const opt = {
    useTrim: true,
    numberToString: true,
    boolToString: true,
    defValue: '',
  };
  let result = undefined;
  try {
    result = readXObjAttrEx(obj, attr, opt, key);
  } catch (err) {
    throw err;
  };
  return result;
};

/**
 * @function readXObjAttrAsBool
 * @param {object} obj
 * @param {string} attr
 * @param {boolean} [defValue]
 * @param {string} [key]
 * @returns {boolean}
 * @throws {TypeError} if first param is not an object
 * @description Extracts an attribute from a given object
 *              and returns it as boolean.
 */
function readXObjAttrAsBool(obj, attr, defValue, key) {
  let result = undefined;
  try {
    result = readXObjAttrRaw(obj, attr, key);
  } catch (err) {
    switch (err.code) {
      case XOBJ_TE_NSTR_ECODE : {
        break;
      }
      default: {
        throw err;
      }
    };
  };
  return readAsBoolEx(result, defValue);
};

/**
 * @function readXObjAttrAsNum
 * @param {object} obj
 * @param {string} attr
 * @param {number} [defValue]
 * @param {string} [key]
 * @returns {number}
 * @throws {TypeError} if first param is not an object
 * @description Extracts an attribute from a given object
 *              and returns it as number.
 */
function readXObjAttrAsNum(obj, attr, defValue, key) {
  let result = undefined;
  try {
    result = readXObjAttrRaw(obj, attr, key);
  } catch (err) {
    switch (err.code) {
      case XOBJ_TE_NSTR_ECODE : {
        break;
      }
      default: {
        throw err;
      }
    };
  };
  return readAsNumberEx(result, defValue);
};

/**
 * @function readXObjAttrEx
 * @param {object} obj
 * @param {string} attr
 * @param {any} [opt]
 * @param {string} [key]
 * @returns {string}
 * @throws {TypeError} if first param is not an object
 * @description Extracts an attribute from a given object
 *              and returns it as string.
 */
function readXObjAttrEx(obj, attr, opt, key) {
  let result = undefined;
  try {
    result = readXObjAttrRaw(obj, attr, key);
  } catch (err) {
    switch (err.code) {
      case XOBJ_TE_NSTR_ECODE : {
        break;
      }
      default: {
        throw err;
      }
    };
  };
  let _opt = opt;
  if (!isPlainObject(_opt)) {
    _opt = {
      useTrim: true,
      numberToString: true,
      boolToString: true,
      defValue: _opt,
    };
  };
  return readAsString(result, _opt);
};

/**
 * @function readXObjAttrAsIndex
 * @param {object} obj
 * @param {string} attr
 * @param {string} [key]
 * @returns {number}
 * @throws {TypeError} if first param is not an object
 * @description Extracts an attribute from a given object
 *              and returns it as 'index' value.
 */
function readXObjAttrAsIndex(obj, attr, key) {
  let result = undefined;
  try {
    result = readXObjAttrRaw(obj, attr, key);
  } catch (err) {
    switch (err.code) {
      case XOBJ_TE_NSTR_ECODE : {
        break;
      }
      default: {
        throw err;
      }
    };
  };
  return valueToIndex(result);
};

/**
 * @function writeXObjAttr
 * @param {object} obj
 * @param {string} attr
 * @param {any} value
 * @param {string} [key]
 * @returns {boolean}
 * @throws {TypeError} if first param is not an object
 * @description Tries to convert a given value to a string
 *              and writes it as an attribute into a given object.
 */
function writeXObjAttr(obj, attr, value, key) {
  let isSucceed = false;
  try {
    const opt = {
      useTrim: true,
      numberToString: true,
      boolToString: true,
      defValue: '',
    };
    isSucceed = writeXObjAttrEx(obj, attr, value, opt, key);
  } catch (err) {
    throw err;
  };
  return isSucceed;
};

/**
 * @function writeXObjAttrAsBool
 * @param {object} obj
 * @param {string} attr
 * @param {any} value
 * @param {boolean} [defValue]
 * @param {string} [key]
 * @returns {boolean}
 * @throws {TypeError} if first param is not an object
 * @description Tries to convert a given value to a boolean
 *              and writes it as an attribute into a given object.
 */
function writeXObjAttrAsBool(obj, attr, value, defValue, key) {
  let isSucceed = false;
  if (value !== undefined || typeof defValue === 'boolean') {
    const _value = readAsBoolEx(value, defValue).toString();
    try {
      isSucceed = writeXObjAttrRaw(obj, attr, _value, key);
    } catch (err) {
      switch (err.code) {
        case XOBJ_TE_NSTR_ECODE : {
          break;
        }
        default: {
          throw err;
        }
      };
    };
  };
  return isSucceed;
};

/**
 * @function writeXObjAttrAsNum
 * @param {object} obj
 * @param {string} attr
 * @param {any} value
 * @param {number} [defValue]
 * @param {string} [key]
 * @returns {boolean}
 * @throws {TypeError} if first param is not an object
 * @description Tries to convert a given value to a number
 *              and writes it as an attribute into a given object.
 */
function writeXObjAttrAsNum(obj, attr, value, defValue, key) {
  let isSucceed = false;
  if (value !== undefined || typeof defValue === 'number') {
    const _value = readAsNumberEx(value, defValue).toString();
    try {
      isSucceed = writeXObjAttrRaw(obj, attr, _value, key);
    } catch (err) {
      switch (err.code) {
        case XOBJ_TE_NSTR_ECODE : {
          break;
        }
        default: {
          throw err;
        }
      };
    };
  };
  return isSucceed;
};

/**
 * @function writeXObjAttrAsIndex
 * @param {object} obj
 * @param {string} attr
 * @param {any} value
 * @param {string} [key]
 * @returns {boolean}
 * @throws {TypeError} if first param is not an object
 * @description Tries to convert a given value into an 'index' value
 *              and writes it as an attribute into a given object.
 */
function writeXObjAttrAsIndex(obj, attr, value, key) {
  let isSucceed = false;
  if (value !== undefined) {
    const _value = valueToIndex(value).toString();
    try {
      isSucceed = writeXObjAttrRaw(obj, attr, _value, key);
    } catch (err) {
      switch (err.code) {
        case XOBJ_TE_NSTR_ECODE : {
          break;
        }
        default: {
          throw err;
        }
      };
    };
  };
  return isSucceed;
};

/**
 * @function writeXObjAttrEx
 * @param {object} obj
 * @param {string} attr
 * @param {any} value
 * @param {any} [opt]
 * @param {string} [key]
 * @returns {boolean}
 * @throws {TypeError} if first param is not an object
 * @description Tries to convert a given value to a string
 *              and writes it as an attribute into a given object.
 */
function writeXObjAttrEx(obj, attr, value, opt, key) {
  let isSucceed = false;
  if (value !== undefined) {
    let _opt = opt;
    if (!isPlainObject(_opt)) {
      const defValue = readAsString(_opt, {
        useTrim: true,
        numberToString: true,
        boolToString: true,
      });
      _opt = {
        useTrim: true,
        numberToString: true,
        boolToString: true,
        defValue,
      };
    };
    const _value = readAsString(value, _opt);
    try {
      isSucceed = writeXObjAttrRaw(obj, attr, _value, key);
    } catch (err) {
      switch (err.code) {
        case XOBJ_TE_NSTR_ECODE : {
          break;
        }
        default: {
          throw err;
        }
      };
    };
  };
  return isSucceed;
};

/**
 * @function insertXObjElements
 * @param {object} obj
 * @param {...string} name
 * @param {OPT_inselops_L} [opt]
 * @returns {number}
 * @throws {TypeError} if first param is not an object
 * @description Inserts an elements into a given object.
 */
function insertXObjElements(obj, ...args) {
  if (!isPlainObject(obj)) {
    const err = new TypeError(XOBJ_TE_NPOBJ_EMSG);
    err.code = XOBJ_TE_NPOBJ_ECODE;
    throw err;
  };
  let count = 0;
  let len = args.length;
  const opt = (
    len > 0 && isPlainObject(args[len - 1])
    ? (len--, args.pop())
    : {}
  );
  for (let key of args) {
    if (insertXObjElement(obj, key, opt) !== null) count++;
  };
  return count;
};

/**
 * typedef OPT_inselops_S
 * type {object}
 * @property {boolean} [force=false]
 * @property {boolean} [ripOldies=false]
 */

/**
 * @function insertXObjEList
 * @param {object} obj
 * @param {string} name
 * @param {OPT_inselops_S} [opt]
 * @returns {?any}
 * @throws {TypeError} if first param is not an object
 * @description Inserts a list elements into a given object.
 */
function insertXObjEList(...args) {
  let item = null;
  try {
    ({ item } = insertXObjEListEx(...args));
  } catch (err) {
    switch (err.code) {
      case XOBJ_TE_NSTR_ECODE :
      case XOBJ_TE_KNES_ECODE : {
        break;
      }
      default: {
        throw err;
      }
    };
  };
  return item;
};

/**
 * @function insertXObjEListEx
 * @param {object} obj
 * @param {string} name
 * @param {OPT_inselops_S} [opt]
 * @returns {RVAL_emodif}
 * @throws {TypeError} if first param is not an object
 * @throws {TypeError} if second param is empty string or not a string at all
 * @since 0.2.0
 * @description Inserts a list elements into a given object.
 */
function insertXObjEListEx(obj, name, opt) {
  if (!isPlainObject(obj)) {
    const err = new TypeError(XOBJ_TE_NPOBJ_EMSG);
    err.code = XOBJ_TE_NPOBJ_ECODE;
    throw err;
  };
  let { isSucceed, value: key } = evalKeyName(name);
  if (!isSucceed) {
    const { code, msg } = key;
    const err = new TypeError(msg);
    err.code = code;
    throw err;
  };
  const _options = isPlainObject(opt) ? opt : {};
  let { force, ripOldies } = _options;
  if (typeof force !== 'boolean') force = false;
  if (typeof ripOldies !== 'boolean') ripOldies = false;
  let prop = obj[key];
  isSucceed = false;
  if (
    isNullOrUndef(prop)
    || (force && (ripOldies || !isObject(prop)))
  ) {
    obj[key] = prop = [];
    isSucceed = true;
  } else {
    if (isObject(prop)) {
      if (!isArray(prop)) prop = [ prop ];
      obj[key] = prop;
      isSucceed = true;
    };
  };
  return {
    isSucceed,
    item: isSucceed ? prop  : null,
  };
};

/**
 * @function insertXObjEChain
 * @param {object} obj
 * @param {...string} name
 * @param {object} [opt]
 * @returns {?any}
 * @throws {TypeError} if first param is not an object
 * @description Inserts a chain of an elements into a given object.
 */
function insertXObjEChain(obj, ...args){
  if (!isPlainObject(obj)) {
    const err = new TypeError(XOBJ_TE_NPOBJ_EMSG);
    err.code = XOBJ_TE_NPOBJ_ECODE;
    throw err;
  };
  let result = null;
  let len = args.length;
  const opt = (
    len > 0 && isPlainObject(args[len - 1])
    ? (len--, args.pop())
    : {}
  );
  if (len > 0) {
    let parent = obj;
    let child = null;
    let isSucceed = false;
    for (let key of args) {
      child = insertXObjElement(parent, key, opt);
      isSucceed = isPlainObject(child);
      if (!isSucceed) break;
      parent = child;
    };
    result = isSucceed ? parent : null;
  };
  return result;
};

/***
 * (* class definitions *)
 */

// === module exports block ===

module.exports.XOBJ_DEF_PARAM_TNAME = XOBJ_DEF_PARAM_TNAME;
module.exports.XOBJ_DEF_ATTR_TNAME = XOBJ_DEF_ATTR_TNAME;

module.exports.readXObjParamRaw = readXObjParamRaw;
module.exports.readXObjParam = readXObjParam;
module.exports.readXObjParamAsBool = readXObjParamAsBool;
module.exports.readXObjParamAsNum = readXObjParamAsNum;
module.exports.readXObjParamEx = readXObjParamEx;
module.exports.readXObjParamAsIndex = readXObjParamAsIndex;
module.exports.writeXObjParamRaw = writeXObjParamRaw;
module.exports.writeXObjParam = writeXObjParam;
module.exports.writeXObjParamAsBool = writeXObjParamAsBool;
module.exports.writeXObjParamAsNum = writeXObjParamAsNum;
module.exports.writeXObjParamAsIndex = writeXObjParamAsIndex;
module.exports.writeXObjParamEx = writeXObjParamEx;

module.exports.readXObjAttrRaw = readXObjAttrRaw;
module.exports.readXObjAttr = readXObjAttr;
module.exports.readXObjAttrAsBool = readXObjAttrAsBool;
module.exports.readXObjAttrAsNum = readXObjAttrAsNum;
module.exports.readXObjAttrEx = readXObjAttrEx;
module.exports.readXObjAttrAsIndex = readXObjAttrAsIndex;
module.exports.writeXObjAttrRaw = writeXObjAttrRaw;
module.exports.writeXObjAttr = writeXObjAttr;
module.exports.writeXObjAttrAsBool = writeXObjAttrAsBool;
module.exports.writeXObjAttrAsNum = writeXObjAttrAsNum;
module.exports.writeXObjAttrAsIndex = writeXObjAttrAsIndex;
module.exports.writeXObjAttrEx = writeXObjAttrEx;

module.exports.getXObjAttributes = getXObjAttributes;
module.exports.checkXObjAttribute = checkXObjAttribute;
module.exports.deleteXObjAttribute = deleteXObjAttribute;
module.exports.renameXObjAttribute = renameXObjAttribute;

module.exports.getXObjElement = getXObjElement;
module.exports.addXObjElement = addXObjElement;
module.exports.insertXObjElement = insertXObjElement;
module.exports.insertXObjElementEx = insertXObjElementEx;
module.exports.deleteXObjElement = deleteXObjElement;
module.exports.deleteXObjElementEx = deleteXObjElementEx;
module.exports.renameXObjElement = renameXObjElement;

module.exports.evalXObjEName = evalXObjEName;
module.exports.insertXObjEList = insertXObjEList;
module.exports.insertXObjEListEx = insertXObjEListEx;

/* inner */
module.exports.evalKeyName = evalKeyName;

/* experimental */
module.exports.genXObjENameDescr = genXObjENameDescr;
/* experimental */
module.exports.insertXObjElements = insertXObjElements;
/* experimental */
module.exports.insertXObjEChain = insertXObjEChain;

/** @deprecated */
module.exports.readXObjParamAsStr = readXObjParamEx;
/** @deprecated */
module.exports.readXObjAttrAsStr = readXObjAttrEx;
