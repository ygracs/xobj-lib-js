#### *v0.2.0*

Release version.

> - `xObj.md` updated;
> - change behavior for function: `deleteXObjElement`, `insertXObjElement`, `renameXObjElement` (*check the package docs*);
> - change behavior for function: `insertXObjEList`, `insertXObjEChain` (*check the package docs*);
> - change behavior for function: `writeXObjParamAsBool`,  `writeXObjParamAsNum`, `writeXObjAttrAsBool`, `writeXObjAttrAsNum` (*check the package docs*);
> - added `insertXObjElementEx` function;
> - added `insertXObjEListEx` function;
> - added `readXObjParamEx` function;
> - added `readXObjAttrEx` function;
> - added `renameXObjAttribute` function;
> - deprecated `readXObjParamAsStr` function;
> - deprecated `readXObjAttrAsStr` function.

#### *v0.1.2*

Release version.

> - updated dependency on `@ygracs/bsfoc-lib-js` module to v0.2.1;
> - move some definitions from `xObj-lib.js` module into `xObj-defs.js`;
> - fix behavior for function: `evalXObjEName`.

#### *v0.1.1*

Release version.

> - updated dependency on `@ygracs/bsfoc-lib-js` module to v0.2.0.

#### *v0.1.0*

Release version.

> - `xObj-lib.md` updated;
> - change behavior for function: `insertXObjElement`, `insertXObjElements`, `insertXObjEChain`, `insertXObjEList` (***remove deprecated options parameters**. check the package docs*)
> - updated dependency on `@ygracs/bsfoc-lib-js` module to v0.1.4.

#### *v0.0.1-v0.0.14rc4*

Pre-release version.
