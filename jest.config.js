// [v0.1.003-20240623]

const cmdArgs = require('minimist')(process.argv.slice(2));

const testObj = new Map([
  //[ 'bsf', 'baseFunc' ],
]);

let rootDirExt = '';
let obj = cmdArgs.object;
let tmSym = cmdArgs.target;
if (
  typeof tmSym !== 'string'
  || ((tmSym = tmSym.trim()) === '')
) {
  tmSym = '*';
};
if (
  typeof obj === 'string'
  && ((obj = obj.trim()) !== '')
) {
  if (testObj.has(obj)) rootDirExt = `/${testObj.get(obj)}/`;
};

module.exports = {
  rootDir: `__test__${rootDirExt}`,
  testMatch: [`**/?(${tmSym}.)+(spec|test).[jt]s?(x)`],
  verbose: false,
};
