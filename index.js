// [v0.1.012-20240624]

// === module init block ===

const {
  TXmlContentParseOptions,
  DEF_XML_PARSE_OPTIONS,
} = require('./lib/xObj-defs');

// === module extra block (helper functions) ===

// === module main block ===

// === module exports block ===

module.exports = require('./lib/xObj-lib');
module.exports.TXmlContentParseOptions = TXmlContentParseOptions;
module.exports.DEF_XML_PARSE_OPTIONS = DEF_XML_PARSE_OPTIONS;
