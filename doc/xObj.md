>|***rev.*:**|0.1.36|
>|:---|---:|
>|date:|2024-09-15|

## Intoduction

This paper describes a functions provided by `xObj.js` module.

> Note: This module was primarily written to deal with a XML-parse provided by a [`xml-js`](https://www.npmjs.com/package/xml-js) module for 'Node.js' running in "compact" mode.

## Content

### Base constants

|name|type|value|
|:---|---|---:|
|XOBJ_DEF_PARAM_TNAME|`string`|`__text`|
|XOBJ_DEF_ATTR_TNAME|`string`|`__attr`|
|DEF_XML_PARSE_OPTIONS|`object`|---|

#### **DEF\_XML\_PARSE_OPTIONS**

This constant object provided by the module contains a default settings of options for XML-parser module used within.

The settings listed in the table below:

|name|type|value|
|:---|---|:---|
|compact|`boolean`|`true`|
|declarationKey|`string`|`__decl`|
|attributesKey|`string`|`__attr`|
|textKey|`string`|`__text`|
|commentKey|`string`|`__note`|
|cdataKey|`string`|`__cdata`|
|nameKey|`string`|`__name`|
|typeKey|`string`|`__type`|
|parentKey|`string`|`parent`|
|elementsKey|`string`|`items`|
|ignoreDeclaration|`boolean`|`false`|
|ignoreDocType|`boolean`|`false`|
|ignoreInstractions|`boolean`|`false`|
|ignoreText|`boolean`|`false`|
|ignoreComments|`boolean`|`false`|
|ignoreCData|`boolean`|`false`|
|fullTagEmptyElement|`boolean`|`true`|
|addParent|`boolean`|`false`|
|trim|`boolean`|`true`|
|spaces|`number`|`2`|

### Base functions for read an object parameter value

> Note:
> If a `key` parameter not given, the function will use a default value defined by `XOBJ_DEF_PARAM_TNAME`.

#### **readXObjParam(object\[, key])** => `string`

This function returns a value of an object parameter. The value is of a `string` type.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance.

#### **readXObjParamAsBool(object\[, defValue \[, key]])** => `boolean`

This function reads a value of an object parameter and returns it as a `boolean`.

The `defValue` must be of a `boolean` type, if else the `false` is used.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance.

#### **readXObjParamAsNum(object\[, defValue \[, key])** => `number`

This function reads a value of an object parameter and returns it as a `number`.

The `defValue` must be of a `number` type, if else the `0` is used.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance.

#### **readXObjParamAsIndex(object\[, key])** => `index`

This function reads a value of an object parameter and try to convert it to a valid index value. If function failed, `-1` is returned.

> Note: *for use in this case, index is validated as a non-negative integer number.*

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance.

#### **readXObjParamAsStr(object\[, defValue \[, key])** => `string`

> WARNING: `[since: v0.2.0]` ***this function deprecated.** Use [`readXObjParam`](#readxobjparamobject-key--string) or [`readXObjParamEx`](#readxobjparamexobject-opt-key--string) instead.*

This function reads a value of an object parameter and returns it as a `string`.

The `defValue` must be of a `string` type, if else the empty string is used.

#### **readXObjParamEx(object\[, opt \[, key])** => `string`

> since: \[v0.2.0]

This function reads a value of an object parameter and returns it as a `string`.

##### ***parameters***

The `opt` parameter must be:
- a `string` - defines a default value;
- an `object` - defines a set of options to transform a value.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance.

### Base functions for write an object parameter value

> Note:
> If a `key` parameter not given, the function will use a default value defined by `XOBJ_DEF_PARAM_TNAME`.

#### **writeXObjParam(object, value\[, key])** => `boolean`

This function sets object parameter to a given value and if succeed returns `true`.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance.

#### **writeXObjParamAsBool(object, value\[, defValue\[, key]])** => `boolean`

This function writes a given `boolean` value of an object parameter.

The `defValue` must be of a `boolean` type, if else the `false` is used.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance.

#### **writeXObjParamAsNum(object, value\[, defValue\[, key]])** => `boolean`

This function writes a given `number` value of an object parameter.

The `defValue` must be of a `number` type, if else the `0` is used.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance.

#### **writeXObjParamAsIndex(object, value\[, key])** => `boolean`

This function try to interpret a given value as a valid index value and, if succeed, writes it into an object parameter.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance.

#### **writeXObjParamEx(object, value\[, opt\[, key]])** => `boolean`

This function sets object parameter to a given value and if succeed returns `true`.

##### ***parameters***

The `opt` parameter must be:
- a `string` - defines a default value;
- an `object` - defines a set of options to transform a value.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance.

### Base functions for read an object attributes value

> Note:
> If a `key` parameter not given, the function will use a default value defined by `XOBJ_DEF_ATTR_TNAME`.

#### **readXObjAttr(object, attr\[, key])** => `string`

This function returns a value of an object attribute. The value is of a `string` type.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance.

#### **readXObjAttrAsBool(object, attr\[, defValue\[, key]])** => `boolean`

This function reads a value of an object attribute and returns it as a `boolean`.

The `defValue` must be of a `boolean` type, if else the `false` is used.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance.

#### **readXObjAttrAsNum(object, attr\[, defValue\[, key]])** => `number`

This function reads a value of an object attribute and returns it as a `number`.

The `defValue` must be of a `number` type, if else the `0` is used.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance.

#### **readXObjAttrAsIndex(object, attr\[,  key])** => `index`

This function reads a value of an object attribute and try to convert it to a valid index value. If function failed, `-1` is returned.

> Note: *for use in this case, index is validated as a non-negative integer number.*

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance.

#### **readXObjAttrAsStr(object, attr\[, defValue\[, key]])** => `string`

> WARNING: `[since: v0.2.0]` ***this function deprecated.** Use [`readXObjAttr`](#readxobjattrobject-attr-key--string) or [`readXObjAttrEx`](#readxobjattrexobject-attr-opt-key--string) instead.*

This function returns a value of an object attribute. The value is of a `string` type.

The `defValue` must be of a `string` type, if else the empty string is used.

#### **readXObjAttrEx(object, attr\[, opt\[, key]])** => `string`

> since: \[v0.2.0]

This function returns a value of an object attribute. The value is of a `string` type.

##### ***parameters***

The `opt` parameter must be:
- a `string` - defines a default value;
- an `object` - defines a set of options to transform a value.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance.

### Base functions for write an object attributes value

> Note:
> If a `key` parameter not given, the function will use a default value defined by `XOBJ_DEF_ATTR_TNAME`.

#### **writeXObjAttr(object, attr, value\[, key])** => `boolean`

This function sets object attribute to a given value and if succeed returns `true`.

>Note: a value must be of a `string` type or can be converted to a string

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance.

#### **writeXObjAttrAsBool(object, attr, value\[, defValue\[, key]])** => `boolean`

This function writes a given `boolean` value of an object attribute.

The `defValue` must be of a `boolean` type, if else the `false` is used.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance.

#### **writeXObjAttrAsNum(object, attr, value\[, defValue\[, key]])** => `boolean`

This function writes a given `number` value of an object attribute.

The `defValue` must be of a `number` type, if else the `0` is used.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance.

#### **writeXObjAttrAsIndex(object, attr, value\[, key])** => `boolean`

This function try to interpret a given value as a valid index value and, if succeed, writes it into an object attribute.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance.

#### **writeXObjAttrEx(object, attr, value\[, opt\[, key]])** => `boolean`

This function sets object attribute to a given value and if succeed returns `true`.

##### ***parameters***

The `opt` parameter must be:
- a `string` - defines a default value;
- an `object` - defines a set of options to transform a value.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance.

### Special functions for read and write an object parameter value

#### **readXObjParamRaw(object\[, key])** => `any`

This is a special function that returns a value of an object parameter as "IT IS".

> NOTE:
> - If a `key` parameter not given, the function will use a default value defined by `XOBJ_DEF_PARAM_TNAME`.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance;
- code: `ERR_XOBJ_NSTR`
  + if `key` parameter given and is not a type of a `string`.

#### **writeXObjParamRaw(object, value\[, key])** => `boolean`

This is a special function that writes a given value of an object parameter as "IT IS".

> NOTE:
> - If `value` parameter not given, the function failed.
> - If a `key` parameter not given, the function will use a default value defined by `XOBJ_DEF_PARAM_TNAME`.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance;
- code: `ERR_XOBJ_NSTR`
  + if `key` parameter given and is not a type of a `string`.

### Special functions for read or write object attributes value

#### **readXObjAttrRaw(object, attr\[, key])** => `any`

This is a special function that returns a value of an object attribute as "IT IS".

> NOTE:
> - If a `key` parameter not given, the function will use a default value defined by `XOBJ_DEF_ATTR_TNAME`.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance;
- code: `ERR_XOBJ_NSTR`
  + if `attr` parameter given and is not a type of a `string`;
  + if `key` parameter given and is not a type of a `string`.

#### **writeXObjAttrRaw(object, attr, value\[, key])** => `boolean`

This is a special function that writes a given value of an object attribute as "IT IS".

> NOTE:
> - If `value` parameter not given, the function failed.
> - If a `key` parameter not given, the function will use a default value defined by `XOBJ_DEF_ATTR_TNAME`.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance;
- code: `ERR_XOBJ_NSTR`
  + if `attr` parameter given and is not a type of a `string`;
  + if `key` parameter given and is not a type of a `string`.

### Other functions for deal with an object attributes

> Note:
> If a `key` parameter given not given, the function will use a default value defined by `XOBJ_DEF_ATTR_TNAME`.

#### **getXObjAttributes(object\[, key])** => `?object`

This function returns an object which represents a set of the element attributes for a given object or `null` if failed.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance.

#### **checkXObjAttribute(object, attr\[, key])** => `boolean`

This function tries to check whether or not an attribute with a name given by `attr` parameter exists for the element given by `object` parameter. If attribute exists `true` is returned.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance;
- code: `ERR_XOBJ_NSTR`
  + if `attr` parameter given and is not a type of a `string`;
  + if `key` parameter given and is not a type of a `string`.

#### **deleteXObjAttribute(object, attr\[, key])** => `boolean`

This function tries to delete  an attribute with a name given by `attr` parameter. If succeed `true` is returned.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance;
- code: `ERR_XOBJ_NSTR`
  + if `attr` parameter given and is not a type of a `string`;
  + if `key` parameter given and is not a type of a `string`.

#### **renameXObjAttribute(object, attr, newName\[, key])** => `boolean`

> since: \[v0.2.0]

This function tries to rename an attribute with a name given by `attr` parameter to its new name. If succeed `true` is returned.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance;
- code: `ERR_XOBJ_NSTR`
  + if `attr` parameter given and is not a type of a `string`;
  + if `newName` parameter given and is not a type of a `string`;
  + if `key` parameter given and is not a type of a `string`.

### Other functions

#### **getXObjElement(object, name)** => `?any`

This function returns an element from a given object by its name or `null` if that element not found.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance;
- code: `ERR_XOBJ_NSTR`
  + if `name` parameter given and is not a type of a `string`;
- code: `ERR_XOBJ_INVARG_KEY`
  + if `name` parameter given and is an empty string.

#### **addXObjElement(object, name)** => `object`

This function adds an element given by `name` parameter to a members of the given object and returns an `object` that represents a status of the operation.

##### ***result***

The status of an operation contains 2 fields:

- `isSucceed` - `boolean` value;
- `item` - an item which was added or a `null`.

> <!> The function will fail if a target element exists and it is not an object or an array or not a `null`.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance;
- code: `ERR_XOBJ_NSTR`
  + if `name` parameter given and is not a type of a `string`;
- code: `ERR_XOBJ_INVARG_KEY`
  + if `name` parameter given and is an empty string.

#### **insertXObjElement(object, name\[, options])** => `?(object|Array)`

This function inserts an empty element named by a `name` parameter into a given object. If succeed the element will be returned or `null` in opposite.

##### ***parameters***

For details of an `options` parameter read the appropriate section in the description of the [`insertXObjElementEx`](#insertxobjelementexobject-name-options--object) function.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance.

#### **insertXObjElementEx(object, name\[, options])** => `object`

> since: \[v0.2.0]

This function inserts an empty element named by a `name` parameter into a given object and returns an `object` that represents a status of the operation.

##### ***parameters***

An `options` parameter contains the following parameters:

|option name|value type|default value|description|
|:---|---|---:|:---|
|`force`|`boolean`|`false`|modifies the functions behavior|
|`ripOldies`|`boolean`|`false`|it prevents a replacement of an element which exists and is an object or an array. Applied only if `force` option is set to `true`|
|`acceptIfList`|`boolean`|`false`|it defines whether a list elements is treated as a type of an `object`|

An `options.force` parameter modifies the functions behavior as follows:
- when set to `true`, the addressed element will be replaced with an empty element.
- when set to `false`, if the target element exists and is not an object or an array, the function will failed.

##### ***result***

The status of an operation contains 2 fields:

- `isSucceed` - `boolean` value;
- `item` - an item which was inserted or a `null`.

> <!> If a target element exists and it is not a `null`, the function behavior defined by the given options. When a required conditions not met, the function failed.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance;
- code: `ERR_XOBJ_NSTR`
  + if `name` parameter given and is not a type of a `string`;
- code: `ERR_XOBJ_INVARG_KEY`
  + if `name` parameter given and is an empty string.

#### **deleteXObjElement(object, name)** => `boolean`

This function deletes an element addressed by `name` parameter from a given object. If succeed `true` is returned.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance.

#### **deleteXObjElementEx(object, name)** => `object`

This function deletes an element addressed by `name` parameter from a given object and returns an `object` that represents a status of the operation.

##### ***result***

The status of an operation contains 2 fields:

- `isSucceed` - `boolean` value;
- `item` - an item which was deleted or a `null`.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance;
- code: `ERR_XOBJ_NSTR`
  + if `name` parameter given and is not a type of a `string`;
- code: `ERR_XOBJ_INVARG_KEY`
  + if `name` parameter given and is an empty string.

#### **renameXObjElement(object, oldName, newName)** => `boolean`

This function renames an element with a name `oldName` to a name `newName` for the given object. If succeed `true` is returned.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance;
- code: `ERR_XOBJ_NSTR`
  + if `oldName` parameter given and is not a type of a `string`;
  + id `newName` parameter given and is not a type of a `string`.

### Other special functions

#### **evalXObjEName(name)**

This function evaluated a value given by a `name` parameter and return a result. The dependencies of a resulted value returned by the function given in the following table:

|given value|value of result|
|:---|---|
|value is an integer positive number or a string that can be converted to a such number|value of a `number`|
|any other string that can\'t be converted to a number|value of a string|
|any other cases|value of `null`|

#### **insertXObjEList(object, name\[, options])** => `?Array`

This function inserts an elements list named by a `name` parameter into a given object. If succeed the list will be returned or `null` in opposite.

##### ***parameters***

For details of an `options` parameter read the appropriate section in the description of the [`insertXObjEListEx`](#insertxobjelistexobject-name-options--object) function.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance.

#### **insertXObjEListEx(object, name\[, options])** => `object`

> since: \[v0.2.0]

This function inserts an elements list named by a `name` parameter into a given object and returns an `object` that represents a status of the operation.

##### ***parameters***

An `options` parameter contains the following parameters:

|option name|value type|default value|description|
|:---|---|---:|:---|
|`force`|`boolean`|`false`|modifies the functions behavior|
|`ripOldies`|`boolean`|`false`|it prevents a replacement of an element which exists and has a type of an `array`. Applied only if `force` option is set to `true`|

An `options.force` parameter modifies the functions behavior as follows:
- when set to `true`, the addressed element will be replaced with an empty elements list.
- when set to `false`, if the target element type is an `object`, the function will wraps it in the list.
- when set to `false`, if the target element exists and is not an object or an array, the function will failed.

##### ***result***

The status of an operation contains 2 fields:

- `isSucceed` - `boolean` value;
- `item` - an item which was inserted or a `null`.

> <!> If a target element exists and it is not a `null`, the function behavior defined by the given options. When a required conditions not met, the function failed.

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance;
- code: `ERR_XOBJ_NSTR`
  + if `name` parameter given and is not a type of a `string`;
- code: `ERR_XOBJ_INVARG_KEY`
  + if `name` parameter given and is an empty string.

### Experimental functions

> Note: Purpose of those functions will be discussed and some may be deprecate and make obsolete or functionality may be altered. So use it with cautions in mind.

#### **genXObjENameDescr(name)**

This function creates a object that contains description of the value given by a `name` parameter.

A value of the `name` parameter must have some of the following formats:

1. `<name>`;
2. `<name>[<child>]`;
3. `<name>[<child>=<value>]`;
4. `<name>[@<attribute>]`;
5. `<name>[@<attribute>=<value>]`;

> Note: the `<value>` must be quoted if it contains characters like a spaces or square brackets.

The returned object has following attributes:

|attribute|value type|description|
|:---|---|---|
|`isERR`|`boolean`|set to `true` if error happend|
|`name`|`string`|contains a name of the element|
|`conditions`|`object`|contains conditions applied to that element|

The object `conditions` property (*if present*) contains following attributes:

|attribute|value type|description|
|:---|---|---|
|`type`|`string`||
|`name`|`string`||
|`value`|`any`||

#### **insertXObjElements(object, name\[...name_N\[, options]])** => `number`

This function inserts an elements given by the list of names and returns a quantity of the inserted elements.

##### ***parameters***

The `options` parameter if given must be an object. For details see [`insertXObjElement`](#insertxobjelementobject-name-options--objectarray).

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance.

#### **insertXObjEChain(object, name\[, ...name_N\[, options])**

This function inserts a chain of the elements listed as a function arguments. If succeed the last inserted element will be returned or `null` if failed.

> The `options` parameter if given must be an object. For details see [`insertXObjElement`](#insertxobjelementobject-name-options--objectarray).

##### ***exceptions***

The function throws a `TypeError` exception in case:

- code: `ERR_XOBJ_NPOBJ`
  + if `object` parameter is not an `Object` instance.

### Base class

#### **TXmlContentParseOptions**

This class implements an interface for handling XML-parse options.

##### class constructor

The class constructor creates a new instance of the class. It receives arguments listed below:

|name|type|default value|description|
|:---|---|---:|:---|
|`object`|---|---|a host object.|
|`options`|`object`|---|an options settings.|

##### class properties

The table below describes a properties of a `TXmlContentParseOptions` class:

|name|read only|description|
|:---|---|:---|
|settings|yes|presents a current options|
|xml2js|yes|returns an options for an XML-to-JS converter|
|js2xml|yes|returns an options for an JS-to-XML converter|

##### class methods (*static*)

###### **createNewOptionsSet(obj)**

This method transforms a given object to a set of accepted parser options.
